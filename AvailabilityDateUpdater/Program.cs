﻿using ProcuctDB;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace AvailabilityDateUpdater
{
    class Program
    {
        #region file
        static void Main()
        {
            // Create task and start it.
            // ... Wait for it to complete.
            Task task = new Task(ProcessDataAsync);
            task.Start();
            task.Wait();
            Console.ReadLine();
        }

        static async void ProcessDataAsync()
        {
            // Start the HandleFile method.
            Task<int> task = HandleFileAsync(@"C:\Users\Devnetworks\Desktop\desktop files here\Aljohn\April11-15\April14-15\gallay.csv");

            // Control returns here before HandleFileAsync returns.
            // ... Prompt the user.
            Console.WriteLine("Please wait patiently " +
                "while I do something important.");

            // Wait for the HandleFile task to complete.
            // ... Display its results.
            int x = await task;
            Console.WriteLine("Count: " + x);
        }

        static async Task<int> HandleFileAsync(string file)
        {
            Console.WriteLine("HandleFile enter");
            int count = 0;

            // Read in the specified file.
            // ... Use async StreamReader method.
            using (StreamReader reader = new StreamReader(file))
            {
                string v = await reader.ReadToEndAsync();

                // ... Process the file data somehow.
                count += v.Length;

                // ... A slow-running computation.
                //     Dummy code.
                for (int i = 0; i < 10000; i++)
                {
                    int x = v.GetHashCode();
                    if (x == 0)
                    {
                        count--;
                    }
                }
            }
            Console.WriteLine("HandleFile exit");
            return count;
        }
        #endregion

        //static void Main(string[] args)
        //{
        //    /// <summary>
        //    /// this is to update supplier having field plus.availabilityDate
        //    /// 

        //    Do();
        //}

        //private static void Do()
        //{
        //    List<string> withAvailabilityField = new List<string>()
        //    {
        //        "BAER",
        //        "BIECO",
        //        "MOLESKINE",
        //        "SCHMIDT",
        //        "STAEDTLER"
        //    };

        //    ProductFinder productFinder = new ProductFinder();
        //    productFinder.getAllDatabases()
        //        .Where(x => withAvailabilityField.Contains(x.GetType().Name.ToUpper().Replace("DB", "")))
        //        .Select(x => x)
        //        .ToList<IDatabase>()
        //        .ForEach(x =>
        //        {
        //            AppStatus(x);
        //            //LoadAnimation(true);
        //        });
        //}

        //private static void AppStatus(IDatabase x)
        //{
        //    DN_Classes.AppStatus.ApplicationStatus s = new DN_Classes.AppStatus.ApplicationStatus("AvailabilityDateUpdater" + x.GetType().Name.Replace("DB","") +"_148");
        //    s.AddMessagLine("");
        //    s.Start();
        //    try
        //    {
        //        Console.WriteLine("AvailabilityDateUpdater" + x.GetType().Name.Replace("DB", "") + "_148");

        //        x.UpdateIsAvailable();

        //        s.Successful();
        //        s.Stop();
        //        s.Dispose();

        //    }

        //    catch (Exception ex)
        //    {
        //        s.Stop();
        //        s.AddMessagLine(ex.Message + "\n" + ex.StackTrace);
        //        s.Dispose();
        //    }
        //}

        //private static void LoadAnimation(bool trigger)
        //{
        //    int counter = 0;
        //    while(trigger)
        //    {
        //        counter++;
        //        switch (counter % 4)
        //        {
        //            case 0: Console.Write("/"); break;
        //            case 1: Console.Write("-"); break;
        //            case 2: Console.Write("\\"); break;
        //            case 3: Console.Write("|"); break;
        //        }
        //        Console.SetCursorPosition(Console.CursorLeft - 1, Console.CursorTop);
        //        Thread.Sleep(200);
        //    }
        //}
    }
}
