﻿using DN.DataAccess;
using DN.ProductUpdateLib.DownloadDirectory;
using DN.ProductUpdateLib.FileChecker;
using DN.ProductUpdateLib.Source;
using DN.ProductUpdateLib.Source.FTP;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;

namespace DN.ProductUpdateLib.Downloader.KNV
{
    /// <summary>
    /// An implementation of downloading knv files
    /// </summary>
    public class KnvDownloader : IDownloader
    {
        //Access to knv file source
        private readonly ISource source;

        //Retrives a knv ftp file list
        private readonly IFtpFileList ftpFileList;

        //Downloads knv ftp file
        private readonly IFtpFileDownloader ftpFileDownloader;

        //File checker for filter
        private readonly IFileChecker fileChecker;

        //Container of settings/configurations
        private readonly IConfigurationManager configurationManager;

        //Download directory
        private readonly IDownloadDirectory downloadDirectory;

        //ftp credentials to knv ftp access
        private readonly NetworkCredential networkCredential;

        //constructor
        public KnvDownloader(
                    ISource source, 
                    IFtpFileList ftpFileList,
                    IFtpFileDownloader ftpFileDownloader,
                    IFileChecker fileChecker, 
                    IConfigurationManager configurationManager, 
                    IDownloadDirectory downloadDirectory)
        {
            this.source = source;
            this.ftpFileList = ftpFileList;
            this.ftpFileDownloader = ftpFileDownloader;
            this.fileChecker = fileChecker;
            this.configurationManager = configurationManager;
            this.downloadDirectory = downloadDirectory;

            string username = configurationManager.GetValue("ftpusername");
            string password = configurationManager.GetValue("ftppassword");
            networkCredential = new NetworkCredential(username, password);
        }

        /// <summary>
        /// Works on download process
        /// </summary>
        /// <returns>Returns downloaded file list</returns>
        /// <seealso cref="DN.ProductUpdateLib.Downloader"/>
        public IEnumerable<string> DownloadedFiles()
        {
            List<string> downloadedFiles = new List<string>();

            foreach (var sourceData in source.GetFileSources())
            {
                if (sourceData.Type == SourceType.Directory)
                {
                    string[] directoryFiles = ftpFileList.GetFiles(networkCredential, sourceData.Path).ToArray();
                    foreach (string file in directoryFiles)
                    {
                        if (fileChecker.IsOk(file))
                        {
                            string downloadDirectoryPath = downloadDirectory.GetDownloadDirectory();
                            string localFile = downloadDirectoryPath + file.Split('/').Last();
                            if (ftpFileDownloader.Download(networkCredential, file, localFile))
                            {
                                downloadedFiles.Add(localFile);
                            }
                        }
                    }
                }
                else if (fileChecker.IsOk(sourceData.Path))
                {
                    string downloadDirectoryPath = downloadDirectory.GetDownloadDirectory();
                    string localFile = downloadDirectoryPath + sourceData.Path.Split('/').Last();
                    if (ftpFileDownloader.Download(networkCredential, sourceData.Path, localFile))
                    {
                        downloadedFiles.Add(localFile);
                    }
                }
            }

            return downloadedFiles;
        }
    }
}
