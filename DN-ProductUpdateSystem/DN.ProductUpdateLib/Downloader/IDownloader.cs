﻿using DN.ProductUpdateLib.Source;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DN.ProductUpdateLib.Downloader
{
    /// <summary>
    /// An interface for downloading files
    /// </summary>
    /// <seealso cref="DN.ProductUpdateLib.Downloader.KNV"/>
    public interface IDownloader
    {
        /// <summary>
        /// Works on downloading process
        /// </summary>
        /// <returns>Returns a file list</returns>
        IEnumerable<string> DownloadedFiles();
    }
}
