﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DN.ProductUpdateLib.Unzip
{
    /// <summary>
    /// Works on file unzipping process
    /// </summary>
    /// <seealso cref="DN.ProductUpdateLib.Unzip.KNV.KnvUnzipper"/>
    public interface IUnzipper
    {
        /// <summary>
        /// Unzips a file
        /// </summary>
        /// <param name="filePath">The file path of the file to unzip</param>
        /// <returns>Returns unzipping information</returns>
        string Unzip(string filePath);
    }
}
