﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DN.ProductUpdateLib.Unzip.KNV
{
    public class KnvUnzipper : IUnzipper
    {
        public string Unzip(string filePath)
        {
            ProcessStartInfo startInfo = new ProcessStartInfo();
            startInfo.CreateNoWindow = true;
            startInfo.UseShellExecute = true;
            startInfo.WindowStyle = ProcessWindowStyle.Normal;
            //startInfo.WorkingDirectory = @"C:\suppliers\knv\knvmonthly";
            //startInfo.WorkingDirectory = @"C:\knvmonthly";
            startInfo.WorkingDirectory = Path.GetDirectoryName(filePath);
            startInfo.FileName = "cmd.exe";
            startInfo.Arguments = @"/C C:\7za.exe e " + filePath;
            // Start the process with the info we specified.
            // Call WaitForExit and then the using statement will close.
            using (Process exeProcess = Process.Start(startInfo))
            {
                exeProcess.WaitForExit();
                Console.WriteLine("File Unzipped");
            }

            return filePath;
        }
    }
}
