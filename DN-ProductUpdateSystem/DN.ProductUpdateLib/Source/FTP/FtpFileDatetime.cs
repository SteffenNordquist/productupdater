﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;

namespace DN.ProductUpdateLib.Source.FTP
{
    public class FtpFileDatetime : IFtpFileDatetime
    {
        public DateTime GetDateTimestampOnServer(NetworkCredential networkCredential, string remoteFile)
        {
            Uri serverUri = new Uri(remoteFile);

            // The serverUri should start with the ftp:// scheme.
            if (serverUri.Scheme != Uri.UriSchemeFtp)
            {
                return DateTime.MinValue;
            }

            // Get the object used to communicate with the server.
            FtpWebRequest request = (FtpWebRequest)WebRequest.Create(serverUri);
            request.Credentials = networkCredential;
            /* When in doubt, use these options */
            request.UseBinary = true;
            request.UsePassive = true;
            request.KeepAlive = true;
            request.Method = WebRequestMethods.Ftp.GetDateTimestamp;
            FtpWebResponse response = (FtpWebResponse)request.GetResponse();
            return response.LastModified;
        }
    }
}
