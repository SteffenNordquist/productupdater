﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;

namespace DN.ProductUpdateLib.Source.FTP
{
    public class FtpFileExistence : IFtpFileExistence
    {
        public bool Exist(NetworkCredential networkCredential, string remoteFile)
        {
            bool exist = false;

            string uri = remoteFile;
            var request = (FtpWebRequest)WebRequest.Create(uri);
            request.Credentials = networkCredential;
            request.Method = WebRequestMethods.Ftp.GetFileSize;

            try
            {
                FtpWebResponse response = (FtpWebResponse)request.GetResponse();
                exist = true;
            }
            catch (WebException ex)
            {
                FtpWebResponse response = (FtpWebResponse)ex.Response;
                if (response.StatusCode ==
                    FtpStatusCode.ActionNotTakenFileUnavailable)
                {
                    exist = false;
                }
            }
            return exist;
        }
    }
}
