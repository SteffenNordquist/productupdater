﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;

namespace DN.ProductUpdateLib.Source.FTP
{
    public interface IFtpFileList
    {
        IEnumerable<string> GetFiles(NetworkCredential networkCredential, string directory);
    }
}
