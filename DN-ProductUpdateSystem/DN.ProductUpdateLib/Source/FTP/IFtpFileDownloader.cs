﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;

namespace DN.ProductUpdateLib.Source.FTP
{
    public interface IFtpFileDownloader
    {
        bool Download(NetworkCredential networkCredential, string remoteFile, string destinationFile);
    }
}
