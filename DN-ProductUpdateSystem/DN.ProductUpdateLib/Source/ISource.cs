﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DN.ProductUpdateLib.Source
{
    public interface ISource
    {
        IEnumerable<SourceEntity> GetFileSources();
    }
}
