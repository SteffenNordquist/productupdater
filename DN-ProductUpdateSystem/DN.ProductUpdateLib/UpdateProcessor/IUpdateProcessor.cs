﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DN.ProductUpdateLib.UpdateProcessor
{
    public interface IUpdateProcessor
    {
        void BeginUpdate();
    }
}
