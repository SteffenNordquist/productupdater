﻿using DN.DataAccess;
using DN.DataAccess.ConnectionFactory;
using DN.DataAccess.MongoDataAccess;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TestProj
{
    class Program
    {
        static void Main(string[] args)
        {
            DataAccessConfiguration da = new DataAccessConfiguration();

            IDbConnectionFactory fact = da.Resolve<IDbConnectionFactory>();
            IDbConnection mongoConnection = fact.CreateConnection();

            //IConfigurationManager sfsd = new DefaultConfigurationManager("");
            //string test = sfsd.GetValue("Setting1");
        }
    }
}
