﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DN.DataAccess;
using Microsoft.Practices.Unity;
using DN.DataAccess.MongoDataAccess;
using DN.DataAccess.ConnectionFactory;
namespace TestProj
{
    public class DataAccessConfiguration
    {
        private readonly IUnityContainer container;

        public DataAccessConfiguration()
        {
            this.container = new UnityContainer();

            RegisterTypes();
        }

        private void RegisterTypes()
        {
            //string configPath = @"D:\C sharp tools-systems-projects\DN-ProductUpdateSystem\TestProj\LibriApp.config";

            string configPath = AppDomain.CurrentDomain.BaseDirectory + @"\LibriApp.config";
            IConfigurationSource configSource = new DefaultConfigurationSource(configPath);
            container.RegisterInstance(configSource);

            container.RegisterType<IConfigurationManager, DefaultConfigurationManager>();
            container.RegisterType<IConnectionProperties, MongoConnectionProperties>();
            container.RegisterType<IDbConnectionFactory, MongoConnectionFactory>();


        }

        public T Resolve<T>()
        {
            return container.Resolve<T>();
        }
    }
}
