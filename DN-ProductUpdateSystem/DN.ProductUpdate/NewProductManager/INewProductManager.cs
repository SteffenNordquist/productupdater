﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DN.ProductUpdateLib.NewProductManager
{
    public interface INewProductManager
    {
        void Process<T>(T obj);
    }
}
