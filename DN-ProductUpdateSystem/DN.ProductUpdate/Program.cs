﻿using DN.ProductUpdateLib.Suppliers.KNV;
using DN.ProductUpdateLib.UpdateProcessor;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DN.ProductUpdateLib
{
    class Program
    {
        static void Main(string[] args)
        {

            List<IUpdateProcessor> updateProcessors = new List<IUpdateProcessor>();

            updateProcessors.Add(new KnvUpdate());

            foreach (var updateProcessor in updateProcessors)
            {
                updateProcessor.BeginUpdate();
            }
        }
    }
}
