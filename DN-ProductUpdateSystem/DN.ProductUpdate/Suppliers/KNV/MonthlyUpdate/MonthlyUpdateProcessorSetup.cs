﻿using DN.DataAccess;
using DN.DataAccess.ConnectionFactory;
using DN.DataAccess.MongoDataAccess;
using DN.ProductUpdateLib.Downloader;
using DN.ProductUpdateLib.FileChecker;
using DN.ProductUpdateLib.FileFinalizer;
using DN.ProductUpdateLib.Logger;
using DN.ProductUpdateLib.NewProductManager;
using DN.ProductUpdateLib.Source;
using DN.ProductUpdateLib.Unzip;
using DN.ProductUpdateLib.UpdateProcessor;
using Microsoft.Practices.Unity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DN.ProductUpdateLib.Suppliers.KNV.MonthlyUpdate
{
    public class MonthlyUpdateProcessorSetup : IUpdateProcessor
    {
        private readonly IUnityContainer container;

        public MonthlyUpdateProcessorSetup()
        {
            this.container = new UnityContainer();

            RegisterDataAccessRequirements();

            container.RegisterType<ISource, MonthlyUpdateSource>();
            container.RegisterType<IFileChecker, MonthlyUpdateChecker>();
            container.RegisterType<IDownloader, KnvDownloader>();
            container.RegisterType<IUnzipper, KnvUnzipper>();
            container.RegisterType<IFileFinalizer, MonthlyFileFinalizer>();
            container.RegisterType<ILogger, MonthlyUpdateLogger>();

            IDbConnection dbConnection = container.Resolve<IDbConnectionFactory>().CreateConnection();
            container.RegisterInstance(dbConnection);

            container.RegisterType<INewProductManager, KnvNewProductManager>();
            container.RegisterType<IUpdateProcessor, MonthlyUpdateProcessor>();

        }

        private void RegisterDataAccessRequirements()
        {
            string configPath = AppDomain.CurrentDomain.BaseDirectory + @"\Suppliers\KNV\KnvConfiguration.config";
            IConfigurationSource configSource = new DefaultConfigurationSource(configPath);
            container.RegisterInstance(configSource);

            container.RegisterType<IConfigurationManager, DefaultConfigurationManager>();
            container.RegisterType<IConnectionProperties, MongoConnectionProperties>();
            container.RegisterType<IDbConnectionFactory, MongoConnectionFactory>();
        }

        public void BeginUpdate()
        {
            var knvUpdateProcessor = container.Resolve<IUpdateProcessor>();

            knvUpdateProcessor.BeginUpdate();
        }
    }
}
