﻿using DN.DataAccess;
using DN.ProductUpdateLib.FileChecker;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;

namespace DN.ProductUpdateLib.Suppliers.KNV.MonthlyUpdate
{
    public class MonthlyUpdateChecker : IFileChecker
    {
        private readonly IConfigurationManager configurationManager;
        private readonly FtpSourceManager ftpSourceManager;

        public MonthlyUpdateChecker(IConfigurationManager configurationManager)
        {
            this.configurationManager = configurationManager;

            string username = configurationManager.GetValue("ftpusername");
            string password = configurationManager.GetValue("ftppassword"); 
            ftpSourceManager = new FtpSourceManager(
                new NetworkCredential(username,password)
            );
        }

        public bool IsOk(string filePath)
        {
            DateTime lastDateTime = DateTime.Parse(configurationManager.GetValue("lastMonthlyUpdateDate"));
            DateTime ftpFileDateTime = ftpSourceManager.GetDateTimestampOnServer(filePath);

            return ftpFileDateTime > lastDateTime ? true : false;
        }
    }
}
