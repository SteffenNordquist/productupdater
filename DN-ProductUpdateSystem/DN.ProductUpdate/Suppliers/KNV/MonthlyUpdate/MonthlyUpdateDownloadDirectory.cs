﻿using DN.DataAccess;
using DN.ProductUpdateLib.DownloadDirectory;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DN.ProductUpdateLib.Suppliers.KNV.MonthlyUpdate
{
    public class MonthlyUpdateDownloadDirectory : IDownloadDirectory
    {
        private readonly IConfigurationManager configurationManager;

        public MonthlyUpdateDownloadDirectory(IConfigurationManager configurationManager)
        {
            this.configurationManager = configurationManager;
        }

        public string GetDownloadDirectory()
        {
            return configurationManager.GetValue("monthlyUpdateDownloadDirectory");
        }
    }
}
