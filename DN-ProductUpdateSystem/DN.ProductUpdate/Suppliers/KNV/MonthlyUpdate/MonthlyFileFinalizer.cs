﻿using DN.ProductUpdateLib.Downloader;
using DN.ProductUpdateLib.FileFinalizer;
using DN.ProductUpdateLib.Unzip;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DN.ProductUpdateLib.Suppliers.KNV.MonthlyUpdate
{
    public class MonthlyFileFinalizer : IFileFinalizer
    {
        private readonly IDownloader downloader;
        private readonly IUnzipper unzipper;

        public MonthlyFileFinalizer(IDownloader downloader, IUnzipper unzipper)
        {
            this.downloader = downloader;
            this.unzipper = unzipper;
        }

        public IEnumerable<string> FinalizedFiles()
        {
            List<string> finalFiles = new List<string>();

            foreach(string downloadedFile in downloader.DownloadedFiles())
            {
                finalFiles.Add(unzipper.Unzip(downloadedFile).Replace(".Z", ""));
            }

            return finalFiles;
        }
    }
}
