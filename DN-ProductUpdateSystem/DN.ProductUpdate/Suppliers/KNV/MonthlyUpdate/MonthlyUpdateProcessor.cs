﻿using DN.DataAccess.ConnectionFactory;
using DN.ProductUpdateLib.FileFinalizer;
using DN.ProductUpdateLib.Logger;
using DN.ProductUpdateLib.NewProductManager;
using DN.ProductUpdateLib.UpdateProcessor;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml;

namespace DN.ProductUpdateLib.Suppliers.KNV.MonthlyUpdate
{
    public class MonthlyUpdateProcessor : IUpdateProcessor
    {
        private readonly IFileFinalizer fileFinalizer;
        private readonly ILogger logger;
        private readonly IDbConnection dbConnection;
        private readonly INewProductManager newProductManager;

        public MonthlyUpdateProcessor(IFileFinalizer fileFinalizer, ILogger logger, IDbConnection dbConnection, INewProductManager newProductManager)
        {
            this.fileFinalizer = fileFinalizer;
            this.logger = logger;
            this.dbConnection = dbConnection;
            this.newProductManager = newProductManager;
        }

        public void BeginUpdate()
        {
            using (logger)
            {
                foreach (string file in fileFinalizer.FinalizedFiles())
                {
                    XmlDocument xmldoc = new XmlDocument();

                    try
                    {
                        xmldoc.Load(file);
                        xmldoc.CreateXmlDeclaration("1.0", "ISO-8859-1", null);
                    }catch{
                        var lines = File.ReadAllLines(file, Encoding.GetEncoding("ISO-8859-1"));
                        StringBuilder xmlSb = new StringBuilder();

                        int countLines = 0;
                        foreach (var line in lines)
                        {
                            if (countLines++ != 1)
                            {
                                xmlSb.AppendLine(line);
                            }
                        }
                        xmldoc.LoadXml(xmlSb.ToString());
                    }

                    List<XmlNode> xmlNodeList = new List<XmlNode>();
                    foreach (XmlNode xmlNode in xmldoc.DocumentElement.ChildNodes)
                    {
                        xmlNodeList.Add(xmlNode);
                    }

                    ParallelOptions parallerOptions = new ParallelOptions();
                    parallerOptions.MaxDegreeOfParallelism = 8;

                    Parallel.ForEach(xmlNodeList, parallerOptions, node =>
                    {
                        string jsonText = JsonConvert.SerializeXmlNode(node);

                        JObject obj = JObject.Parse(jsonText);
                        //BsonDocument document = MongoDB.Bson.Serialization.BsonSerializer.Deserialize<BsonDocument>(jsonText);

                        if (obj["product"] != null)
                        //if (document.ToString().Contains("product"))
                        {
                            string a001 = obj["product"]["a001"].ToString();
                            //string a001 = document["product"]["a001"].ToString();

                            Object knv = dbConnection.DataAccess.Queries.FindByKey<Object>("knvbook.product.a001", a001);
                            //var test = Collection.FindOne(Query.EQ("knvbook.product.a001", a001));
                            
                            if(knv != null)
                            //if (test != null)
                            {
                                
                                //var id = test.Id;
                                //var update = Update.Set("knvbook", document);
                                //Collection.Update(Query.EQ("_id", id), update);
                                dbConnection.DataAccess.Commands.Update("knvbook.product.a001", a001, "knvbook", jsonText, false);

                                string log = String.Format("Knvmonthly : {0} document updated.", a001);
                                logger.Log(log);
                            }
                            else
                            {
                                dbConnection.DataAccess.Commands.Insert<string>(jsonText);
                                //Entity entity = new Entity { knvbook = document, plus = new BsonDocument() };
                                //Collection.Insert(entity);

                                newProductManager.Process<JObject>(obj);
                                //AddToProductSizesCollection(GetEan(document));

                                string log = String.Format("Knvmonthly : {0} document inserted.", a001);
                                logger.Log(log);
                            }

                        }
                    });

                }

            }
        }
    }
}
