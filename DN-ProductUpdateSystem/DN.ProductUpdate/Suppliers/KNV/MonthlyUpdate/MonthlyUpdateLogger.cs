﻿using DN.DataAccess;
using DN.ProductUpdateLib.Logger;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DN.ProductUpdateLib.Suppliers.KNV.MonthlyUpdate
{
    public class MonthlyUpdateLogger : ILogger
    {
        private List<string> logs;
        private readonly IConfigurationManager configurationManager;

        public MonthlyUpdateLogger(IConfigurationManager configurationManager)
        {
            logs = new List<string>();
            this.configurationManager = configurationManager;
        }

        public void Log(string log)
        {
            logs.Add(log);
            Console.WriteLine(log);
        }

        public void Dispose()
        {
            string logDirectory = configurationManager.GetValue("monthlyUpdateLogDirectory");
            string logFile = logDirectory + "\\" + DateTime.Now.Ticks + "_log.txt";
            File.WriteAllLines(logFile, logs);
        }
    }
}
