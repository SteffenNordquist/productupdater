﻿using DN.DataAccess;
using DN.ProductUpdateLib.Source;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DN.ProductUpdateLib.Suppliers.KNV.MonthlyUpdate
{
    public class MonthlyUpdateSource : ISource
    {
        private readonly IConfigurationManager configurationManager;

        public MonthlyUpdateSource(IConfigurationManager configurationManager)
        {
            this.configurationManager = configurationManager;
        }

        public IEnumerable<SourceEntity> GetFileSources()
        {
            string path = configurationManager.GetValue("monthlyUpdateSource");

            string type = configurationManager.GetValue("monthlyUpdateSourceType");
            SourceType sourceType = (SourceType)Enum.Parse(typeof(SourceType), type);  

            List<SourceEntity> fileSources = new List<SourceEntity>();
            fileSources.Add(new SourceEntity
            {
                Path = path,
                Type = sourceType
            });
            return fileSources;
        }
    }
}
