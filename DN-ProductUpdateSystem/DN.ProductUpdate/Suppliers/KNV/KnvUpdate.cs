﻿using DN.DataAccess;
using DN.DataAccess.ConnectionFactory;
using DN.DataAccess.MongoDataAccess;
using DN.ProductUpdateLib.Suppliers.KNV.Daily;
using DN.ProductUpdateLib.Suppliers.KNV.HourlyUpdate;
using DN.ProductUpdateLib.Suppliers.KNV.MonthlyUpdate;
using DN.ProductUpdateLib.UpdateProcessor;
using Microsoft.Practices.Unity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DN.ProductUpdateLib.Suppliers.KNV
{
    public class KnvUpdate : IUpdateProcessor
    {

        private readonly List<IUpdateProcessor> knvUpdates;

        public KnvUpdate()
        {
            knvUpdates = new List<IUpdateProcessor>();
            knvUpdates.Add(new MonthlyUpdateProcessorSetup());
            knvUpdates.Add(new DailyUpdateProcessorSetup());
            knvUpdates.Add(new HourlyUpdateProcessorSetup());

        }

        public void BeginUpdate()
        {
            foreach (IUpdateProcessor updateProcessor in knvUpdates)
            {
                updateProcessor.BeginUpdate();
            }
        }
    }
}
