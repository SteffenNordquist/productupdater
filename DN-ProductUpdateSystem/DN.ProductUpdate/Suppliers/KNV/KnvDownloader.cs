﻿using DN.DataAccess;
using DN.ProductUpdateLib.DownloadDirectory;
using DN.ProductUpdateLib.Downloader;
using DN.ProductUpdateLib.FileChecker;
using DN.ProductUpdateLib.Source;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;

namespace DN.ProductUpdateLib.Suppliers.KNV
{
    public class KnvDownloader : IDownloader
    {
        private readonly ISource source;
        private readonly FtpSourceManager ftpSourceManager;
        private readonly IFileChecker fileChecker;
        private readonly IConfigurationManager configurationManager;
        private readonly IDownloadDirectory downloadDirectory;

        public KnvDownloader(ISource source, IFileChecker fileChecker, IConfigurationManager configurationManager, IDownloadDirectory downloadDirectory)
        {
            this.source = source;
            this.fileChecker = fileChecker;
            this.configurationManager = configurationManager;
            this.downloadDirectory = downloadDirectory;

            string username = configurationManager.GetValue("ftpusername");
            string password = configurationManager.GetValue("ftppassword"); 
            ftpSourceManager = new FtpSourceManager(
                new NetworkCredential(username, password));
        }

        public IEnumerable<string> DownloadedFiles()
        {
            List<string> downloadedFiles = new List<string>();

            foreach (var sourceData in source.GetFileSources())
            {
                if (sourceData.Type == SourceType.Directory)
                {
                    string[] directoryFiles = ftpSourceManager.GetFiles(sourceData.Path);
                    foreach (string file in directoryFiles)
                    {
                        if (fileChecker.IsOk(file))
                        {
                            string downloadDirectoryPath = downloadDirectory.GetDownloadDirectory();
                            string localFile = downloadDirectoryPath + file.Split('/').Last();
                            if (ftpSourceManager.Download(file, localFile))
                            {
                                downloadedFiles.Add(localFile);
                            }
                        }
                    }
                }
                else if (fileChecker.IsOk(sourceData.Path))
                {
                    string downloadDirectoryPath = downloadDirectory.GetDownloadDirectory();
                    string localFile = downloadDirectoryPath + sourceData.Path.Split('/').Last();
                    if (ftpSourceManager.Download(sourceData.Path, localFile))
                    {
                        downloadedFiles.Add(localFile);
                    }
                }
            }

            return downloadedFiles;
        }

       
    }

    
}
