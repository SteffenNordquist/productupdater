﻿using DN.DataAccess;
using DN.DataAccess.ConnectionFactory;
using DN.DataAccess.MongoDataAccess;
using DN.ProductUpdateLib.NewProductManager;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DN.ProductUpdateLib.Suppliers.KNV
{
    public class KnvNewProductManager : INewProductManager
    {
        private readonly IDbConnection dbConnection;

        public KnvNewProductManager(IConfigurationManager configurationManager)
        {

            IConnectionProperties connectionProperties = new MongoConnectionProperties(configurationManager);
            connectionProperties.SetPropertyName("productsizesconnectionstring");
            connectionProperties.SetPropertyName("productsizesdatabasename");
            connectionProperties.SetPropertyName("productsizescollectionname");

            IDbConnectionFactory dbConnectionFactory = new MongoConnectionFactory(connectionProperties);
            dbConnection = dbConnectionFactory.CreateConnection();
        }

        public void Process<T>(T obj)
        {
            string ean = "";
            //JObject jsonObject = JObject.Parse(knvSample);
            JObject jsonObject = obj as JObject;
            JToken product = jsonObject["product"];
            if (product["productidentifier"] != null && product["productidentifier"] is JArray)
            {
                JArray productidentifiers = product["productidentifier"] as JArray;
                foreach (JToken productidentifier in productidentifiers)
                {
                    JToken b244 = productidentifier["b244"];
                    if (b244 != null && b244.ToString().Length == 13)
                    {
                        ean = b244.ToString();
                        break;
                    }

                }
            }
            else if (product["productidentifier"] != null)
            {
                JToken productidentifier = product["productidentifier"];
                JToken b244 = productidentifier["b244"];
                if (b244 != null && b244.ToString().Length == 13)
                {
                    ean = b244.ToString();
                }

            }

            if (ean != "")
            {
                string json = "{ ean = " + ean + ", measure = {}}";
                Object item = dbConnection.DataAccess.Queries.FindByKey<Object>("ean", ean);
                if (item == null)
                {
                    dbConnection.DataAccess.Commands.Insert<string>(json);
                }
            }
        }
    }
}
