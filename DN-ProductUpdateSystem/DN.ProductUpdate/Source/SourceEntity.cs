﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DN.ProductUpdateLib.Source
{
    public class SourceEntity
    {
        public string Path { get; set; }
        public SourceType Type { get; set; }
    }
}
