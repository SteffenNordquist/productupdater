﻿using DN.ProductUpdateLib.Source;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DN.ProductUpdateLib.Downloader
{
    public interface IDownloader
    {

        IEnumerable<string> DownloadedFiles();
    }
}
