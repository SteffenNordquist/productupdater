﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Configuration;
namespace DN.DataAccess.ConfigurationData
{
    public class DefaultConfigurationManager : IConfigurationManager
    {
        public DefaultConfigurationManager(IConfigurationSource configurationSource)
        {
            configurationSource.SetConfigurationFile();
        }

        public string GetValue(string name)
        {
            return ConfigurationManager.AppSettings[name];
        }


        public void SetValue(string name, string value)
        {
            ConfigurationManager.AppSettings[name] = value;
        }
    }
}
