﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace DN.DataAccess.ConfigurationData
{
    public class DefaultConfigurationSource : IConfigurationSource
    {
        private readonly string configFullPath;

        public DefaultConfigurationSource(string configFullPath)
        {
            this.configFullPath = configFullPath;
        }

        public void SetConfigurationFile()
        {
            AppDomain.CurrentDomain.SetData("APP_CONFIG_FILE", configFullPath);

            //There exists a class ClientConfigPaths that caches the paths. So, even after changing the path with SetData, it is not re-read, because there already exist cached values. The solution is to remove these
            //http://stackoverflow.com/questions/6150644/change-default-app-config-at-runtime
            ResetConfigMechanism();
        }

        private void ResetConfigMechanism()
        {
            typeof(ConfigurationManager)
                .GetField("s_initState", BindingFlags.NonPublic | 
                                         BindingFlags.Static)
                .SetValue(null, 0);

            typeof(ConfigurationManager)
                .GetField("s_configSystem", BindingFlags.NonPublic | 
                                            BindingFlags.Static)
                .SetValue(null, null);

            typeof(ConfigurationManager)
                .Assembly.GetTypes()
                .Where(x => x.FullName == 
                            "System.Configuration.ClientConfigPaths")
                .First()
                .GetField("s_current", BindingFlags.NonPublic | 
                                       BindingFlags.Static)
                .SetValue(null, null);
        }
    }
}
