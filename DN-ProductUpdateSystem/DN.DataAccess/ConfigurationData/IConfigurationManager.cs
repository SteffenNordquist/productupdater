﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DN.DataAccess.ConfigurationData
{
    public interface IConfigurationManager
    {
        string GetValue(string name);

        void SetValue(string name, string value);
    }
}
