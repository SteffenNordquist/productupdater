﻿using DN.DataAccess.DataCommand;
using DN.DataAccess.DataQuery;
using MongoDB.Bson;
using MongoDB.Bson.Serialization;
using MongoDB.Driver;
using MongoDB.Driver.Builders;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DN.DataAccess.MongoDataAccess
{
    public class MongoDataAccess : IDataAccess
    {
        private readonly IDataCommand _commands;
        private readonly IDataQuery _queries;

        public MongoDataAccess(IDataCommand commands, IDataQuery queries)
        {
            this._commands = commands;
            this._queries = queries;
        }

        public IDataCommand Commands
        {
            get { return _commands; }
        }

        public IDataQuery Queries
        {
            get { return _queries; }
        }
    }
}
