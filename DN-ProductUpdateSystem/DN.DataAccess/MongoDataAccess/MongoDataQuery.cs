﻿using DN.DataAccess.DataQuery;
using MongoDB.Bson;
using MongoDB.Bson.Serialization;
using MongoDB.Driver;
using MongoDB.Driver.Builders;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DN.DataAccess.MongoDataAccess
{
    public class MongoDataQuery : IDataQuery
    {
        private readonly MongoCollection<BsonDocument> collection;

        public MongoDataQuery(MongoCollection<BsonDocument> collection)
        {
            this.collection = collection;
        }

        public T FindByKey<T>(string key, string value)
        {
            BsonDocument result = collection.FindOne(Query.EQ(key, value));

            if (result == null)
            {
                return default(T);
            }

            return BsonSerializer.Deserialize<T>(result);
        }

        public IEnumerable<T> Find<T>(string key, string value, string[] setfields = null, int skip = 0, int limit = Int32.MaxValue)
        {
            if (setfields == null) { setfields = new string[0]; }

            List<T> mappedResults = new List<T>();

            List<BsonDocument> results = collection.Find(Query.EQ(key, value)).SetSkip(skip).SetLimit(limit)
                .SetFlags(QueryFlags.NoCursorTimeout).SetFields(setfields).ToList();

            if (results == null)
            {
                return default(IEnumerable<T>);
            }

            results.ForEach(x =>
                mappedResults.Add(BsonSerializer.Deserialize<T>(x)));

            return mappedResults;
        }

        public IEnumerable<T> Find<T>(string query, string[] setfields = null, int skip = 0, int limit = Int32.MaxValue)
        {
            if (setfields == null) { setfields = new string[0];}

            //example "{ name : value }"
            BsonDocument document = BsonSerializer.Deserialize<BsonDocument>(query);
            QueryDocument queryDocument = new QueryDocument(document);

            List<T> mappedResults = new List<T>();

            List<BsonDocument> results = collection.Find(queryDocument).SetSkip(skip).SetLimit(limit)
                .SetFlags(QueryFlags.NoCursorTimeout).SetFields(setfields).ToList();

            if (results == null)
            {
                return default(IEnumerable<T>);
            }

            results.ForEach(x =>
                mappedResults.Add(BsonSerializer.Deserialize<T>(x)));

            return mappedResults;
        }


        public IEnumerable<T> Find<T>(string query, List<BsonValue> notInThisValues, string[] setfields = null, int skip = 0, int limit = Int32.MaxValue)
        {
            if (setfields == null) { setfields = new string[0]; }

            //example "{ name : value }"
            BsonDocument document = BsonSerializer.Deserialize<BsonDocument>(query);
            QueryDocument queryDocument = new QueryDocument(document);

            List<T> mappedResults = new List<T>();

            List<BsonDocument> results = collection.Find(Query.And(queryDocument, Query.NotIn("_id", notInThisValues))).SetSkip(skip).SetLimit(limit)
                .SetFlags(QueryFlags.NoCursorTimeout).SetFields(setfields).ToList();

            if (results == null)
            {
                return default(IEnumerable<T>);
            }

            results.ForEach(x =>
                mappedResults.Add(BsonSerializer.Deserialize<T>(x)));

            return mappedResults;
        }
    }
}
