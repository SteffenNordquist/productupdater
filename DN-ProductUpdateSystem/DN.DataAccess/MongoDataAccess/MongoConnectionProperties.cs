﻿using DN.DataAccess.ConnectionFactory;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DN.DataAccess.MongoDataAccess
{
    public class MongoConnectionProperties : IConnectionProperties
    {
        private readonly IConfigurationManager configurationManager;
        private List<string> runtimeAddedPropertyNames;
        private Dictionary<string, string> runtimeAddedPropertyNameValuePair;

        public MongoConnectionProperties(IConfigurationManager configurationManager)
        {
            this.configurationManager = configurationManager;
            runtimeAddedPropertyNames = new List<string>();
            runtimeAddedPropertyNameValuePair = new Dictionary<string, string>();
        }

        public string GetProperty(string name)
        {
            string propertyName = runtimeAddedPropertyNames.Find(x => x.Contains(name));

            string propertyValue = null;

            if (runtimeAddedPropertyNameValuePair.ContainsKey(name)) 
            {
                propertyValue = runtimeAddedPropertyNameValuePair[name];
            }

            if (propertyName != null && propertyValue == null)
            {
                return configurationManager.GetValue(propertyName);
            }
            else if(propertyValue != null)
            {
                return propertyValue;
            }

            return configurationManager.GetValue(name);
        }


        public void SetPropertyName(string name)
        {
            runtimeAddedPropertyNames.Add(name);
        }

        public void SetProperty(string name, string value)
        {
            runtimeAddedPropertyNameValuePair.Add(name, value);
        }
    }
}
