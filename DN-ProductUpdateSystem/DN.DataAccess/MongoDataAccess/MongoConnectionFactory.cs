﻿using DN.DataAccess.ConnectionFactory;
using DN.DataAccess.DataCommand;
using DN.DataAccess.DataQuery;
using MongoDB.Bson;
using MongoDB.Driver;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DN.DataAccess.MongoDataAccess
{
    public class MongoConnectionFactory : IDbConnectionFactory
    {
        private readonly IConnectionProperties connectionProperties;

        public MongoConnectionFactory(IConnectionProperties connectionProperties)
        {
            this.connectionProperties = connectionProperties;
        }

        public IDbConnection CreateConnection()
        {   
            string connectionString = connectionProperties.GetProperty("connectionstring");
            string databaseName = connectionProperties.GetProperty("databasename");
            string collectionName = connectionProperties.GetProperty("collectionname");
            
            var collection = new MongoClient(connectionString)
                .GetServer()
                .GetDatabase(databaseName)
                .GetCollection<BsonDocument>(collectionName);

            IDataQuery dataQuery = new MongoDataQuery(collection);
            IDataCommand dataCommand = new MongoDataCommand(collection);

            IDataAccess mongoDataAccess = new MongoDataAccess(dataCommand, dataQuery);

            return new MongoConnection(mongoDataAccess);
        }

    }
}
