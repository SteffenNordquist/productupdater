﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DN.DataAccess
{
    public class NullConfigurationManager : IConfigurationManager
    {
        public string GetValue(string name)
        {
            return "";
        }

        public void SetValue(string name, string value)
        {
            
        }
    }
}
