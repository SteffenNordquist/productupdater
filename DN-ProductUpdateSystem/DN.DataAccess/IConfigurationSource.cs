﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DN.DataAccess
{
    public interface IConfigurationSource
    {
        void SetConfigurationFile();
    }
}
