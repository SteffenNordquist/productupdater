﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DN.DataAccess
{
    public class DefaultConfigurationSource : IConfigurationSource
    {
        private readonly string configFullPath;

        public DefaultConfigurationSource(string configFullPath)
        {
            this.configFullPath = configFullPath;
        }

        public void SetConfigurationFile()
        {
            AppDomain.CurrentDomain.SetData("APP_CONFIG_FILE", configFullPath);
        }
    }
}
