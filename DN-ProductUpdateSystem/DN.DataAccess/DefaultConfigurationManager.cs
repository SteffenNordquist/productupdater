﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Configuration;
namespace DN.DataAccess
{
    public class DefaultConfigurationManager : IConfigurationManager
    {
        private readonly IConfigurationSource configurationSource;

        public DefaultConfigurationManager(IConfigurationSource configurationSource)
        {
            this.configurationSource = configurationSource;

            this.configurationSource.SetConfigurationFile();
        }

        public string GetValue(string name)
        {
            return ConfigurationManager.AppSettings[name];
        }


        public void SetValue(string name, string value)
        {
            ConfigurationManager.AppSettings[name] = value;
        }
    }
}
