﻿using MongoDB.Bson;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DN.DataAccess.DataQuery
{
    public interface IDataQuery
    {
        /// <summary>
        /// returns and item matched key-value data
        /// </summary>
        /// <typeparam name="T">generic</typeparam>
        /// <param name="key">eg: age</param>
        /// <param name="value">eg : 24</param>
        /// <returns></returns>
        T FindByKey<T>(string key, string value);

        /// <summary>
        /// return items matched key-value data
        /// </summary>
        /// <typeparam name="T">generic</typeparam>
        /// <param name="key">eg: age</param>
        /// <param name="value">eg : 24</param>
        /// <returns></returns>
        IEnumerable<T> Find<T>(string key, string value, string[] setfields = null, int skip = 0, int limit = Int32.MaxValue);

        /// <summary>
        /// return items matched query value
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="query">eg for mongo: { 'age' : 24 }</param>
        /// <returns></returns>
        IEnumerable<T> Find<T>(string query, string[] setfields = null, int skip = 0, int limit = Int32.MaxValue);

        /// <summary>
        /// return items matched query value and filtered with 'notInThisValues'
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="query"></param>
        /// <param name="notInThisValues"></param>
        /// <returns></returns>
        IEnumerable<T> Find<T>(string query, List<BsonValue> notInThisValues, string[] setfields = null, int skip = 0, int limit = Int32.MaxValue);
    }
}
