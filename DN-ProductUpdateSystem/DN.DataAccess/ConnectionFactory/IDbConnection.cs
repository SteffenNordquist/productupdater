﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DN.DataAccess.ConnectionFactory
{
    public interface IDbConnection
    {
        IDataAccess DataAccess { get;}
    }
}
