﻿using DN.DataAccess.ConnectionFactory;
using MongoDB.Bson;
using MongoDB.Driver;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DN.DataAccess.ConnectionFactory.Mongo
{
    public class MongoConnection : IDbConnection
    {
        public MongoConnection(IDataAccess dataAccess)
        {
            DataAccess = dataAccess;
        }


        public IDataAccess DataAccess
        {
            get;
            private set;
        }
    }
}
