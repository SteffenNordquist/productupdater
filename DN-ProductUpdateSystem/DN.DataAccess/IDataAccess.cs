﻿using DN.DataAccess.DataCommand;
using DN.DataAccess.DataQuery;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DN.DataAccess
{
    public interface IDataAccess
    {

        IDataCommand Commands { get; }

        IDataQuery Queries { get; }
        
    }
}
