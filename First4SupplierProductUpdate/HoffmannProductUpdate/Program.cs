﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;

using MongoDB.Bson;
using MongoDB.Driver;
using MongoDB.Driver.Builders;
using MongoDB.Driver.GridFS;
using MongoDB.Driver.Linq;

using ProcuctDB.Hoffmann;
using DN_Classes.AppStatus;

namespace HoffmannProductUpdate
{
    class Program
    {
        public static MongoCollection<HoffmannEntity> Collection;
        public static MongoCollection<ProductSizeEntity> ProductSizesCollection;

        static void SetCollection()
        {
            MongoClient Client = new MongoClient(@"mongodb://client148:client148devnetworks@136.243.44.111/PD_Hoffmann");
            MongoServer Server = Client.GetServer();
            MongoDatabase Database = Server.GetDatabase("PD_Hoffmann");
            Collection = Database.GetCollection<HoffmannEntity>("hoffmann");

            ProductSizesCollection = new MongoClient("mongodb://client148:client148devnetworks@136.243.44.111/ProductSize").GetServer().GetDatabase("ProductSize").GetCollection<ProductSizeEntity>("productsizes");

        }

        static void Main(string[] args)
        {
            using(ApplicationStatus appStatus = new ApplicationStatus("HoffmannProductUpdate_148"))
            {
                try
                {
                    if (!Directory.Exists(Properties.Settings.Default.logPath + @"\Weekly\")) { Directory.CreateDirectory(Properties.Settings.Default.logPath + @"\Weekly\"); }
                    if (!Directory.Exists(Properties.Settings.Default.logPath + @"\Hourly\")) { Directory.CreateDirectory(Properties.Settings.Default.logPath + @"\Hourly\"); }

                    SetCollection();

                    Console.WriteLine("Starting Hoffmann Pricat update...");
                    CheckPricatFile();
                    Console.WriteLine("Starting Hoffmann Inventory update...");
                    CheckInventoryFile("/Daten/Inventory_20/");
                    CheckInventoryFile("/Daten/Inventory_21/");

                    appStatus.Successful();
                    appStatus.AddMessagLine("Success");
                }
                catch (Exception ex)
                {
                    appStatus.AddMessagLine(ex.Message + "\n" + ex.StackTrace);
                }
            
            }

        }

        static void CheckPricatFile()
        {
            if (FTPClientStuff.Exist("PRICAT.csv", "/Daten/PRICAT/"))
            {
                DateTime FileDateModified = FTPClientStuff.GetDateTimestampOnServer("PRICAT.csv", "/Daten/PRICAT/");

                if (Properties.Settings.Default.lastdatetime < FileDateModified)
                {
                    StringBuilder sb = new StringBuilder();

                    Properties.Settings.Default.lastdatetime = FileDateModified;

                    bool downloaded = FTPClientStuff.Download("PRICAT.csv", Properties.Settings.Default.PricatDownloadDir, "/Daten/PRICAT/");

                    if (downloaded)
                    {
                        List<string> fileRows = File.ReadAllLines(Properties.Settings.Default.PricatDownloadDir, Encoding.GetEncoding("ISO-8859-1")).ToList();
                        fileRows.RemoveAt(0);
                        foreach (string line in fileRows)
                        {
                            string[] wlines = line.Split(';');
                            #region document

                            string ean = wlines[4].Replace("\"", "");
                            if (ean != "" && ean != null)
                            {

                                AddToProductSizesCollection(ean);

                                BsonDocument bsondoc = new BsonDocument
                            {
                            {"Hoffmann Artikelnr",wlines[0].Replace("\"","")},
                            {"Hersteller Artikelnr",wlines[1].Replace("\"","")},
                            {"Hersteller Name",wlines[2].Replace("\"","")},
                            {"Artikelbezeichnung",wlines[3].Replace("\"","")},
                            {"EAN",ean},
                            {"EK-Preis",wlines[5].Replace("\"","").Replace(",",".")},
                            {"UVP",wlines[6].Replace("\"","").Replace(",",".")},
                            {"Buchpreisbindung",wlines[7].Replace("\"","")},
                            {"Verpackungseinheit",wlines[8].Replace("\"","")},
                            {"MWSt Satz",wlines[9].Replace("\"","")},
                            {"  ",wlines[10].Replace("\"","")},
                            {"Ursprungsland",wlines[11].Replace("\"","")},
                            {"Länge in mm",wlines[12].Replace("\"","")},
                            {"Lagerbestand",0},
                            {"Breite in mm",wlines[13].Replace("\"","")},
                            {"Höhe in mm",wlines[14].Replace("\"","")},
                            {"Bruttogewicht (g)",wlines[15].Replace("\"","")},
                            {"noname1",wlines[16].Replace("\"","")},
                            {"Verpackungsart",wlines[17].Replace("\"","")},
                            {"Buschdata WG",wlines[18].Replace("\"","")},
                            {"Gepl Erscheinungsdatum",wlines[19].Replace("\"","")},
                            {"Verkauf nur in VE",wlines[20].Replace("\"","")},
                            {"noname2",wlines[21].Replace("\"","")},
                            {"Haupt-EAN",wlines[22].Replace("\"","")},
                            {"Werbetext",wlines[23].Replace("\"","")},
                            {"noname3",wlines[24].Replace("\"","")},
                            {"Info",wlines[25].Replace("\"","")},
                            {"pic",wlines[26].Replace("\"","")},
                            {"pic01",wlines[27].Replace("\"","")},
                            {"pic02",wlines[28].Replace("\"","")},
                            {"pic03",wlines[29].Replace("\"","")},
                            {"pic04",wlines[30].Replace("\"","")},
                            {"pic05",wlines[31].Replace("\"","")},
                            {"pic06",wlines[32].Replace("\"","")},
                            {"pic07",wlines[33].Replace("\"","")},
                            {"pic08",wlines[34].Replace("\"","")},
                            {"pic09",wlines[35].Replace("\"","")},
                            {"thumbnail",wlines[36].Replace("\"","")},
                            {"neue Artikelnummer",wlines[37].Replace("\"","")},
                            {"alte Artikelnummer",wlines[38].Replace("\"","")},
                            {"Sprungmenge",wlines[39].Replace("\"","")},
                            {"Anzahl Warnhinweise",wlines[40].Replace("\"","")},
                            {"Anzahl Werbetexte",wlines[41].Replace("\"","")},
                            {"Grundpreispflichtig",wlines[42].Replace("\"","")},
                            {"Inhalt",wlines[43].Replace("\"","")},
                            {"Inhalt Einheit",wlines[44].Replace("\"","")}
                            };
                            #endregion

                                HoffmannEntity hEntity = new HoffmannEntity { doc = bsondoc };

                                #region upating/inserting document
                                UpdateBuilder update = Update.Set("doc", bsondoc);
                                WriteConcernResult wcr = Collection.Update(Query.EQ("doc.EAN", ean), update);
                                if (wcr.UpdatedExisting)
                                {
                                    string message = String.Format("Hoffmann Pricat : Up Success : {0}", ean);
                                    Console.WriteLine(message);
                                    sb.AppendLine(message);
                                }
                                else if (!wcr.UpdatedExisting)
                                {
                                    Collection.Insert(hEntity);
                                    string message = String.Format("Hoffmann Pricat : Ins Success : {0}", ean);
                                    Console.WriteLine(message);
                                    sb.AppendLine(message);
                                }
                            }
                                #endregion
                        }

                        File.WriteAllText(Properties.Settings.Default.logPath + @"\Weekly\" + DateTime.Now.Month.ToString() + DateTime.Now.Day.ToString() + DateTime.Now.Year.ToString() + "pricat.txt", sb.ToString());
                    }
                }
            }
        }

        static void CheckInventoryFile(string workingDir)
        {
            StringBuilder sb = new StringBuilder();

            FTPClientStuff.Download("Inventory.csv", Properties.Settings.Default.InventoryDownloadDir, workingDir);

            List<string> fileRows = File.ReadAllLines(Properties.Settings.Default.InventoryDownloadDir).ToList();

            if (fileRows.Count() > 0)
            {
                fileRows.RemoveAt(0);
            }

            foreach (string line in fileRows)
            {
                string[] wlines = line.Split(';');
                string ek_preis = wlines[2].Replace("\"", "").Replace(",", ".");
                string uvp = wlines[3].Replace("\"", "").Replace(",", ".");
                string artNumber = wlines[0].Replace("\"", "");

                UpdateBuilder update = Update.Set("doc.EK-Preis", ek_preis);
                WriteConcernResult wcr1 = Collection.Update(Query.EQ("doc.Hoffmann Artikelnr", artNumber), update);

                update = Update.Set("doc.UVP", uvp);
                WriteConcernResult wcr2 = Collection.Update(Query.EQ("doc.Hoffmann Artikelnr", artNumber), update);

                string stock = wlines[4].Replace("\"", "");
                int x = 0;
                Int32.TryParse(stock, out x);

                update = Update.Set("doc.Lagerbestand", x);
                WriteConcernResult wcr3 = Collection.Update(Query.EQ("doc.Hoffmann Artikelnr", artNumber), update);

                update = Update.Set("doc.Lagerbestand Ampel", wlines[5].Replace("\"", ""));
                WriteConcernResult wcr4 = Collection.Update(Query.EQ("doc.Hoffmann Artikelnr", artNumber), update);

                if (!wcr1.UpdatedExisting || !wcr2.UpdatedExisting || !wcr3.UpdatedExisting || !wcr4.UpdatedExisting)
                {
                    string message = String.Format("Hoffmann Inventory : Up Failed : {0}", artNumber);
                    Console.WriteLine(message);
                    sb.AppendLine(message);

                }
                else
                {
                    string message = String.Format("Hoffmann Inventory : Up Success : {0}", artNumber);
                    Console.WriteLine(message);
                    sb.AppendLine(message);
                }
            }

            File.WriteAllText(Properties.Settings.Default.logPath + @"\Hourly\" + DateTime.Now.Month.ToString() + DateTime.Now.Day.ToString() + DateTime.Now.Year.ToString() + "inventory.txt", sb.ToString());

        }

        static void AddToProductSizesCollection(string ean)
        {
            if (ean != string.Empty && ProductSizesCollection.FindOne(Query.EQ("ean", ean)) == null)
            {
                ProductSizesCollection.Insert(new ProductSizeEntity
                {
                    ean = ean,
                    measure = new BsonDocument()
                });
            }
        }


    }
}
