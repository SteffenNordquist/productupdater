﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Net;
using System.Net.FtpClient;
using System.Xml;
using System.IO;

using MongoDB.Bson;
using MongoDB.Driver;
using MongoDB.Driver.Builders;
using MongoDB.Driver.Linq;

using Newtonsoft.Json;
using Newtonsoft.Json.Converters;
using System.Threading;
using DN_Classes.AppStatus;

namespace BremerProductUpdate
{
    class Program
    {
        public static MongoCollection<BremerEntity> Collection;
        public static MongoCollection<ProductSizeEntity> ProductSizesCollection;

        public static string ServerIp = Properties.Settings.Default.Server;
        

        public static void SetCollection()
        {
            //MongoClient Client = new MongoClient(@"mongodb://" + ServerIp);
            Collection = new MongoClient(@"mongodb://client148:client148devnetworks@136.243.44.111/PD_Bremer").GetServer().GetDatabase("PD_Bremer").GetCollection<BremerEntity>("bremeritems");

            ProductSizesCollection = new MongoClient("mongodb://client148:client148devnetworks@136.243.44.111/ProductSize").GetServer().GetDatabase("ProductSize").GetCollection<ProductSizeEntity>("productsizes");

        }

        static void DeleteDuplicates()
        {
            var bremers = Collection.FindAll();
            bremers.SetFlags(QueryFlags.NoCursorTimeout);

            StringBuilder sb = new StringBuilder();
            foreach (var b in bremers)
            {
                string ean = b.bremeritem["ARTIKEL"]["SUPPLIER_AID"].AsString;
                var bremer = Collection.Find(Query.EQ("bremeritem.ARTIKEL.SUPPLIER_AID", ean));

                int count = (int)bremer.Count();
                if (count > 1)
                {
                    for (int x = 1; x < count; x++)
                    {
                        Collection.Remove(Query.EQ("bremeritem.ARTIKEL.SUPPLIER_AID", ean), RemoveFlags.Single);
                        Console.WriteLine("removed " + ean);
                        sb.AppendLine(ean);
                    }
                }
            }
            File.WriteAllText("removedduplicates.txt", sb.ToString());
        }

        //from bremeritem.ARTIKEL.ARTIKEL to bremeritem.ARTIKEL
        static void UpdateUnorganizedDocument()
        {
            var documents = Collection.Find(Query.Exists("bremeritem.ARTIKEL.ARTIKEL")).SetFlags(QueryFlags.NoCursorTimeout);

            foreach (var d in documents)
            {
                BsonDocument newDoc = d.bremeritem["ARTIKEL"].AsBsonDocument;
                UpdateBuilder update = Update.Set("bremeritem", newDoc);
                Collection.Update(Query.EQ("_id", d.id), update, UpdateFlags.Upsert);
            }

        }

        static void UpdateEanLength()
        {
            var documents = Collection.FindAll().SetFlags(QueryFlags.NoCursorTimeout).Where(item => item.bremeritem["ARTIKEL"]["SUPPLIER_AID"].AsString.Length == 12);
            foreach (var d in documents)
            {
                string ean = "0" + d.bremeritem["ARTIKEL"]["SUPPLIER_AID"].AsString;
                UpdateBuilder update = Update.Set("bremeritem.ARTIKEL.SUPPLIER_AID", ean);
                Collection.Update(Query.EQ("_id", d.id), update);
            }

            var documentss = Collection.FindAll().Where(item => item.bremeritem["ARTIKEL"]["SUPPLIER_AID"].AsString.Length == 8);
            foreach (var d in documentss)
            {
                string ean = "00000" + d.bremeritem["ARTIKEL"]["SUPPLIER_AID"].AsString;
                UpdateBuilder update = Update.Set("bremeritem.ARTIKEL.SUPPLIER_AID", ean);
                Collection.Update(Query.EQ("_id", d.id), update);
            }
        }

        static void Main(string[] args)
        {
            using (ApplicationStatus appStatus = new ApplicationStatus("BremerProductUpdate_148"))
            {
                try
                {

                    SetCollection();

                    //DeleteDuplicates();

                    //UpdateUnorganizedDocument();

                    //UpdateEanLength();

                    LZTfiles.Clear();
                    LAGfiles.Clear();
                    ARTfiles.Clear();

                    Console.WriteLine(String.Format("Art Last File Date : {0}", Properties.Settings.Default.ArtLastDateTime));
                    Console.WriteLine(String.Format("Lzt Last File Date : {0}", Properties.Settings.Default.LztLastDateTime));
                    Console.WriteLine(String.Format("Lag Last File Date : {0}", Properties.Settings.Default.LagLastDateTime));

                    Thread.Sleep(10000);

                    FtpClientStuff.SetCredentials();

                    if (!Directory.Exists(Properties.Settings.Default.logPath + @"\Logs\")) { Directory.CreateDirectory(Properties.Settings.Default.logPath + @"\Logs\"); }
                    Console.WriteLine("Starting Bremer Art Update...");
                    DoUpateART();


                    if (LZTfiles.Count() > 0)
                    {
                        Console.WriteLine("Starting Bremer Lzt Update : " + LZTfiles.Count() + " for update");
                        DoUpdateLZT();
                    }

                    if (LAGfiles.Count() > 0)
                    {
                        Console.WriteLine("Starting Bremer Lag Update : " + LAGfiles.Count() + " for update");
                        DoUpdateLAG();
                    }

                    CheckOldFiles(Properties.Settings.Default.ArtFileDirectory);
                    CheckOldFiles(Properties.Settings.Default.LztFileDirectory);
                    CheckOldFiles(Properties.Settings.Default.LagFileDirectory);

                    FtpClientStuff.DeleteFiles(LZTfiles);
                    FtpClientStuff.DeleteFiles(LAGfiles);
                    FtpClientStuff.DeleteFiles(ARTfiles);

                    appStatus.Successful();
                    appStatus.AddMessagLine("success");
                }
                catch (Exception ex)
                {
                    appStatus.AddMessagLine(ex.Message);
                    appStatus.AddMessagLine(ex.StackTrace);
                }

            }
        }

        static List<FtpListItem> LZTfiles = new List<FtpListItem>();
        static List<FtpListItem> LAGfiles = new List<FtpListItem>();
        static List<FtpListItem> ARTfiles = new List<FtpListItem>();

        public static void DoUpateART()
        {
            foreach (FtpListItem file in FtpClientStuff.GetFileList("in").Where(item => item.FullName.Contains(".XML")))
            {
                if (file.FullName.Contains("ART"))
                {
                    DateTime thisFileDT = FtpClientStuff.GetModifiedDateTime(file);
                    if (thisFileDT > Properties.Settings.Default.ArtLastDateTime)
                    {
                        if (FtpClientStuff.isFileExists(file.FullName.Replace(".XML", ".ok")) && FtpClientStuff.Download(file, Properties.Settings.Default.ArtFileDirectory + file.Name) == true)
                        {
                            ARTfiles.Add(file);
                            StringBuilder sb = new StringBuilder();
                            XmlDocument doc = new XmlDocument();
                            doc.Load(Properties.Settings.Default.ArtFileDirectory + file.Name);
                            foreach (XmlNode node in doc.DocumentElement.ChildNodes)
                            {
                                string jsonText = JsonConvert.SerializeXmlNode(node);
                                BsonDocument document = MongoDB.Bson.Serialization.BsonSerializer.Deserialize<BsonDocument>(jsonText);
                                UpdatedDocument(document,sb);
                            }
                            File.WriteAllText(Properties.Settings.Default.logPath + "/Logs/" + file.Name + ".txt", sb.ToString());

                        }

                        Properties.Settings.Default.ArtLastDateTime = thisFileDT;
                        Properties.Settings.Default.Save();
                    }
                }
                else { FilterFtpFile(file); }
            }
        }

        public static void UpdatedDocument(BsonDocument doc, StringBuilder sb)
        {
            string id = doc["ARTIKEL"][1].AsString;

            if (id.Length == 12) { id = "0" + id; }
            else if (id.Length == 8) { id = "00000" + id; }

            UpdateBuilder update = Update.Set("bremeritem", doc);
            Collection.Update(Query.EQ("bremeritem.ARTIKEL.SUPPLIER_AID", id), update, UpdateFlags.Upsert);
            Console.WriteLine(String.Format("Bremer Art Up : {0} : document update", id));

            AddToProductSizesCollection(id);

            //WriteConcernResult wcr = Collection.Update(Query.EQ("bremeritem.ARTIKEL.SUPPLIER_AID", id), update, UpdateFlags.Upsert);
            //if (wcr.UpdatedExisting)
            //{
            //    Console.WriteLine(id + " updated.");
            //    sb.AppendLine(id + " updated."); 
            //}
            //else
            //{
            //    Collection.Insert(new BremerEntity { bremeritem = doc });
            //    Console.WriteLine(id + " inserted.");
            //    sb.AppendLine(id + " inserted."); 
            //}
        }


        static void FilterFtpFile(FtpListItem file)
        {
            if (FtpClientStuff.isFileExists(file.FullName.Replace(".XML", ".ok")))
            {
                DateTime modified = FtpClientStuff.GetModifiedDateTime(file);

                if (file.FullName.Contains("LZT"))
                {
                    if (modified > Properties.Settings.Default.LztLastDateTime)
                    {
                        LZTfiles.Add(file);
                        Properties.Settings.Default.LztLastDateTime = modified;
                        Properties.Settings.Default.Save();
                    }
                }
                else if (file.FullName.Contains("LAG"))
                {
                    if (modified > Properties.Settings.Default.LagLastDateTime)
                    {
                        LAGfiles.Add(file);
                        Properties.Settings.Default.LagLastDateTime = modified;
                        Properties.Settings.Default.Save();
                    }
                }
            }
        }

        static void DoUpdateLZT()
        {
                foreach (FtpListItem file in LZTfiles)
                {
                    if (FtpClientStuff.Download(file, Properties.Settings.Default.LztFileDirectory + file.Name) == true)
                    {
                        StringBuilder sb = new StringBuilder();

                        XmlDocument doc = new XmlDocument();
                        doc.Load(Properties.Settings.Default.LztFileDirectory + file.Name);
                        //doc.Load("D:\\BVW_Kunde_LZT_20050918_000000.xml");
                        foreach (XmlNode node in doc.DocumentElement.ChildNodes)
                        {
                            string jsonText = JsonConvert.SerializeXmlNode(node);
                            BsonDocument document = MongoDB.Bson.Serialization.BsonSerializer.Deserialize<BsonDocument>(jsonText);
                            string supplierID = document["LIEFERZEIT"]["SUPPLIER_AID"].AsString;

                            if (supplierID.Length == 12) { supplierID = "0" + supplierID; }
                            else if (supplierID.Length == 8) { supplierID = "00000" + supplierID; }

                            string deliverytime = document["LIEFERZEIT"]["DELIVERY_TIME"].AsString;
                            UpdateBuilder update = Update.Set("plus.deliverytime", deliverytime);
                            WriteConcernResult wcr = Collection.Update(Query.EQ("bremeritem.ARTIKEL.SUPPLIER_AID", supplierID), update,UpdateFlags.Multi);
                            if (wcr.UpdatedExisting)
                            {
                                sb.AppendLine(supplierID + " updated: derliverytime => " + deliverytime);
                                Console.WriteLine(String.Format("Bremer Lzt Up : {0} : {1}", supplierID,deliverytime));
                            }
                            else
                            {
                                Console.WriteLine(String.Format("Bremer Lzt Up : {0} : update failed", supplierID));
                                sb.AppendLine(supplierID + " updated: failed.");
                            }

                        }

                        File.WriteAllText(Properties.Settings.Default.logPath + "/Logs/" + file.Name + ".txt", sb.ToString());
                    }
                }
            

        }


        static void DoUpdateLAG()
        {
            List<string> eansToUpdate = new List<string>();

                foreach (FtpListItem file in LAGfiles)
                {
                    if (FtpClientStuff.Download(file, Properties.Settings.Default.LagFileDirectory + file.Name) == true)
                    {
                        StringBuilder sb = new StringBuilder();

                        XmlDocument doc = new XmlDocument();
                        doc.Load(Properties.Settings.Default.LagFileDirectory + file.Name);
                        //doc.Load("D:\\BVW_Kunde_LAG_20050922_000000.xml");
                        string supplierID = "";
                        string quantity = "";
                        int counter = 0;
                        foreach (XmlNode node in doc.DocumentElement.ChildNodes)
                        {
                            counter++;
                            string jsonText = JsonConvert.SerializeXmlNode(node);
                            BsonDocument document = MongoDB.Bson.Serialization.BsonSerializer.Deserialize<BsonDocument>(jsonText);
                            if (counter == 1)
                            {
                                supplierID = document["SUPPLIER_AID"].AsString;

                                if (supplierID.Length == 12) { supplierID = "0" + supplierID; }
                                else if (supplierID.Length == 8) { supplierID = "00000" + supplierID; }
                                eansToUpdate.Add(supplierID);
                            }
                            else if (counter == 2)
                            {
                                quantity = document["QUANTITY"].AsString;
                                Console.WriteLine(supplierID + " " + quantity);
                                counter = 0;

                                UpdateBuilder update = Update.Set("plus.quantity", Convert.ToInt32(quantity));
                                WriteConcernResult wcr = Collection.Update(Query.EQ("bremeritem.ARTIKEL.SUPPLIER_AID", supplierID), update, UpdateFlags.Multi);
                                if (wcr.UpdatedExisting)
                                {
                                    sb.AppendLine(supplierID + " updated: quantity => " + quantity);
                                    Console.WriteLine(String.Format("Bremer Lag Up : {0} : {1}", supplierID, quantity));
                                }
                                else
                                {
                                    Console.WriteLine(String.Format("Bremer Lag Up : {0} : update failed", supplierID));
                                    sb.AppendLine(supplierID + " updated: failed.");
                                }
                            }
                        }

                        File.WriteAllText(Properties.Settings.Default.logPath + "/Logs/" + file.Name + ".txt", sb.ToString());
                    }
                }


                var documents = Collection.FindAll().SetFlags(QueryFlags.NoCursorTimeout) ;
                foreach (var d in documents)
                {
                    string ean = d.bremeritem["ARTIKEL"]["SUPPLIER_AID"].AsString;
                    if (!eansToUpdate.Exists(element => element == ean))
                    {
                        UpdateBuilder update = Update.Set("plus.quantity", 0);
                        Collection.Update(Query.EQ("bremeritem.ARTIKEL.SUPPLIER_AID", ean), update);
                        Console.WriteLine(String.Format("Bremer Lag Up : {0} : 0", ean));
                    }
                }
        }

        static void CheckOldFiles(string fileDirectory)
        {
            try
            {
                string[] filePaths = Directory.GetFiles(fileDirectory);
                foreach (string filepath in filePaths)
                {
                    FileInfo file = new FileInfo(filepath);
                    Console.WriteLine(file.FullName);
                    if (file.CreationTime < DateTime.Now.AddMonths(-3))
                    {
                        file.Delete();
                        Console.WriteLine("Local Dir: " + file.Name + " deleted.");
                    }
                }
            }catch{}

        }

        static void AddToProductSizesCollection(string ean)
        {
            if (ean != string.Empty && ProductSizesCollection.FindOne(Query.EQ("ean", ean)) == null)
            {
                ProductSizesCollection.Insert(new ProductSizeEntity
                {
                    ean = ean,
                    measure = new BsonDocument()
                });
            }
        }

    }
}
