﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;
using System.Net;
using System.Net.FtpClient;
namespace BremerProductUpdate
{
    public class FtpClientStuff
    {
        public static string FtpHost = Properties.Settings.Default.FtpHost;
        public static string FtpUserName = Properties.Settings.Default.FtpUserName;
        public static string FtpPassword = Properties.Settings.Default.FtpPassword;

        public static FtpClient ftpClient;

        public static void SetCredentials(){

            ftpClient = new FtpClient();
            ftpClient.Host = FtpHost;
            ftpClient.Credentials = new NetworkCredential(FtpUserName, FtpPassword);
            
            ftpClient.Connect();
            
        }

        public static FtpListItem[] GetFileList(string directory)
        {
            if (!ftpClient.IsConnected || ftpClient == null)
            {
                SetCredentials();
            }

            return ftpClient.GetListing(directory);


        }

        public static bool isFileExists(string path)
        {

            return ftpClient.FileExists(path);

        }

        public static bool Download(FtpListItem ftpListItem, string downloadPath)
        {
            try
            {
                using (var ftpStream = ftpClient.OpenRead(ftpListItem.FullName))
                {
                    using (var fileStream = File.Create(downloadPath, (int)ftpStream.Length))
                    {
                        var buffer = new byte[8 * 1024];
                        int count;
                        while ((count = ftpStream.Read(buffer, 0, buffer.Length)) > 0)
                        {
                            fileStream.Write(buffer, 0, count);
                        }

                    }
                }
                return true;
            }
            catch (Exception ex) { Console.WriteLine(ex.Message);  return false; }
        }

        public static DateTime GetModifiedDateTime(FtpListItem file)
        {
            return ftpClient.GetModifiedTime(file.FullName);
        }


        public static void DeleteFiles(List<FtpListItem> files)
        {
            foreach (FtpListItem file in files)
            {
                ftpClient.DeleteFile(file.FullName);
                ftpClient.DeleteFile(file.FullName.Replace(".XML", ".ok"));
                Console.WriteLine("Bvw Ftp File: "+file.Name + " deleted.");
            }
        }
        
    }
}
