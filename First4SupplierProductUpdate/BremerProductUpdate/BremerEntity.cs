﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MongoDB.Bson;
namespace BremerProductUpdate
{
    class BremerEntity
    {
        public ObjectId id { get; set; }
        public BsonDocument bremeritem { get; set; }
        public BsonDocument plus { get; set; }
    }
}
