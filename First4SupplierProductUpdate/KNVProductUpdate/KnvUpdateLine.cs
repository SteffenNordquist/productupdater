﻿using DN_Classes.Products.Price;
using MongoDB.Bson;
using ProcuctDB;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace KNVProductUpdate
{
    public class KnvUpdateLine
    {
        private List<string> _items = new List<string>(); 
        public KnvUpdateLine(string line)
        {
            
            _items = line.Split('|').ToList();
        }

        public string EAN
        {
            get
            {
                return _items[1];
            }
        }

        public int Stock {
            get {
                try
                {
                    return int.Parse(_items[3]);
                }
                catch
                {
                    return 0; 
                }
            }
        }

        public double Price
        {
            get
            {
                try
                {
                    return double.Parse(_items[5].Replace(",","."),CultureInfo.InvariantCulture);
                }
                catch
                {
                    return 0;
                }
            }
        }


        public double Discount
        {
            get
            {
                try
                {
                    return double.Parse(_items[7].Replace(",", "."), CultureInfo.InvariantCulture);
                }
                catch
                {
                    return 0;
                }
            }
        }


        public PriceType PriceType
        {
            get
            {
                if (_items[12].Trim() == "E") // G is fixedprice  - E is flexible
                {
                    return PriceType.flexiblePrice;
                }
                return PriceType.fixedPrice;
            }
        }


        public BsonDocument getPriceBson()
        {
            string priceTypeString = "04";
            if (PriceType == PriceType.flexiblePrice)
            {
                priceTypeString = "02";
            }

            BsonDocument getPriceBson = new BsonDocument
                    {
                        {"j148", priceTypeString},
                        {"j151", Price},
                        {"j267", Discount}
                    };

            return getPriceBson;
        }


        public bool NeedsUpdate(IProduct product)
        {
            bool priceUpdate = product.getPrice() != Price;
            bool discountUpdate = product.getDiscount() != Discount;
            bool stockUpdate = product.getStock() != Stock;
            bool priceTypeUpdate = product.getPriceType() != PriceType;

            if (priceUpdate || discountUpdate || stockUpdate || priceUpdate)
            {
                return true;
            }
            else {
                return false;
            }
        }



    }
}
