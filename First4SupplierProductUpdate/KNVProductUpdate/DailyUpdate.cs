﻿using MongoDB.Bson;
using MongoDB.Driver;
using MongoDB.Driver.Builders;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Xml;

namespace KNVProductUpdate
{
    public class DailyUpdate : IUpdate
    {
        public MongoCollection<Entity> Collection;

        public void StartUpdate(MongoCollection<Entity> Collection)
        {
            //Collection = new MongoClient("mongodb://client148:client148devnetworks@148.251.0.235/ProductDatabase").GetServer().GetDatabase("ProductDatabase").GetCollection<Entity>("knvbooks");
            this.Collection = Collection;

            DateTime lastDateTime = Properties.Settings.Default.lastDailyDateTime;

            string workingDir = "/doc/IDS/catalog/ONIX/Katalog-K/Updates/";


            //get files
            List<FileForSort> filelistdaily = new List<FileForSort>();
            string[] list = FTPClientStuff.DirectoryList(workingDir);
            foreach (var i in list)
            {
                if (i.Contains("xml"))
                {
                    DateTime d = FTPClientStuff.GetDateTimestampOnServer(workingDir , i.ToString());
                    
                    if (d > lastDateTime)
                    {
                        FileForSort f = new FileForSort();
                        f.name = i;
                        f.datetime = d;
                        filelistdaily.Add(f);
                    }
                }
            }
            filelistdaily = filelistdaily.OrderBy(x => x.datetime).ToList();



            //work each file
            foreach (FileForSort fd in filelistdaily)
            {
                
                    processfile(workingDir, fd.name);
                    Properties.Settings.Default.lastDailyDateTime = fd.datetime;
                    Properties.Settings.Default.Save();
                    Properties.Settings.Default.Reload();
                
            }
        }

        void processfile(string workingDir, string filename)
        {
            if (!Directory.Exists(Properties.Settings.Default.logPath + @"\knvdaily\"))
            {
                Directory.CreateDirectory(Properties.Settings.Default.logPath + @"\knvdaily\");
            }
            if (!Directory.Exists(Properties.Settings.Default.logPath + @"\knvdaily\logfile"))
            {
                Directory.CreateDirectory(Properties.Settings.Default.logPath + @"\knvdaily\logfile");
            }
            string logfilePath = String.Format(Properties.Settings.Default.logPath + @"\knvdaily\logfile\{0}.txt", filename);
            StringBuilder sb = new StringBuilder();

            string downloadPath = Properties.Settings.Default.logPath + @"\knvdaily\" + filename;
            bool downloaded =    FTPClientStuff.Download(workingDir, filename, downloadPath);
            if (downloaded)
            {
                sevenzip(downloadPath);

                XmlDocument xmldoc = new XmlDocument();

                try
                {
                    xmldoc.Load(downloadPath.Replace(".Z", ""));
                    xmldoc.CreateXmlDeclaration("1.0", "ISO-8859-1", null);
                }
                catch
                {
                    var lines = File.ReadAllLines(downloadPath.Replace(".Z", ""), Encoding.GetEncoding("ISO-8859-1"));
                    StringBuilder xmlSb = new StringBuilder();

                    int countLines = 0;
                    foreach (var line in lines)
                    {
                        if (countLines++ != 1)
                        {
                            xmlSb.AppendLine(line);
                        }
                    }
                    xmldoc.LoadXml(xmlSb.ToString());
                }


                foreach (XmlNode node in xmldoc.DocumentElement.ChildNodes)
                {
                    string jsonText = JsonConvert.SerializeXmlNode(node);
                    BsonDocument document = MongoDB.Bson.Serialization.BsonSerializer.Deserialize<BsonDocument>(jsonText);

                    if (document.ToString().Contains("product"))
                    {
                        string a001 = document["product"]["a001"].ToString();
                        var test = Collection.FindOne(Query.EQ("knvbook.product.a001", a001));
                        if (test != null)
                        {
                            var id = test.Id;
                            var update = Update.Set("knvbook", document);
                            Collection.Update(Query.EQ("_id", id), update);

                            sb.AppendLine(String.Format("{0} document updated.", a001));
                            Console.WriteLine(String.Format("Knvdaily : {0} document updated.", a001));
                        }
                        else
                        {
                            Entity entity = new Entity { knvbook = document, plus = new BsonDocument() };
                            Collection.Insert(entity);

                            sb.AppendLine(String.Format("{0} document inserted.", a001));
                            Console.WriteLine(String.Format("Knvdaily : {0} document inserted.", a001));
                        }

                    }
                }

                File.WriteAllText(logfilePath, sb.ToString());
            }
            
        }

        void sevenzip(string filename)
        {
            ProcessStartInfo startInfo = new ProcessStartInfo();
            startInfo.CreateNoWindow = true;
            startInfo.UseShellExecute = true;
            startInfo.WindowStyle = ProcessWindowStyle.Normal;
            //startInfo.WorkingDirectory = @"C:\suppliers\knv\knvdaily";
            startInfo.WorkingDirectory = Properties.Settings.Default.logPath + @"\knvdaily";
            startInfo.FileName = "cmd.exe";
            startInfo.Arguments = @"/C C:\7za.exe e " + filename;
            // Start the process with the info we specified.
            // Call WaitForExit and then the using statement will close.
            using (Process exeProcess = Process.Start(startInfo))
            {
                exeProcess.WaitForExit();
                Console.WriteLine("File Unzipped");
            }
        }

    }
}
