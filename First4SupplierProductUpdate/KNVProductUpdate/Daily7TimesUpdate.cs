﻿using MongoDB.Bson;
using MongoDB.Driver;
using ProcuctDB;
using ProcuctDB.Knv;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace KNVProductUpdate
{
    public class Daily7TimesUpdate : IUpdate
    {
        public void StartUpdate(MongoCollection<Entity> Collection)
        {
            StringBuilder sb = new StringBuilder();
            sb.AppendLine(DateTime.Now.ToString());

            string workingDir = "/doc/IDS/catalog/KNVupd/BestK/";

            bool dirListingError = false;

            string[] fileListFromFtp = new string[]{""};
            try { fileListFromFtp = FTPClientStuff.DirectoryList(workingDir); }
            catch { dirListingError = true; }

            if (!dirListingError && fileListFromFtp.Count() > 0)
            {
                string fileToBeReadFromFtp = string.Empty;
                foreach (string file in fileListFromFtp)
                {
                    if (!file.Contains(".ok") || FTPClientStuff.Exist(workingDir, file))
                    {
                        fileToBeReadFromFtp = file;
                        break;
                    }
                }


                if (!Directory.Exists(Properties.Settings.Default.logPath + @"\knvhourly\"))
                {
                    Directory.CreateDirectory(Properties.Settings.Default.logPath + @"\knvhourly\");
                }
                if (!Directory.Exists(Properties.Settings.Default.logPath + @"\knvhourly\logfile"))
                {
                    Directory.CreateDirectory(Properties.Settings.Default.logPath + @"\knvhourly\logfile");
                }

                string downloadPath = Properties.Settings.Default.logPath + @"\knvhourly\" + fileToBeReadFromFtp;
                if (FTPClientStuff.Download(workingDir, fileToBeReadFromFtp, downloadPath))
                {

                    FileInfo downloadFile = new FileInfo(downloadPath);

                    int counter = 0;
                    while (IsFileLocked(downloadFile))
                    {
                        Console.WriteLine("File being used, waiting few seconds.");
                        Thread.Sleep(10000);
                        counter++;
                        if (counter > 360)//after 1 hour
                        {
                            break;
                        }
                    }

                    List<string> fileRows = File.ReadAllLines(downloadPath).ToList();
                    if (fileRows.Count() > 1)
                    {

                        fileRows.RemoveAt(0);
                        Dictionary<string, KnvUpdateLine> knvUpdateLineDict = new Dictionary<string, KnvUpdateLine>();
                        foreach (string row in fileRows)
                        {
                            KnvUpdateLine knvUpdateLine = new KnvUpdateLine(row);
                            if (!knvUpdateLineDict.ContainsKey(knvUpdateLine.EAN))
                            {
                                knvUpdateLineDict.Add(knvUpdateLine.EAN, knvUpdateLine);
                            }
                        }

                        KnvDB knvDB = new KnvDB(Properties.Settings.Default.ServerIP);
                        List<IProduct> listStockGreaterZero = knvDB.FindStockGreaterZero();

                        ParallelOptions po = new ParallelOptions();
                        po.MaxDegreeOfParallelism = 8;

                        var _lockThis = new object();

                        Parallel.ForEach(listStockGreaterZero, po, product =>
                        {
                            KnvUpdateLine knvUpdateLine = null;
                            foreach (var ean in product.GetEanList())
                            {
                                if (knvUpdateLineDict.ContainsKey(ean))
                                {
                                    knvUpdateLine = knvUpdateLineDict[ean];
                                }
                            }

                            if (knvUpdateLine != null)
                            {
                                lock (_lockThis)
                                {
                                    knvUpdateLineDict.Remove(knvUpdateLine.EAN);
                                }
                                string ean = knvUpdateLine.EAN;
                                int stock = knvUpdateLine.Stock;

                                if (knvUpdateLine.NeedsUpdate(product))
                                {
                                    BsonDocument price = knvUpdateLine.getPriceBson();
                                    knvDB.UpdateItem(ean, stock, price);
                                    lock (_lockThis)
                                    {
                                        sb.AppendLine(String.Format("{0}\t{1}", ean, stock));
                                    }
                                    Console.WriteLine(String.Format("Knvhourly : {0} : {1}", ean, stock));
                                }
                                else
                                {
                                    knvDB.UpdateStockDate(ean);
                                    lock (_lockThis)
                                    {
                                        sb.AppendLine(String.Format("{0}\t{1}", ean, stock));
                                    }
                                    Console.WriteLine(String.Format("Knvhourly : {0} : {1}", ean, stock));
                                }
                            }
                            else
                            {
                                if (product.GetEanList().Count() > 0)
                                {
                                    knvDB.UpdateItem(product.GetEanList()[0], 0, null);
                                    lock (_lockThis)
                                    {
                                        sb.AppendLine(String.Format("{0}\t{1}", product.GetEanList()[0], 0));
                                    }
                                    Console.WriteLine(String.Format("Knvhourly : {0} : {1}", product.GetEanList()[0], 0));
                                }
                            }
                        });


                        Parallel.ForEach(knvUpdateLineDict, po, knvUpdateLineKV =>
                        {
                            if (knvUpdateLineKV.Value.Stock > 0)
                            {
                                string ean = knvUpdateLineKV.Key;
                                int stock = knvUpdateLineKV.Value.Stock;
                                BsonDocument price = knvUpdateLineKV.Value.getPriceBson();
                                knvDB.UpdateItem(ean, stock, price);
                                lock (_lockThis)
                                {
                                    sb.AppendLine(String.Format("{0}\t{1}", ean, stock));
                                }
                                Console.WriteLine(String.Format("Knvhourly : {0} : {1}", ean, stock));
                            }
                        });


                        File.WriteAllText(Properties.Settings.Default.logPath + @"\knvhourly\logfile\log_" + DateTime.Now.Ticks + ".txt", sb.ToString());

                    }
                }
            }
        }

        protected virtual bool IsFileLocked(FileInfo file)
        {
            FileStream stream = null;

            try
            {
                stream = file.Open(FileMode.Open, FileAccess.ReadWrite, FileShare.None);
            }
            catch (IOException)
            {
                //the file is unavailable because it is:
                //still being written to
                //or being processed by another thread
                //or does not exist (has already been processed)
                return true;
            }
            finally
            {
                if (stream != null)
                    stream.Close();
            }

            //file is not locked
            return false;
        }
    }
}
