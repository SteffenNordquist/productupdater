﻿using MongoDB.Bson;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace KNVProductUpdate
{
    public class ProductSizeEntity
    {
        public ObjectId id { get; set; }
        public string ean { get; set; }
        public BsonDocument measure { get; set; }
    }
}
