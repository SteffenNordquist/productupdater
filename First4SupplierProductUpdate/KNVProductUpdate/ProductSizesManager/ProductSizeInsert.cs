﻿using MongoDB.Driver;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MongoDB.Bson;
using MongoDB.Driver.Builders;

namespace KNVProductUpdate.ProductSizesManager
{
    public class ProductSizeInsert : IProductSizeInsert
    {
        private readonly MongoCollection<ProductSizeEntity> _productSizesCollection;

        public ProductSizeInsert(MongoCollection<ProductSizeEntity> productSizesCollection)
        {
            this._productSizesCollection = productSizesCollection;
        }

        public void Insert(string ean)
        {
            if (_productSizesCollection.FindOne(Query.EQ("ean", ean)) == null)
            {
                _productSizesCollection.Insert(new ProductSizeEntity
                {
                    ean = ean,
                    measure = new BsonDocument()
                });
            }
        }
    }
}
