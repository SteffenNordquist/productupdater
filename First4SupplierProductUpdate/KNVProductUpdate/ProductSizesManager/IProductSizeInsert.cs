﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace KNVProductUpdate.ProductSizesManager
{
    public interface IProductSizeInsert
    {
        void Insert(string ean);
    }
}
