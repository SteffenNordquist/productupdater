﻿using MongoDB.Bson;
using ProcuctDB.Knv;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml;
using MongoDB;
using MongoDB.Driver;
using Newtonsoft.Json;
using MongoDB.Driver.Builders;
using System.IO;
using DN.DataAccess.ConfigurationData;
using DN_Classes.Logger;
using KNVProductUpdate.Parser;
using KNVProductUpdate.Unzip;
using KNVProductUpdate.ProductSizesManager;

namespace KNVProductUpdate.Updates
{
    public class MonthlyUpdate : IUpdate
    {

        private readonly MongoCollection<Entity> _collection;
        private readonly IProductSizeInsert _productSizeInsert;
        private readonly IConfigurationManager _appConfiguration;
        private readonly IEanParser _eanParser;
        private readonly IUnzipper _unzipper;


        public MonthlyUpdate(MongoCollection<Entity> collection,
            IConfigurationManager appConfiguration,
            IEanParser eanParser,
            IUnzipper unzipper,
            IProductSizeInsert productSizeInsert)
        {
            this._collection = collection;
            this._productSizeInsert = productSizeInsert;
            this._appConfiguration = appConfiguration;
            this._eanParser = eanParser;
            this._unzipper = unzipper;
        }

        public void StartUpdate()
        {
            //DateTime lastDateTime = Properties.Settings.Default.lastMonthlyDateTime;
            DateTime lastDateTime = DateTime.Parse(_appConfiguration.GetValue("lastMonthlyDateTime"), CultureInfo.InvariantCulture);

            //string workingDir = "/doc/IDS/catalog/ONIX/Katalog-K/Gesamt/";
            string workingDir = _appConfiguration.GetValue("monthlyUpdateFtpDirectory");

            //getting files
            List<FileForSort> filelist = new List<FileForSort>();
            
            string[] ftpFileList = FTPClient.DirectoryList(workingDir);
            if (ftpFileList != null && ftpFileList.Any())
            {
                foreach (var ftpFile in ftpFileList)
                {
                    if (ftpFile.Contains("xml"))
                    {
                        DateTime d = FTPClient.GetDateTimestampOnServer(workingDir, ftpFile.ToString());
                        if (d > lastDateTime)
                        {
                            FileForSort f = new FileForSort();
                            f.name = ftpFile;
                            f.datetime = d;
                            filelist.Add(f);
                        }
                    }
                }
                filelist = filelist.OrderBy(x => x.datetime).ToList();



                //work each file
                foreach (FileForSort fd in filelist)
                {
                    Processfile(workingDir, fd.name);
                    _appConfiguration.SetValue("lastMonthlyDateTime", fd.datetime.ToString(CultureInfo.InvariantCulture));
                    //Properties.Settings.Default.lastMonthlyDateTime = fd.datetime;
                    //Properties.Settings.Default.Save();
                    //Properties.Settings.Default.Reload();

                }

            }
        }

        void Processfile(string workingDir, string filename)
        {
            AppLogger.Info(filename);

            string downloadPath = _appConfiguration.GetValue("appPath") + @"\knvmonthly\" + filename;
            
            bool downloaded = FTPClient.Download(workingDir, filename, downloadPath);

            if (downloaded)
            {
                _unzipper.Unzip(_appConfiguration.GetValue("appPath") + @"\knvmonthly\", filename);

                XmlDocument xmldoc = new XmlDocument();
                try
                {
                    xmldoc.Load(downloadPath.Replace(".Z", ""));
                    xmldoc.CreateXmlDeclaration("1.0", "ISO-8859-1", null);
                }
                catch(Exception ex)
                {
                    var lines = File.ReadAllLines(downloadPath.Replace(".Z", ""), Encoding.GetEncoding("ISO-8859-1"));
                    StringBuilder xmlSb = new StringBuilder();

                    int countLines = 0;
                    foreach (var line in lines)
                    {
                        if (countLines++ != 1)
                        {
                            xmlSb.AppendLine(line);
                        }
                    }
                    xmldoc.LoadXml(xmlSb.ToString());
                }


                ParallelOptions po = new ParallelOptions();

                List<XmlNode> xmlNodeList = new List<XmlNode>();
                foreach (XmlNode xmlNode in xmldoc.DocumentElement.ChildNodes)
                {
                    xmlNodeList.Add(xmlNode);
                }


                // ok test
                po.MaxDegreeOfParallelism = 8;
                //Parallel.ForEach(xmlNodeList, po, node =>
                foreach(var node in xmlNodeList)
                {
                    string jsonText = JsonConvert.SerializeXmlNode(node);
                    BsonDocument document = MongoDB.Bson.Serialization.BsonSerializer.Deserialize<BsonDocument>(jsonText);

                    if (document.ToString().Contains("product"))
                    {
                        string a001 = document["product"]["a001"].ToString();
                        var test = _collection.FindOne(Query.EQ("knvbook.product.a001", a001));
                        if (test != null)
                        {
                            var id = test.Id;
                            var update = Update.Set("knvbook", document);
                            _collection.Update(Query.EQ("_id", id), update);

                            AppLogger.Info(String.Format("{0} document updated.", a001));
                        }
                        else
                        {
                            Entity entity = new Entity { knvbook = document, plus = new BsonDocument() };
                            _collection.Insert(entity);

                            _productSizeInsert.Insert(_eanParser.Parse(document));

                            AppLogger.Info(String.Format("{0} document inserted.", a001));
                        }

                    }
                }
                //);

            }
        }

    }
}
