﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using KNVProductUpdate.Parser;
using MongoDB.Bson;

namespace KNVProductUpdate.Updates
{
    public class EanParser : IEanParser
    {
        public string Parse(BsonDocument document)
        {
            string ean = "";

            if (document["product"].AsBsonDocument.Contains("productidentifier"))
            {
                if (document["product"]["productidentifier"].IsBsonArray)
                {
                    foreach (BsonDocument p in document["product"]["productidentifier"].AsBsonArray)
                    {
                        if (p["b244"].AsString.Length == 13)
                        {
                            ean = p["b244"].AsString;
                        }
                    }
                }
                else if (document["product"]["productidentifier"].AsBsonDocument.Contains("b244"))
                {
                    ean = document["product"]["productidentifier"]["b244"].AsString;

                }
            }
            return ean;
        }
    }
}
