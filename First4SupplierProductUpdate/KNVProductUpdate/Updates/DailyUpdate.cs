﻿using MongoDB.Bson;
using MongoDB.Driver;
using MongoDB.Driver.Builders;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Xml;
using DN.DataAccess.ConfigurationData;
using DN_Classes.Logger;
using KNVProductUpdate.Unzip;

namespace KNVProductUpdate.Updates
{
    public class DailyUpdate : IUpdate
    {
        public MongoCollection<Entity> _collection;
        private readonly IConfigurationManager _appConfiguration;
        private readonly IUnzipper _unzipper;

        public DailyUpdate(MongoCollection<Entity> collection, IConfigurationManager appConfiguration, IUnzipper unzipper)
        {
            this._collection = collection;
            this._appConfiguration = appConfiguration;
            this._unzipper = unzipper;
        }

        public void StartUpdate()
        {
            //DateTime lastDateTime = Properties.Settings.Default.lastDailyDateTime;
            DateTime lastDateTime = DateTime.Parse(_appConfiguration.GetValue("lastDailyDateTime"), CultureInfo.InvariantCulture);

            //string workingDir = "/doc/IDS/catalog/ONIX/Katalog-K/Updates/";
            string workingDir = _appConfiguration.GetValue("dailyUpdateFtpDirectory");

            //get files
            List<FileForSort> filelistdaily = new List<FileForSort>();
            string[] ftpFileList = FTPClient.DirectoryList(workingDir);
            if (ftpFileList != null && ftpFileList.Any())
            {
                foreach (var ftpFile in ftpFileList)
                {
                    if (ftpFile.Contains("xml"))
                    {
                        DateTime d = FTPClient.GetDateTimestampOnServer(workingDir, ftpFile.ToString());

                        if (d > lastDateTime)
                        {
                            FileForSort f = new FileForSort();
                            f.name = ftpFile;
                            f.datetime = d;
                            filelistdaily.Add(f);
                        }
                    }
                }
                filelistdaily = filelistdaily.OrderBy(x => x.datetime).ToList();

                foreach (FileForSort fd in filelistdaily)
                {

                    Processfile(workingDir, fd.name);
                    _appConfiguration.SetValue("lastDailyDateTime", fd.datetime.ToString(CultureInfo.InvariantCulture));
                    //Properties.Settings.Default.lastDailyDateTime = fd.datetime;
                    //Properties.Settings.Default.Save();
                    //Properties.Settings.Default.Reload();

                }
            }
        }

        void Processfile(string workingDir, string filename)
        {
            string downloadPath = _appConfiguration.GetValue("appPath") + @"\knvdaily\" + filename;
            bool downloaded =    FTPClient.Download(workingDir, filename, downloadPath);
            if (downloaded)
            {
                _unzipper.Unzip(_appConfiguration.GetValue("appPath") + @"\knvdaily\", filename);

                XmlDocument xmldoc = new XmlDocument();

                try
                {
                    xmldoc.Load(downloadPath.Replace(".Z", ""));
                    xmldoc.CreateXmlDeclaration("1.0", "ISO-8859-1", null);
                }
                catch
                {
                    var lines = File.ReadAllLines(downloadPath.Replace(".Z", ""), Encoding.GetEncoding("ISO-8859-1"));
                    StringBuilder xmlSb = new StringBuilder();

                    int countLines = 0;
                    foreach (var line in lines)
                    {
                        if (countLines++ != 1)
                        {
                            xmlSb.AppendLine(line);
                        }
                    }
                    xmldoc.LoadXml(xmlSb.ToString());
                }


                foreach (XmlNode node in xmldoc.DocumentElement.ChildNodes)
                {
                    string jsonText = JsonConvert.SerializeXmlNode(node);
                    BsonDocument document = MongoDB.Bson.Serialization.BsonSerializer.Deserialize<BsonDocument>(jsonText);

                    if (document.ToString().Contains("product"))
                    {
                        string a001 = document["product"]["a001"].ToString();
                        var test = _collection.FindOne(Query.EQ("knvbook.product.a001", a001));
                        if (test != null)
                        {
                            var id = test.Id;
                            var update = Update.Set("knvbook", document);
                            _collection.Update(Query.EQ("_id", id), update);

                            AppLogger.Info(String.Format("{0} document updated.", a001));
                        }
                        else
                        {
                            Entity entity = new Entity { knvbook = document, plus = new BsonDocument() };
                            _collection.Insert(entity);

                            AppLogger.Info(String.Format("{0} document inserted.", a001));
                        }

                    }
                }
            }
            
        }

    }
}
