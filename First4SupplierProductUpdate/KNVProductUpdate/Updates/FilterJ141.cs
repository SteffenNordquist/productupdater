﻿using MongoDB.Driver;
using MongoDB.Driver.Builders;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace KNVProductUpdate.Updates
{
    public class FilterJ141 : IUpdate
    {
        public MongoCollection<Entity> _collection;

        public FilterJ141(MongoCollection<Entity> collection)
        {
            this._collection = collection;
        }

        public void StartUpdate()
        {
            _collection.Update(Query.And(Query.Exists("knvbook.product.supplydetail.j141"), Query.GT("knvbook.product.supplydetail.stock.j350", 0)), Update.Set("knvbook.product.supplydetail.stock.j350", 0), UpdateFlags.Multi);
            
        }
    }
}
