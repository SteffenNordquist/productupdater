﻿using MongoDB.Driver;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace KNVProductUpdate
{
    public interface IUpdate
    {
        void StartUpdate(MongoCollection<Entity> Collection);
    }
}
