﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DN_Classes.Logger;

namespace KNVProductUpdate.Unzip
{
    public class SevenZip : IUnzipper
    {
        public void Unzip(string path, string filename)
        {
            ProcessStartInfo startInfo = new ProcessStartInfo();
            startInfo.CreateNoWindow = true;
            startInfo.UseShellExecute = true;
            startInfo.WindowStyle = ProcessWindowStyle.Normal;
            //startInfo.WorkingDirectory = @"C:\suppliers\knv\knvmonthly";
            startInfo.WorkingDirectory = path;
            startInfo.FileName = "cmd.exe";
            startInfo.Arguments = @"/C C:\7za.exe e " + filename;
            // Start the process with the info we specified.
            // Call WaitForExit and then the using statement will close.
            using (Process exeProcess = Process.Start(startInfo))
            {
                exeProcess.WaitForExit();
                AppLogger.Info("Unzipped : " + filename);
            }
        }
    }
}
