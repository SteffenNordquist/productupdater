﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace KNVProductUpdate.Unzip
{
    public interface IUnzipper
    {
        void Unzip(string path, string filename);
    }
}
