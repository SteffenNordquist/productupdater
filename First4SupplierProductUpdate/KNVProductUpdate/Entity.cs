﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using MongoDB.Bson;
using MongoDB.Driver;
using MongoDB.Driver.Builders;
using MongoDB.Driver.GridFS;
using MongoDB.Driver.Linq;

namespace KNVProductUpdate
{
    public class Entity
    {
        public ObjectId Id { get; set; }
        public BsonDocument knvbook { get; set; }
        public BsonDocument plus { get; set; }
        public BsonNull title { get; set; }
    }
}

