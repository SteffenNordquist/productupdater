﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;
using System.Net;
using System.Diagnostics;
using System.Threading;
using System.Globalization;
using MongoDB.Bson;
using ProcuctDB.Knv;
using ProcuctDB;
using MongoDB.Driver;
using MongoDB.Driver.Builders;
using MongoDB.Bson.Serialization;
using Newtonsoft.Json.Linq;
using Newtonsoft.Json;
using DN_Classes.Entities;
using DN_Classes.AppStatus;
using DN_Classes.Logger;
using DN.DataAccess.ConfigurationData;
using KNVProductUpdate.ProductSizesManager;
using KNVProductUpdate.Unzip;
using KNVProductUpdate.Updates;

namespace KNVProductUpdate
{
    class Program
    {
        public static void Main(string[] args)
        {
            string configFile = AppDomain.CurrentDomain.BaseDirectory + @"\AppConfiguration.config";
            IConfigurationSource configSource = new ConfigurationSource(configFile);
            IConfigurationManager appConfiguration = new AppConfigurationManager(configSource);

            ILogFileLocation logFileLocation = new DefaultLogFileLocation { LogDirectory = appConfiguration.GetValue("LogDirectory"), ToolName = "KnvProductUpdate", SubToolName = "KnvProductUpdate" };

            using (ILogger logger = new DefaultLogger(logFileLocation))
            {
                using (ApplicationStatus appStatus = new ApplicationStatus("KNVProductUpdate_148"))
                {
                    try
                    {
                        logger.Info(String.Format("last monthly file date: {0}\nlast daily file date: {1}",
                            appConfiguration.GetValue("lastMonthlyDateTime"),
                            appConfiguration.GetValue("lastDailyDateTime")));

                        FTPClient.ftphost = appConfiguration.GetValue("ftphost");
                        FTPClient.ftpusername = appConfiguration.GetValue("ftpusername");
                        FTPClient.ftppassword = appConfiguration.GetValue("ftppassword");

                        MongoCollection<Entity> Collection =
                            new MongoClient(appConfiguration.GetValue("KnvDBConnectionStringFormat"))
                                .GetServer()
                                .GetDatabase(appConfiguration.GetValue("KnvDBNameFormat"))
                                .GetCollection<Entity>(appConfiguration.GetValue("KnvCollectionName"));

                        MongoCollection<ProductSizeEntity> ProductCollectionSizesCollection =
                            new MongoClient(appConfiguration.GetValue("ProductSizesDBConnectionStringFormat"))
                                .GetServer()
                                .GetDatabase(appConfiguration.GetValue("ProductSizesDBNameFormat"))
                                .GetCollection<ProductSizeEntity>(appConfiguration.GetValue("ProductSizesCollectionName"));

                        IProductSizeInsert productSizeInsert = new ProductSizeInsert(ProductCollectionSizesCollection);

                        IUnzipper unzipper = new SevenZip();

                        logger.Info("****************monthly update******************");
                        new MonthlyUpdate(
                            Collection,
                            appConfiguration, 
                            new EanParser(),
                            unzipper,
                            productSizeInsert).StartUpdate();

                        logger.Info("****************daily update******************");
                        new DailyUpdate(Collection, appConfiguration, unzipper).StartUpdate();

                        logger.Info("****************daily 7 times update******************");
                        new Daily7TimesUpdate(Collection, appConfiguration).StartUpdate();

                        logger.Info("****************j141 filter update******************");
                        new FilterJ141(Collection).StartUpdate();

                        appStatus.Successful();
                        appStatus.AddMessagLine("Success");
                    }
                    catch (Exception ex)
                    {
                        appStatus.AddMessagLine(ex.Message + "\n" + ex.StackTrace);
                        logger.Error(ex.Message + "\n" + ex.StackTrace);
                    }
                }
            }
        }

        

       

    }
}
