﻿using MongoDB.Driver;
using MongoDB.Driver.Builders;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace KNVProductUpdate
{
    public class FilterJ141 : IUpdate
    {
        public void StartUpdate(MongoDB.Driver.MongoCollection<Entity> Collection)
        {
            
            Console.WriteLine("filtering j141...");
            Collection.Update(Query.And(Query.Exists("knvbook.product.supplydetail.j141"), Query.GT("knvbook.product.supplydetail.stock.j350", 0)), Update.Set("knvbook.product.supplydetail.stock.j350", 0), UpdateFlags.Multi);
            
        }
    }
}
