﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Net;
using System.Text.RegularExpressions;
using System.Threading;
using MongoDB;
using MongoDB.Bson;
using MongoDB.Driver;
using MongoDB.Driver.Builders;
using MongoDB.Driver.Linq;
using ProcuctDB;
using System.IO;
using System.Globalization;
namespace LibriProductUpdate
{
    class Program
    {
        static void Main()
        {
            using (DN_Classes.AppStatus.ApplicationStatus appStatus = new DN_Classes.AppStatus.ApplicationStatus("LibriProductUpdate_148"))
            {
                try
                {
                    IUpdate update = null;

                    DateTime lastHourlyUpdateDate = Properties.Settings.Default.lasthourlyupdatetime;
                    DateTime lastDailyUpdateDate = Properties.Settings.Default.lastdailytime;
                    //Properties.Settings.Default.lasthourlyimportnumber = 2597;
                    //Properties.Settings.Default.lasthourlyupdatetime = DateTime.Now.AddHours(-10);
                    //Properties.Settings.Default.lastdailytime = DateTime.Now.AddHours(-11);
                    //Properties.Settings.Default.Save();
                    //Console.ReadLine();
                    string lastnumber = Properties.Settings.Default.lasthourlyimportnumber;



                    Console.WriteLine(String.Format("Last Hourly Up Date: {0}\nLast Daily Up Date: {1}\nLast Hourly Import Number: {2}", lastHourlyUpdateDate, lastDailyUpdateDate, lastnumber));
                    Thread.Sleep(10000);


                    if (!Directory.Exists(Properties.Settings.Default.logPath + @"\HourlyImport\")) { Directory.CreateDirectory(Properties.Settings.Default.logPath + @"\HourlyImport\"); }
                    if (!Directory.Exists(Properties.Settings.Default.logPath + @"\HourlyImport\logfile\")) { Directory.CreateDirectory(Properties.Settings.Default.logPath + @"\HourlyImport\logfile\"); }
                    if (!File.Exists(Properties.Settings.Default.logPath + @"\HourlyImport\logfile\log.txt")) { File.Create(Properties.Settings.Default.logPath + @"\HourlyImport\logfile\log.txt"); }
                    update = new HourlyImportONL();
                    Console.WriteLine("Starting Libri hourly import update...");
                    update.StartUpdate();


                    if (!Directory.Exists(Properties.Settings.Default.logPath + @"\DailyUpdate\")) { Directory.CreateDirectory(Properties.Settings.Default.logPath + @"\DailyUpdate\"); }
                    update = new DailyUpdate();//daily should be first from DailyUpdate(), ask wylan why :)
                    Console.WriteLine("Starting Libri daily update...");
                    update.StartUpdate();

                    if (!Directory.Exists(Properties.Settings.Default.logPath + @"\DailyPrice\")) { Directory.CreateDirectory(Properties.Settings.Default.logPath + @"\DailyPrice\"); }
                    update = new DailyPrice();
                    Console.WriteLine("Starting daily price...");
                    update.StartUpdate();

                    if (!Directory.Exists(Properties.Settings.Default.logPath + @"\HourlyUpdate\")) { Directory.CreateDirectory(Properties.Settings.Default.logPath + @"\HourlyUpdate\"); }
                    update = new HourlyUpdate();
                    Console.WriteLine("Starting Libri hourly update...");
                    update.StartUpdate();

                    //j141 filter
                    Console.WriteLine("filtering j141...");
                    MongoLibriDatabase.GetCollectionOnx().Update(Query.And(Query.Exists("doc.product.supplydetail.j141"), Query.GT("doc.product.supplydetail.stock.j350", 0)), Update.Set("doc.product.supplydetail.stock.j350", 0), UpdateFlags.Multi);
                    //

                    if (!Directory.Exists(Properties.Settings.Default.logPath + @"\ProductDescription\")) { Directory.CreateDirectory(Properties.Settings.Default.logPath + @"\ProductDescription\"); }
                    update = new ProductDescription();
                    Console.WriteLine("Starting Product Description...");
                    update.StartUpdate();

                    appStatus.Successful();
                    appStatus.AddMessagLine("success");
                }
                catch (Exception ex)
                {
                    appStatus.AddMessagLine(ex.Message + "\n" + ex.StackTrace);
                }

            }
        }
    }
}
