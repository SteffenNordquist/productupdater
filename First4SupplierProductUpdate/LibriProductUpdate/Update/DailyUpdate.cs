﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;
using System.Threading;
using System.Text.RegularExpressions;

using MongoDB.Bson;
using MongoDB.Driver;
using MongoDB.Driver.Builders;
using ProcuctDB;
namespace LibriProductUpdate
{
    public class DailyUpdate : IUpdate
    {
        string workingDirectory = "/STK/BIL/";
        DateTime lastDateTime = Properties.Settings.Default.lastdailytime;
        MongoCollection<LibriEntity> productCollection;

        public void StartUpdate()
        {
            productCollection = MongoLibriDatabase.GetCollectionOnx();

            List<FileSort> fileList = FtpClientStuff.GetFileList(workingDirectory, false);
            //List<FileSort> fileList = FtpClientStuff.ReusableFileList;

            foreach (FileSort file in fileList)
            {
                if (file.name.Contains("LUSTK") && file.datetime > lastDateTime && file.datetime > DateTime.Now.AddDays(-7))
                {
                    DoUpdate(file.name, "");
                    SaveLastDateTime(file.datetime);
                }
            }

        }

        public void SaveLastDateTime(DateTime newDateTime)
        {
            Properties.Settings.Default.lastdailytime = newDateTime;
            Properties.Settings.Default.Save();
            Properties.Settings.Default.Reload();
            File.WriteAllText("log_daily.txt", String.Format("Latest File for update date: {0}", newDateTime));
            Console.WriteLine(String.Format("Latest File for update date: {0}", newDateTime));
        }

        public void DoUpdate(string filename, string temp)
        {
            List<string> eanListStockGreaterZero = GetEanListOfStockNotZero();

            List<string> eansUpdated = new List<string>();

            string localfile = Properties.Settings.Default.logPath + @"\DailyUpdate\LUSTK.txt";

            StringBuilder sb_log = new StringBuilder();
            sb_log.AppendLine(DateTime.Now.ToString());
            sb_log.AppendLine(localfile);


                FtpClientStuff.Download(workingDirectory + filename, localfile,false);

                List<string> lines = File.ReadAllLines(localfile).ToList();
                lines.RemoveAt(0);

                foreach (string l in lines)
                {

                    string[] line = l.Split(';');
                    string ean = line[0];
                    int stock = Convert.ToInt32(line[5]);

                    UpdateBuilder update = Update.Set("plus.stock.j350", stock).Set("plus.stock.stockdate", DateTime.Now);

                    eansUpdated.Add(ean);

                    //if (!eanListStockGreaterZero.Contains(ean))
                    //{
                        productCollection.Update(Query.EQ("doc.product.productidentifier.b244", ean), update, UpdateFlags.Multi);
                        Console.WriteLine("libriupdate : " + ean + " from 0 to " + stock);
                        sb_log.AppendLine("libriupdate : " + ean + " from 0 to " + stock);
                        
                    //}
                    //else
                    //{
                    //    productCollection.Update(Query.EQ("doc.product.productidentifier.b244", ean), update, UpdateFlags.Multi);
                    //    Console.WriteLine("libriupdate : " + ean + " to " + stock);
                    //    sb_log.AppendLine("libriupdate : " + ean + " to " + stock);
                    //}

                }


                foreach (string ean in eanListStockGreaterZero)
                {
                    UpdateBuilder update = Update.Set("plus.stock.j350", 0).Set("plus.stock.stockdate", DateTime.Now);
                    if (!eansUpdated.Contains(ean))
                    {
                        productCollection.Update(Query.EQ("doc.product.productidentifier.b244", ean), update, UpdateFlags.Multi);
                        Console.WriteLine("libriupdate : " + ean + " : to 0");
                        sb_log.AppendLine("libriupdate : " + ean + " : to 0");
                    }
                }
                

                File.WriteAllText(Properties.Settings.Default.logPath + @"\DailyUpdate\" + filename + ".txt", sb_log.ToString());

        }

        List<string> GetEanListOfStockNotZero()
        {
            List<string> eans = new List<string>();

            foreach (var libriProduct in MongoLibriDatabase.GetListOfStockNotZero())
            {
                BsonDocument product = libriProduct.doc;

                if (product.Contains("product") && product["product"].AsBsonDocument.Contains("productidentifier"))
                {
                    if (product["product"]["productidentifier"].IsBsonArray)
                    {
                        foreach (var productidentifier in product["product"]["productidentifier"].AsBsonArray)
                        {

                            BsonString b244 = productidentifier["b244"].AsString;
                            if (b244.AsString.Length == 13)
                            {
                                eans.Add(b244.AsString);
                                break;
                            }
                        }
                    }
                    else
                    {
                        BsonString b244 = product["product"]["productidentifier"]["b244"].AsString;
                        if (b244.AsString.Length == 13)
                        {
                            eans.Add(b244.AsString);
                        }

                    }
                }
            }

            return eans;
        }
    }
}
