﻿using MongoDB.Driver;
using MongoDB.Driver.Builders;
using ProcuctDB;
using ProcuctDB.Libri;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ProcuctDB;
using ProcuctDB.Libri;
using System.Globalization;
namespace LibriProductUpdate
{
    class DailyPrice : IUpdate
    {

        public void StartUpdate()
        {
            //IDatabase libriDb = new LibriDB();

            var productCollection = MongoLibriDatabase.GetCollectionOnx();

            //var products = productCollection.Find(Query.Or(Query.NotExists("doc.product.supplydetail.price_update_date"), Query.LT("doc.product.supplydetail.price_update_date", DateTime.Now.AddHours(-10)))).SetFields("doc.product.supplydetail.price").SetLimit(100000);
            //var products = productCollection.Find(Query.GT("doc.product.supplydetail.j350", 0)).SetLimit(10).Explain();

            //var bulk = productCollection.InitializeUnorderedBulkOperation();

            string localfile = Properties.Settings.Default.logPath + @"\DailyPrice\stockdata.txt";
            List<string> log = new List<string>();
            log.Add("ean discount before_discount price before_price");

            if (FtpClientStuff.Download("/IVF/V92902/BIL/STOCKDATEN.txt", localfile, false) == true)
            {
                Console.WriteLine("DailyPrice : update file downloaded");
                List<string> lines = File.ReadAllLines(localfile).ToList();
                lines.RemoveAt(0);

                //Parallel.ForEach(lines, line =>
                foreach (string line in lines)
                {
                    string[] split = line.Split(';');
                    string ean = split[0];
                    string LIST_PRICE_EXCL_TAX = split[2];
                    string LIST_PRICE_INCL_TAX = split[3];
                    string cost_price = split[4];


                    double discount = (1 - Convert.ToDouble(cost_price, CultureInfo.InvariantCulture) / Convert.ToDouble(LIST_PRICE_EXCL_TAX, CultureInfo.InvariantCulture)) * 100;
                    discount = Math.Round(discount, 1);

                    

                    //double discount_db = 99999;
                    //double price_db = 99999;

                    //IProduct product = null;
                    //try
                    //{
                    //    product = libriDb.GetMongoProductByEan(ean);
                    //}
                    //catch { }

                    //if (product != null)
                    //{
                    //    discount_db = product.getDiscount();
                    //    price_db = product.getPrice();
                    //}

                    //double max = Math.Max(discount, discount_db);
                    //double min = Math.Min(discount, discount_db);

                    //double diff = max - min;

                    //if (diff < 1)
                    //{
                    //Console.WriteLine(String.Format("{0} {1} {2}", ean, discount, discount_db));
                    //}
                    //if (productCollection.Find(Query.EQ("doc.product.producidentifier.b244", ean)).Count() > 1)
                    //{
                    productCollection.Update(
                        Query.EQ("doc.product.productidentifier.b244", ean),
                        Update.Set("plus.price.j267", discount.ToString(CultureInfo.InvariantCulture))
                              .Set("plus.price.j151", LIST_PRICE_INCL_TAX),
                        UpdateFlags.Multi);
                    //}
                    //else
                    //{
                    //productCollection.Update(Query.EQ("doc.product.producidentifier.b244", ean), Update.Set("doc.product.supplydetail.price.j267", discount.ToString()).Set("doc.product.supplydetail.price.j151", LIST_PRICE_INCL_TAX));
                    //}
                    Console.WriteLine(String.Format("DailyPrice : {0} : {1} : {2}", ean, discount.ToString(CultureInfo.InvariantCulture), LIST_PRICE_INCL_TAX));
                    //log.Add(String.Format("{0} {1} {2} {3} {4}", ean, discount, discount_db, LIST_PRICE_INCL_TAX, price_db));



                }

                //BulkWriteResult result = bulk.Execute();
                //log.Add(result.ModifiedCount.ToString());

            }


            File.WriteAllLines("log"+ DateTime.Now.Ticks + ".txt", log);

            
        }

        public void SaveLastDateTime(DateTime newDateTime)
        {
            throw new NotImplementedException();
        }

        public void DoUpdate(string filename, string localfile)
        {
            throw new NotImplementedException();
        }
    }
}
