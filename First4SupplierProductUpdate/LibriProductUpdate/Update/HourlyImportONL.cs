﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;
using System.Xml;

using MongoDB.Bson;
using MongoDB.Driver;
using MongoDB.Driver.Builders;



using Newtonsoft.Json;
using Newtonsoft.Json.Converters;
using System.Globalization;
using System.Text.RegularExpressions;
using ProcuctDB;

namespace LibriProductUpdate
{
    public class HourlyImportONL : IUpdate
    {

        public static string workingDirectory = "/TUP/ONL/";
        DateTime lastDateTime = Properties.Settings.Default.lasthourlyimporttime;
        string lasthourlyimportnumber = Properties.Settings.Default.lasthourlyimportnumber;
        MongoCollection<LibriEntity> productCollection = MongoLibriDatabase.GetCollectionOnx();
        MongoCollection<ProductSizeEntity> productsizesCollection = MongoLibriDatabase.GetProductSizesCollection();

        private string templasthourlyimportnumber = "";

        public void StartUpdate()
        {
            templasthourlyimportnumber = lasthourlyimportnumber;
            int newnumber = 0;

            if (templasthourlyimportnumber.StartsWith("0"))
            {
                newnumber = int.Parse(lasthourlyimportnumber.Remove(0,1)) + 1;
            }


            string filename = "";

            if (templasthourlyimportnumber.StartsWith("0"))
            {
                 filename = "GTUPD0" + newnumber.ToString() + ".zip";
            }
            else
            {
                 filename = "GTUPD" + newnumber.ToString() + ".zip";
            }

            bool downloaded = true;
            if (FtpClientStuff.Exist(workingDirectory, filename, true))
            {
                string localfile = Properties.Settings.Default.logPath + @"\HourlyImport\" + filename;

                downloaded = FtpClientStuff.Download(workingDirectory + filename, localfile, true);

                if (downloaded == true)
                {
                    
                    DoUpdate(filename, localfile);
                    SaveNewNumber(newnumber);
                }

                StartUpdate();

            }
            
            CheckOldFiles();

            

        }

        void SaveNewNumber(int newnumber)
        {
            if (templasthourlyimportnumber.StartsWith("0"))
            {

                Properties.Settings.Default.lasthourlyimportnumber = "0" + newnumber.ToString();
            }
            else
            {
                Properties.Settings.Default.lasthourlyimportnumber = newnumber.ToString();
            }

                Properties.Settings.Default.Save();
                Properties.Settings.Default.Reload();
                lasthourlyimportnumber = templasthourlyimportnumber;
            
            List<string> lastNumbers = File.ReadAllLines(Properties.Settings.Default.logPath + @"\HourlyImport\logfile\log.txt").ToList();
            lastNumbers.Add(newnumber.ToString());
            File.WriteAllLines(Properties.Settings.Default.logPath + @"\HourlyImport\logfile\log.txt", lastNumbers);
            Console.WriteLine(String.Format("Latest File for update date: {0}", newnumber));
            
        }

        public void SaveLastDateTime(DateTime newDateTime)
        {
            Properties.Settings.Default.lasthourlyimporttime = newDateTime;
            Properties.Settings.Default.Save();
            Properties.Settings.Default.Reload();
            File.WriteAllText(Properties.Settings.Default.logPath + @"\HourlyImport\logfile\logHourlyImport.txt", String.Format("Latest File for update date: {0}", newDateTime));
            Console.WriteLine(String.Format("Latest File for update date: {0}", newDateTime));
        }

        public void DoUpdate(string filename, string localfile)
        {
            

            Decompressor.Decompress(localfile);

            StringBuilder sb = new StringBuilder();
            sb.AppendLine(DateTime.Now.ToString());
            sb.AppendLine(localfile);
            
                Console.WriteLine(filename);
                filename = filename.Replace(".zip", ".xml");

                string xmlString = File.ReadAllText(filename).Replace("<BR>","");
                xmlString = CleanInvalidXmlChars(xmlString);

                XmlDocument doc = new XmlDocument();
                //doc.Load(filename);
                doc.LoadXml(xmlString);

                foreach (XmlNode node in doc.DocumentElement.ChildNodes)
                {
                    string jsonText = JsonConvert.SerializeXmlNode(node);
                    MongoDB.Bson.BsonDocument document = MongoDB.Bson.Serialization.BsonSerializer.Deserialize<BsonDocument>(jsonText);
                    Console.WriteLine("converting xml...");
                    if (document.ToString().Contains("product"))
                    {
                        string a001 = document["product"]["a001"].ToString();

                            if (!UpdateItem(a001, document))
                            {
                                Console.WriteLine("inserted document...");
                                sb.AppendLine("inserted---" + a001);
                            }
                            else
                            {
                                Console.WriteLine("updated---" + a001);
                                sb.AppendLine("updated---" + a001);
                            }

                        
                    }

                }

                File.WriteAllText(Properties.Settings.Default.logPath + @"\HourlyImport\" + filename + ".txt", sb.ToString());
           

            File.Delete(filename);
        }

        public string CleanInvalidXmlChars(string text)
        {
            string re = @"[^\x09\x0A\x0D\x20-\xD7FF\xE000-\xFFFD\x10000-x10FFFF]";
            return Regex.Replace(text, re, "");
        }  

        bool UpdateItem(string sku, BsonDocument document)
        {
            var testProduct = productCollection.FindOne(Query.EQ("doc.product.a001", sku));
            if (testProduct != null)
            {
                UpdateBuilder update = Update.Set("doc", document);
                productCollection.Update(Query.EQ("doc.product.a001", sku), update, UpdateFlags.Multi);
                productCollection.Update(Query.EQ("doc.product.a001", sku), Update.Set("plus.stock.stockdate", DateTime.Now).Set("plus.stock.j350",0), UpdateFlags.Multi);


                return true;
            }
            else
            {
                var entity = new LibriEntity { doc = document };
                productCollection.Insert(entity);

                AddToProductSizesCollection(GetEan(document));

                return false;
            }
            

            

            

        }

        string GetEan(BsonDocument document)
        {
            string ean = "";

            if (document["product"].AsBsonDocument.Contains("productidentifier"))
            {
                if (document["product"]["productidentifier"].IsBsonArray)
                {
                    foreach (BsonDocument p in document["product"]["productidentifier"].AsBsonArray)
                    {
                        if (p["b244"].AsString.Length == 13)
                        {
                            ean = p["b244"].AsString;
                        }
                    }
                }
                else if(document["product"]["productidentifier"].AsBsonDocument.Contains("b244"))
                {
                    ean = document["product"]["productidentifier"]["b244"].AsString;
                    
                }
            }
            return ean;
        }

        void AddToProductSizesCollection(string ean)
        {
            if (ean != string.Empty && productsizesCollection.FindOne(Query.EQ("ean", ean)) == null)
            {
                productsizesCollection.Insert(new ProductSizeEntity
                {
                    ean = ean,
                    measure = new BsonDocument()
                });
            }
        }

        void CheckOldFiles()
        {
            try
            {
                string[] filePaths = Directory.GetFiles(Properties.Settings.Default.logPath + @"\HourlyImport\");
                foreach (string filepath in filePaths)
                {
                    FileInfo file = new FileInfo(filepath);
                    if (file.CreationTime < DateTime.Now.AddMonths(-3))
                    {
                        file.Delete();
                        Console.WriteLine("Deleted.");
                    }
                }
            }
            catch { }

        }

       
    }
}
