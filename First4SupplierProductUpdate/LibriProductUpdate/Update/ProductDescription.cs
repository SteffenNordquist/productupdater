﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.IO;
using System.Net;
using System.Xml;

using HtmlAgilityPack;
using ProcuctDB;
using MongoDB.Driver;
using MongoDB.Driver.Builders;

namespace LibriProductUpdate
{
    public class ProductDescription : IUpdate
    {
        string workingDirectory = "/Anno/Tup/";
        MongoDB.Driver.MongoCollection<DescriptionFilesEntity> descriptionCollection;
        MongoDB.Driver.MongoCollection<LibriEntity> productCollection;

        string localDir = Properties.Settings.Default.logPath + @"\ProductDescription\";

        public void StartUpdate()
        {
            
           
            SetLibriProdDescFilesCollection();

            FtpClientStuff.SetCredential(true);
            string[] list = FtpClientStuff.directoryListSimple(workingDirectory);

            List<string> decompressedFiles = new List<string>();

            foreach (string l in list)
            {
                if (l.Contains("GSALES") || l.Contains("GKTEXT") || l.Contains("GIVERZ") || l.Contains("GAINFO"))
                {
                    
                    if (!FileExists(l))
                    {
                        if (FtpClientStuff.Download(workingDirectory + l, localDir + l, true))
                        {
                            decompressedFiles = Decompressor.Decompress(localDir + l);
                            foreach (string file in decompressedFiles)
                            {
                                DoUpdate(file, "");
                            }
                        }
                    }
                    
                }
            }

            DeleteDecompressedFiles(decompressedFiles);

        }

        public void SaveLastDateTime(DateTime newDateTime)
        {
            throw new NotImplementedException();
        }

        public void DoUpdate(string filename, string temp)
        {
            //try
            //{
            //    string ean = filename.Split('_')[1];
            //    string text1 = File.ReadAllText(filename).Replace("<BR>", "\r\n");
            //    text1 = text1.Remove(text1.IndexOf("</BODY>"), text1.Length - text1.IndexOf("</BODY>"));
            //    string text2 = text1.Remove(0, text1.IndexOf("<BODY>") + 6);
            //    XmlDocument xml = new XmlDocument();
            //    string p_node = "";
            //    try
            //    {
            //        xml.LoadXml(text2);
            //        try { p_node = xml.SelectSingleNode("p").InnerText; }
            //        catch { p_node = xml.SelectSingleNode("P").InnerText; }
            //    }
            //    catch { p_node = text2; }
            //    p_node = p_node.Replace("<p>", "").Replace("</p>", "").Replace("<P>", "").Replace("</P>", "");

            //    MongoDB.Driver.Builders.UpdateBuilder update = MongoDB.Driver.Builders.Update.Set("doc.product.description", p_node);
            //    productCollection.Update(MongoDB.Driver.Builders.Query.EQ("doc.product.productidentifier.b244", ean), update);
            //}
            //catch { }

            HtmlDocument htmlFile = new HtmlDocument();
            try
            {
                string ean = filename.Split('_')[1];

                
                //htmlFile.Load(filename);
                //HtmlNode bodyNode = htmlFile.DocumentNode.SelectSingleNode("/html/body");
                string file_content = File.ReadAllText(filename);

                string HTML_TAG_PATTERN = "<.*?>";
                //string description = WebUtility.HtmlDecode(bodyNode.InnerText);
                string description = WebUtility.HtmlDecode(file_content);
                description = Regex.Replace(description, HTML_TAG_PATTERN, "").Replace("\r\n", " ").Replace("\r", " ").Replace("\n", " ").Replace("\t", " ");

                MongoDB.Driver.Builders.UpdateBuilder update = MongoDB.Driver.Builders.Update.Set("doc.product.description", description);
                productCollection.Update(MongoDB.Driver.Builders.Query.EQ("doc.product.productidentifier.b244", ean), update);
            }
            catch (Exception ex) { Console.WriteLine(ex.Message + "\r\n" + filename + "\r\n"); Console.ReadLine(); };
        }

        public void SetLibriProdDescFilesCollection()
        {
            descriptionCollection = MongoLibriDatabase.GetDB().GetCollection<DescriptionFilesEntity>("descriptionFiles");
            productCollection = MongoLibriDatabase.GetCollectionOnx();
        }

        public bool FileExists(string file)
        {
            if (descriptionCollection.Find(MongoDB.Driver.Builders.Query.EQ("file", file)).Count() > 0)
            {
                return true;
            }
            else {
                descriptionCollection.Insert(new DescriptionFilesEntity { file = file });
                return false; }
        }

        public void DeleteDecompressedFiles(List<string> files)
        {
            foreach (string file in files)
            {
                File.Delete(file);
            }
        }


    }
}
