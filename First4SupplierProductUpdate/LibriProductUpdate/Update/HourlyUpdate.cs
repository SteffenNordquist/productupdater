﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;
using System.Xml;

using MongoDB.Bson;
using MongoDB.Driver;
using MongoDB.Driver.Builders;
using System.Globalization;
using ProcuctDB;



namespace LibriProductUpdate
{
    public class HourlyUpdate : IUpdate
    {
        string workingDirectory = "/STK/BIL/";
        DateTime lastDateTime = Convert.ToDateTime(Properties.Settings.Default.lasthourlyupdatetime,CultureInfo.InvariantCulture);
        MongoCollection<LibriEntity> productCollection;

        public void StartUpdate()
        {
            productCollection = MongoLibriDatabase.GetCollectionOnx();

            List<FileSort> fileList = FtpClientStuff.ReusableFileList;
            //List<FileSort> fileList = FtpClientStuff.GetFileList(workingDirectory, false);

            foreach (FileSort file in fileList)
            {
                if (file.name.Contains("LTSTK") && file.datetime > lastDateTime && file.datetime > DateTime.Now.AddDays(-7))
                {
                    //Console.WriteLine(file.name + " " + file.datetime);
                    
                    DoUpdate(file.name, "");
                    SaveLastDateTime(file.datetime);
                }
            }
        }

        public void SaveLastDateTime(DateTime newDateTime)
        {
            Properties.Settings.Default.lasthourlyupdatetime = newDateTime;
            Properties.Settings.Default.Save();
            Properties.Settings.Default.Reload();
            File.WriteAllText("log_hourly.txt", String.Format("Latest File for update date: {0}", newDateTime));
            Console.WriteLine(String.Format("Latest File for update date: {0}", newDateTime));
        }

        public void DoUpdate(string filename, string temp)
        {
            string localfile = Properties.Settings.Default.logPath + @"\HourlyUpdate\LTSTK.txt";
            //string localfile = "test.txt";
            FtpClientStuff.Download(workingDirectory + filename, localfile,false);
            Console.WriteLine(localfile);

            StringBuilder sb_log = new StringBuilder();
            sb_log.AppendLine(DateTime.Now.ToString());
            sb_log.AppendLine(localfile);
            
            Console.WriteLine("Comparing update file to db..");

            List<string> lines = File.ReadAllLines(localfile).ToList();
            lines.RemoveAt(0);

            foreach(string l in lines)
            {
                
                    string[] line = l.Split(';' );
                    string ean = line[0];
                    int stock = Convert.ToInt32(line[5]);

                    if (UpdateItem(ean, stock))
                    {
                        Console.WriteLine(ean + " updated to " + stock);
                        sb_log.AppendLine(ean + " updated to " + stock);
                    }
                    else
                    {
                        sb_log.AppendLine(ean + " error updating");
                    }

            }

            File.WriteAllText(Properties.Settings.Default.logPath + @"\HourlyUpdate\" + filename + ".txt", sb_log.ToString());
        }

        bool UpdateItem(string ean, int stock)
        {

            UpdateBuilder StockUpdate = Update.Set("plus.stock.j350", stock);
            WriteConcernResult writeConcern = productCollection.Update(Query.EQ("doc.product.productidentifier.b244", ean), StockUpdate, UpdateFlags.Multi);

            UpdateBuilder DateTimeUpdate = Update.Set("plus.stock.stockdate", DateTime.Now);
            productCollection.Update(Query.EQ("doc.product.productidentifier.b244", ean), DateTimeUpdate, UpdateFlags.Multi);

            if (!writeConcern.UpdatedExisting)
            {
                return false;
            }
            else
            {
                return true;
            }


        }
    }
}
