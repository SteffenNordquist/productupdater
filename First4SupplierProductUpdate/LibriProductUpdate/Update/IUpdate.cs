﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using MongoDB.Driver;
namespace LibriProductUpdate
{
    public interface IUpdate
    {
        void StartUpdate();
        void SaveLastDateTime(DateTime newDateTime);
        void DoUpdate(string filename, string localfile);
    }
}
