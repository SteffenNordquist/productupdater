﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using MongoDB.Bson;
using MongoDB.Driver;
using MongoDB.Driver.Builders;
using MongoDB.Driver.GridFS;
using MongoDB.Driver.Linq;
using ProcuctDB;

namespace LibriProductUpdate
{
    public class MongoLibriDatabase
    {
        static MongoCollection<LibriEntity> collection_onx = null;
        static MongoCollection<BsonDocument> collection = null;
        static MongoCollection<ProductSizeEntity> productSizesCollection = null;
        static MongoDatabase db = null;
        static MongoDatabase productSizeDB = null;

        public static MongoDatabase GetDB()
        {
            if(db == null)
            {
                db = new MongoClient(@"mongodb://client148:client148devnetworks@136.243.44.111/PD_Libri")
                .GetServer()
                .GetDatabase("PD_Libri");
            }

            return db;
        }

        public static MongoDatabase GetProductSizesDB()
        {
            if (productSizeDB == null)
            {
                productSizeDB = new MongoClient(@"mongodb://client148:client148devnetworks@136.243.44.111/ProductSize")
                .GetServer()
                .GetDatabase("ProductSize");
            }

            return productSizeDB;
        }

        public static MongoCollection<LibriEntity> GetCollectionOnx()
        {
            if (collection_onx == null)
            {
                collection_onx = GetDB().GetCollection<LibriEntity>("libri");
            }

            return collection_onx;
        }

        public static MongoCollection<BsonDocument> GetCollection()
        {
            if (collection == null)
            {
                collection = GetDB().GetCollection<BsonDocument>("libri");
            }

            return collection;
        }

        public static MongoCollection<ProductSizeEntity> GetProductSizesCollection()
        {
            if (productSizesCollection == null)
            {
                productSizesCollection = GetProductSizesDB().GetCollection<ProductSizeEntity>("productsizes");
            }

            return productSizesCollection;
        }

        public static List<LibriEntity> GetListOfStockNotZero()
        {
            List<string> skus = new List<string>();
            return GetCollectionOnx().Find(Query.GT("plus.stock.j350", 0)).SetFields("doc.product.productidentifier").ToList();

        }

        public static void GetStock(string ean)
        {
            LibriEntity collection = GetCollectionOnx().FindOne(Query.EQ("doc.product.productidentifier.b244", ean));
            BsonDocument bson = collection.doc;
            Console.WriteLine(ean+" => " + bson["product"]["supplydetail"]["stock"]["j350"].AsInt32);
            
        }
    }
}
