﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MongoDB.Bson;
namespace LibriProductUpdate
{
    public class DescriptionFilesEntity
    {
        public ObjectId id { get; set; }
        public string file { get; set; }
    }
}
