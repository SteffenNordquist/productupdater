﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Net;
using System.IO;
using System.Threading;
namespace LibriProductUpdate
{
    public class FtpClientStuff
    {
        public static FtpWebRequest ftpRequest = null;
        public static FtpWebResponse ftpResponse = null;
        public static Stream ftpStream = null;

        public static string ftphost = Properties.Settings.Default.ftphost;
        public static string ftp3host = Properties.Settings.Default.ftp3host;
        public static string ftpusername = Properties.Settings.Default.ftpusername;
        public static string ftppassword = Properties.Settings.Default.ftppassword;
        public static string ftp3password = Properties.Settings.Default.ftp3password;

        public static string FTPHOST = "";
        public static string FTPPASS = "";

        public static string[] directoryListSimple(string workingDirectory)
        {
            try
            {
                /* Create an FTP Request */
                ftpRequest = (FtpWebRequest)FtpWebRequest.Create(FTPHOST + workingDirectory);
                /* Log in to the FTP Server with the User Name and Password Provided */
                ftpRequest.Credentials = new NetworkCredential(ftpusername, FTPPASS);
                /* When in doubt, use these options */
                ftpRequest.UseBinary = true;
                ftpRequest.UsePassive = true;
                ftpRequest.KeepAlive = true;
                //ftpRequest.Timeout = Timeout.Infinite;
                /* Specify the Type of FTP Request */
                ftpRequest.Method = WebRequestMethods.Ftp.ListDirectory;

                //Thread.Sleep(1000);

                /* Establish Return Communication with the FTP Server */
                ftpResponse = (FtpWebResponse)ftpRequest.GetResponse();

                Thread.Sleep(1000);

                /* Establish Return Communication with the FTP Server */
                ftpStream = ftpResponse.GetResponseStream();
                /* Get the FTP Server's Response Stream */
                StreamReader ftpReader = new StreamReader(ftpStream);
                /* Store the Raw Response */
                string directoryRaw = null;
                /* Read Each Line of the Response and Append a Pipe to Each Line for Easy Parsing */
                try { while (ftpReader.Peek() != -1) { directoryRaw += ftpReader.ReadLine() + "|"; } }
                catch (Exception ex) { Console.WriteLine(ex.ToString()); }
                
                /* Return the Directory Listing as a string Array by Parsing 'directoryRaw' with the Delimiter you Append (I use | in This Example) */
                try { string[] directoryList = directoryRaw.Split("|".ToCharArray()); return directoryList; 
                }
                catch (Exception ex) { Console.WriteLine(ex.ToString()); }

                /* Resource Cleanup */
                ftpReader.Close();
                ftpStream.Close();
                ftpResponse.Close();
                ftpRequest = null;

            }
            catch (Exception ex) { Console.WriteLine(ex.ToString()); }
            /* Return an Empty string Array if an Exception Occurs */
            return new string[] { "" };

        }

        public static List<FileSort> ReusableFileList = new List<FileSort>();

        public static List<FileSort> GetFileList(string workingDirectory, bool ftp3)
        {
            SetCredential(ftp3);

            //Console.WriteLine(GetDateTimestampOnServer("/STK/BIL/", "LTSTK5863.TXT")); Console.ReadLine();
            //Console.WriteLine(GetDateTimestampOnServer("/Anno/Tup/", "GAINFO150.ZIP")); Console.ReadLine();
            

            List<FileSort> fileList = new List<FileSort>();
            
            string[] list = directoryListSimple(workingDirectory);
            
            foreach (var i in list)
            {
                if (i.Length > 0)
                {
                    //Console.WriteLine(i);

                    DateTime d = GetDateTimestampOnServer(workingDirectory, i);
                    FileSort f = new FileSort();
                    f.name = i;
                    f.datetime = d;
                    fileList.Add(f);
                }
            }
            fileList = fileList.OrderBy(x => x.datetime).ToList();

            ReusableFileList = fileList;

            return fileList;
        }

        public static DateTime GetDateTimestampOnServer(string workingDirectory, string fileName)
        {
            

            Uri serverUri = new Uri(FTPHOST  + workingDirectory + fileName);
            //Console.WriteLine(serverUri);
            // The serverUri should start with the ftp:// scheme.
            if (serverUri.Scheme != Uri.UriSchemeFtp)
            {
                return DateTime.MinValue;
            }

           
            ftpRequest = (FtpWebRequest)WebRequest.Create(serverUri);
            ftpRequest.Credentials = new NetworkCredential(ftpusername, FTPPASS);

            /* When in doubt, use these options */
            ftpRequest.UseBinary = true;
            ftpRequest.UsePassive = true;
            ftpRequest.KeepAlive = true;
            ftpRequest.Timeout = Timeout.Infinite;

            ftpRequest.Method = WebRequestMethods.Ftp.GetDateTimestamp;

            ftpResponse = (FtpWebResponse)ftpRequest.GetResponse();
            

            //FtpWebResponse response = ftpResponse;

            //ftpRequest = null;
            //ftpResponse = null;

            DateTime modifiedTime = ftpResponse.LastModified;

            ftpResponse.Close();
            ftpRequest = null;

            return modifiedTime;
        }

        public static bool Download(string remoteFile, string localFile, bool ftp3)
        {
            SetCredential(ftp3);

            FtpWebRequest ftpRequest = null;
            FtpWebResponse ftpResponse = null;
            Stream ftpStream;
            int bufferSize = 2048;

            bool download_success = false;
            try
            {
                /* Create an FTP Request */
                ftpRequest = (FtpWebRequest)FtpWebRequest.Create(FTPHOST + remoteFile);
                /* Log in to the FTP Server with the User Name and Password Provided */
                ftpRequest.Credentials = new NetworkCredential(ftpusername, FTPPASS);
                /* When in doubt, use these options */
                ftpRequest.UseBinary = true;
                ftpRequest.UsePassive = true;
                ftpRequest.KeepAlive = true;
                /* Specify the Type of FTP Request */
                ftpRequest.Method = WebRequestMethods.Ftp.DownloadFile;
                /* Establish Return Communication with the FTP Server */
                ftpResponse = (FtpWebResponse)ftpRequest.GetResponse();
                /* Get the FTP Server's Response Stream */
                ftpStream = ftpResponse.GetResponseStream();
                /* Open a File Stream to Write the Downloaded File */
                FileStream localFileStream = new FileStream(localFile, FileMode.Create);
                /* Buffer for the Downloaded Data */
                byte[] byteBuffer = new byte[bufferSize];
                int bytesRead = ftpStream.Read(byteBuffer, 0, bufferSize);
                /* Download the File by Writing the Buffered Data Until the Transfer is Complete */
                //try
                //{
                    while (bytesRead > 0)
                    {
                        localFileStream.Write(byteBuffer, 0, bytesRead);
                        bytesRead = ftpStream.Read(byteBuffer, 0, bufferSize);
                    }
                //}
                //catch (Exception ex) { Console.WriteLine(ex.ToString()); }
                /* Resource Cleanup */
                localFileStream.Close();
                ftpStream.Close();
                ftpResponse.Close();
                ftpRequest = null;

                download_success = true;
            }
            catch (Exception ex) { Console.WriteLine(ex.Message); download_success = false; }
            return download_success;
        }

        public static void SetCredential(bool ftp3)
        {
            FTPHOST = ftphost;
            FTPPASS = ftppassword;

            if (ftp3)
            {
                FTPHOST = ftp3host;
                FTPPASS = ftp3password;
            }
        }

        public static bool Exist(string workingDirectory,string file, bool ftp3)
        {
            SetCredential(ftp3);

            bool exist = false;

            string uri = FTPHOST + workingDirectory + file;
            var request = (FtpWebRequest)WebRequest.Create(uri);
            request.Credentials = new NetworkCredential(ftpusername, FTPPASS);
            request.Method = WebRequestMethods.Ftp.GetFileSize;

            try
            {
                FtpWebResponse response = (FtpWebResponse)request.GetResponse();
                exist = true;
            }
            catch (WebException ex)
            {
                FtpWebResponse response = (FtpWebResponse)ex.Response;
                if (response.StatusCode ==
                    FtpStatusCode.ActionNotTakenFileUnavailable)
                {
                    exist = false;
                }
            }
            return exist;
        }
    }

    
}
