
### Knv, libri, bvw/bremer and hoffmann merged product update tools ###

* KnvProductUpdate. This tool will pull update files from knv ftp. Downloaded files will then be used to update our database with updated knv stocks and prices. It is currently running in 148.251.0.235 server.

* LibriProductUpdate. This tool will pull update files from libri ftp. Downloaded files will then be used to update our database with updated libri stocks and prices. It is currently running in 148.251.0.235 server.

* BremerProductUpdate. This tool will pull update files from bvw ftp. Downloaded files will then be used to update our database with updated bvw stocks and prices. It is currently running in 148.251.0.235 server.

* HoffmannProductUpdate. This tool will pull update files from hoffmann ftp. Downloaded files will then be used to update our database with updated hoffmann stocks and prices. It is currently running in 148.251.0.235 server.

### How do I get set up? ###

* For a new deployment. We need to update the configuration within the tool debug folder. The things needed to update in the configuration are the directories, mongodb IP address (if needed) and datetime of the files where to start downloading (used for update) again.

### Who do I talk to? ###

* Wylan Osorio