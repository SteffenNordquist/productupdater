﻿using DN_Classes.Logger;
using MongoDB.Bson;
using MongoDB.Driver;
using MongoDB.Driver.Builders;
using ProcuctDB.Herweck;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;

namespace SupplierUpdateSystem.Suppliers.Herweck
{
    class HerweckStockUpdate:IUpdate
    {
        private string file;
        private string supplierPath;
        private List<string> lines;

        private List<string> existsInUpdateFile = new List<string>();

        public HerweckStockUpdate()
        {
            file = "7006126_Artikel.csv";
            supplierPath = @"C:\DevNetworks\Uncategorized\Updater\SupplierUpdateSystem\Herweck\";
        }

        public void DoUpdate()
        {
            CreateDirectory();
            Download();
            ReadFile();
            UpdateHerweck();
            UpdateToZero_NotInUpdateFile();
        }

        public void UpdateHerweck()
        {
            HerweckDB herweckDB = new HerweckDB();
            
            Parallel.ForEach(lines, new ParallelOptions{ MaxDegreeOfParallelism = 7}, x=>
            {
                string[] splittedLine = x.Split('|').ToArray();

                try
                {
                    HerweckEntity item = HerweckDB.mongoCollection.FindOne(Query.EQ("product.ean", splittedLine[1]));

                    if (item.product.ean != string.Empty)
                    {

                        if (item != null)
                        {
                            UpdateItem(splittedLine);
                            //Console.WriteLine("HerweckUpdate\t" + splittedLine[1]);
                        }

                        //Add as new item
                        else
                        {
                            item = AddAsNew(splittedLine);
                        }
                    }

                    //no ean items
                    else
                    {
                        AddAsNew(splittedLine);
                    }
                }

                catch
                { }
            });

            Console.WriteLine("Herweck Update done!");
        }

        private static HerweckEntity AddAsNew(string[] splittedLine)
        {
            HerweckEntity item;
            item = new HerweckEntity();
            item.product = new ProcuctDB.Herweck.HerweckEntity.Product
            {
                ean = splittedLine[1],
                artikelNummer = splittedLine[0],
                bezeichnung1 = splittedLine[2],
                bezeichnung2 = splittedLine[3],
                webText = splittedLine[4],
                hersteller = splittedLine[5],
                herstellerArtikelnummer = splittedLine[6],
                hek = Convert.ToDouble(splittedLine[7].ToString().Replace(",", "."), CultureInfo.InvariantCulture),
                uvp = Convert.ToDouble(splittedLine[8].ToString().Replace(",", "."), CultureInfo.InvariantCulture),
                k1 = splittedLine[9],
                k2 = splittedLine[10],
                k3 = splittedLine[11],
                abLager = splittedLine[12],
                auslaufartikel = splittedLine[13],
                bild = splittedLine[14],
                mwSt = Convert.ToInt32(splittedLine[15])
            };

            HerweckDB.mongoCollection.Insert(item);

            //AppLogger.Log("Updating: " + " ean: " + splittedLine[1] + " abLager: " + splittedLine[12]);
            return item;
        }

        private void UpdateItem(string[] splittedLine)
        {
            int stock = 0;
            if (splittedLine[12] == "Ja")
            {
                stock = 1;
            }

            IMongoUpdate update = Update.Set("product.artikelNummer", splittedLine[0])
                                        .Set("product.ean", splittedLine[1])
                                        .Set("product.bezeichnung1", splittedLine[2])
                                        .Set("product.bezeichnung2", splittedLine[3])
                                        .Set("product.webText", splittedLine[4])
                                        .Set("product.hersteller", splittedLine[5])
                                        .Set("product.herstellerArtikelnummer", splittedLine[6])
                                        .Set("product.hek", Convert.ToDouble(splittedLine[7].ToString().Replace(",", "."), CultureInfo.InvariantCulture))
                                        .Set("product.uvp", Convert.ToDouble(splittedLine[8].ToString().Replace(",", "."), CultureInfo.InvariantCulture))
                                        .Set("product.k1", splittedLine[9])
                                        .Set("product.k2", splittedLine[10])
                                        .Set("product.k3", splittedLine[11])
                                        .Set("product.abLager", splittedLine[12])
                                        .Set("product.auslaufartikel", splittedLine[13])
                                        .Set("product.bild", splittedLine[14])
                                        .Set("product.mwSt", Convert.ToInt32(splittedLine[15]))
                                        .Set("product.lagermenge", stock);

            existsInUpdateFile.Add(splittedLine[1]);
            
            IMongoQuery query = Query.EQ("product.ean", splittedLine[1]);
            UpdateSets(stock, query);

            HerweckDB.mongoCollection.Update(query, update);

            //AppLogger.Log("Updating: " + " ean: " + splittedLine[1] +" largermenge: " +stock);
            Console.WriteLine("Herweck" + "\tean: " +splittedLine[1]);
        }

        private static void UpdateSets(int stock, IMongoQuery query)
        {
            if (stock == 0)
            {
                SetUpdater<HerweckEntity> setUpdater = new SetUpdater<HerweckEntity>(HerweckDB.mongoCollection.FindOne(query), true);
                setUpdater.UpdateForAsinOutsideOrInsidePlus();
            }

            else if (stock > 0)
            {
                SetUpdater<HerweckEntity> setUpdater = new SetUpdater<HerweckEntity>(HerweckDB.mongoCollection.FindOne(query), true);
                setUpdater.UpdateForAsinOutsideOrInsidePlus();
            }
        }


        public void ReadFile()
        {
            lines = File.ReadAllLines(supplierPath + file, Encoding.Default).Skip(1).ToList();
        }

        public void CreateDirectory()
        {
            Directory.CreateDirectory(supplierPath);
        }

        public void Download()
        {
            FtpWebRequest request = (FtpWebRequest)WebRequest.Create("ftp://ftp.herweck.de/" + file);
            request.Method = WebRequestMethods.Ftp.DownloadFile;
            request.Credentials = new NetworkCredential("7006126", "bumeneri");
            request.KeepAlive = true;
            FtpWebResponse response = (FtpWebResponse)request.GetResponse();
            Stream responseStream = response.GetResponseStream();
            StreamReader reader = new StreamReader(responseStream);
            FileStream localFileStream = new FileStream(@"C:\DevNetworks\Uncategorized\Updater\SupplierUpdateSystem\Herweck\" + file.Replace("\r", ""), FileMode.Create);
            byte[] byteBuffer = new byte[2048];
            int bytesRead = responseStream.Read(byteBuffer, 0, 2048);
            try
            {
                while (bytesRead > 0)
                {
                    localFileStream.Write(byteBuffer, 0, bytesRead);
                    bytesRead = responseStream.Read(byteBuffer, 0, 2048);
                }
            }
            catch (Exception ex) { Console.WriteLine(ex.ToString()); }
            Console.WriteLine("Download Complete, status {0}", response.StatusDescription);
            reader.Close();
            response.Close();

            localFileStream.Close();

            request = null;

            //AppLogger.Log("Download file: success");
        }

        private void UpdateToZero_NotInUpdateFile()
        {
            var toBeUpdatedList = HerweckDB.mongoCollection.Find(Query.GT("product.lagermenge", 0));

            foreach (var toBeUpdated in toBeUpdatedList)
            {
                string ean = toBeUpdated.GetEan();

                if (!existsInUpdateFile.Contains(ean))
                {
                    HerweckDB.mongoCollection.Update(Query.EQ("product.ean", ean), Update.Set("product.lagermenge", 0));
                    //AppLogger.Log("Set to zero no in the file:" +ean);
                }
            }
        }
    }
}
