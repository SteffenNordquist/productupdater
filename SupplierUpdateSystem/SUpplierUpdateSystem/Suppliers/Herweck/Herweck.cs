﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SupplierUpdateSystem.Suppliers.Herweck
{
    class Herweck: ISupplier
    {
        public void Start()
        {
            IUpdate update = null;
            update = new HerweckStockUpdate();

            update.DoUpdate();
        }
    }
}
