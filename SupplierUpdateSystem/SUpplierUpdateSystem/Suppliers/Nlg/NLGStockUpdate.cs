﻿using DN_Classes.Logger;
using MongoDB.Bson;
using MongoDB.Driver;
using MongoDB.Driver.Builders;
using ProcuctDB.NLG;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;

namespace SupplierUpdateSystem.Suppliers
{
    public class NLGStockUpdate : IUpdate
    {
        private string status="";
        private MongoCollection<NLGEntity> Collection = null;

        public void DoUpdate()
        {
         
            Console.WriteLine("Downloading CSV file");

            DownloadFile();
            MongoDatabase db = Databases.GetDB("PD_NLG");
            Collection = db.GetCollection<NLGEntity>("nlg");

            List<string> sources = System.IO.File.ReadAllLines(@"C:\DevNetworks\Uncategorized\Updater\SupplierUpdateSystem\NLG_Dropshipping_full.csv", Encoding.GetEncoding("windows-1252")).ToList();
            sources.RemoveAt(0);
            int items = 0;
            foreach (string line in sources)
            {
                string articleNumber = line.Split(';')[0];

                if (Collection.Find(Query.EQ("products.articlenumber", articleNumber)).Count() == 0)
                {
                    try
                    {
                        InsertNewProduct(line);
                    }
                    catch { }
                }
                else
                {
                    UpdateProduct(line);
                }

                items++;
      
            }
       }

        private void DownloadFile()
        {
      
            FtpWebRequest request = (FtpWebRequest)WebRequest.Create("ftp://NLGShop.de/NLG_Dropshipping_full.csv");
            request.Method = WebRequestMethods.Ftp.DownloadFile;
            request.Credentials = new NetworkCredential("nlgshw_23", "MFGiYNUE8ghZtdqv");
            request.KeepAlive = true;
            FtpWebResponse response = (FtpWebResponse)request.GetResponse();
            Stream responseStream = response.GetResponseStream();
            StreamReader reader = new StreamReader(responseStream);
            FileStream localFileStream = new FileStream(@"C:\DevNetworks\Uncategorized\Updater\SupplierUpdateSystem\NLG_Dropshipping_full.csv", FileMode.Create);
            byte[] byteBuffer = new byte[2048];
            int bytesRead = responseStream.Read(byteBuffer, 0, 2048);
            try
            {
                while (bytesRead > 0)
                {
                    localFileStream.Write(byteBuffer, 0, bytesRead);
                    bytesRead = responseStream.Read(byteBuffer, 0, 2048);
                }
            }
            catch (Exception ex) {
                status = "Caught Exception at DownloadFile Method (NLGStockUpdate.cs Line 63) Exception message: "+ ex.ToString();

                Console.WriteLine(ex.ToString()); }
            Console.WriteLine("Download Complete, status {0}", response.StatusDescription);
            reader.Close();
            response.Close();

            localFileStream.Close();

            request = null;

            //AppLogger.Log("Download file: success");
        }

        private void InsertNewProduct(string line)
        {
            string[] split = line.Split(';');

                NLGEntity nlg = new NLGEntity();
                NLGEntity.Products nlgproducts = new NLGEntity.Products();

                #region split assignments
                nlgproducts.articlenumber = split[0];
                nlgproducts.ean = "";
                if (split[1].Length == 13) { nlgproducts.ean = split[1]; }
                nlgproducts.title = split[2];
                nlgproducts.KURZBESCH = split[3];
                nlgproducts.description = split[4];

                double vkgross = 0;
                try { vkgross = Convert.ToDouble(split[5].Replace("\"", "").Replace(",", ".")); }
                catch { }
                nlgproducts.vkgross = vkgross;

                double vat = 0;
                try { vat = Convert.ToDouble(split[6].Replace("\"", "").Replace(",", ".")); }
                catch { }
                nlgproducts.vat = vat;
                nlgproducts.UVP = split[7];
                nlgproducts.unit = split[8];
                nlgproducts.weight = split[9];

                double vkhgross = 0;
                try { vkhgross = Convert.ToDouble(split[10].Replace("\"", "").Replace(",", ".")); }
                catch { }
                nlgproducts.vkhgross = vkhgross;
                nlgproducts.eknet = split[11];
                nlgproducts.LIEFNR = split[12];
                nlgproducts.mindistance = split[13];

                int stock = 0;
                try { stock = Convert.ToInt32(split[14].Replace("\"", "").Replace(",", ".")); }
                catch { }
                nlgproducts.stockbest = stock;
                nlgproducts.deliverystatus = split[15];
                nlgproducts.pricelist = 0;
                nlgproducts.TOPART = split[17];
                nlgproducts.newvalue = split[18];
                nlgproducts.active = split[19];

                double baseprice = 0;
                try { baseprice = Convert.ToDouble(split[20].Replace("\"", "").Replace(",", ".")); }
                catch { }
                nlgproducts.baseprice = baseprice;

                double basepriceunit = 0;
                try { basepriceunit = Convert.ToDouble(split[21].Replace("\"", "").Replace(",", ".")); }
                catch { }
                nlgproducts.basepriceunit = basepriceunit;

                double basepricevalue = 0;
                try { basepricevalue = Convert.ToDouble(split[22].Replace("\"", "").Replace(",", ".")); }
                catch { }
                nlgproducts.basepricevalue = basepricevalue;
                nlgproducts.contents = split[23];
                nlgproducts.HAN = split[24];
                nlgproducts.fig1 = split[25];
                nlgproducts.fig2 = split[26];
                nlgproducts.fig3 = split[27];
                nlgproducts.fig4 = split[28];
                nlgproducts.activestock = split[29];
                nlgproducts.webshopactive = split[30];
                nlgproducts.littlenull = split[31];
                nlgproducts.divible = split[32];
                nlgproducts.manufacturer = split[33];

                int numberp1 = 0, numberp2 = 0, numberp3 = 0, numberp4 = 0, numberp5 = 0, numberh1 = 0, numberh2 = 0, numberh3 = 0, numberh4 = 0, numberh5 = 0;
                double pricep1 = 0, pricep2 = 0, pricep3 = 0, pricep4 = 0, pricep5 = 0, priceh1 = 0, priceh2 = 0, priceh3 = 0, priceh4 = 0, priceh5 = 0;

                try { numberp1 = Convert.ToInt32(split[34]); }
                catch { }
                try { pricep1 = Convert.ToDouble(split[35]); }
                catch { }
                try { numberp2 = Convert.ToInt32(split[36]); }
                catch { }
                try { pricep2 = Convert.ToDouble(split[37]); }
                catch { }
                try { numberp3 = Convert.ToInt32(split[38]); }
                catch { }
                try { pricep3 = Convert.ToDouble(split[39]); }
                catch { }
                try { numberp4 = Convert.ToInt32(split[40]); }
                catch { }
                try { pricep4 = Convert.ToDouble(split[41]); }
                catch { }
                try { numberp5 = Convert.ToInt32(split[42]); }
                catch { }
                try { pricep5 = Convert.ToDouble(split[43]); }
                catch { }
                try { numberh1 = Convert.ToInt32(split[44]); }
                catch { }
                try { priceh1 = Convert.ToDouble(split[45]); }
                catch { }
                try { numberh2 = Convert.ToInt32(split[46]); }
                catch { }
                try { priceh2 = Convert.ToDouble(split[47]); }
                catch { }
                try { numberh3 = Convert.ToInt32(split[48]); }
                catch { }
                try { priceh3 = Convert.ToDouble(split[49]); }
                catch { }
                try { numberh4 = Convert.ToInt32(split[50]); }
                catch { }
                try { priceh4 = Convert.ToDouble(split[51]); }
                catch { }
                try { numberh5 = Convert.ToInt32(split[52]); }
                catch { }
                try { priceh5 = Convert.ToDouble(split[53]); }
                catch { }

                nlgproducts.numberp1 = numberp1;
                nlgproducts.numberp2 = numberp2;
                nlgproducts.numberp3 = numberp3;
                nlgproducts.numberp4 = numberp4;
                nlgproducts.numberp5 = numberp5;

                nlgproducts.pricep1 = pricep1;
                nlgproducts.pricep2 = pricep2;
                nlgproducts.pricep3 = pricep3;
                nlgproducts.pricep4 = pricep4;
                nlgproducts.pricep5 = pricep5;

                nlgproducts.numberh1 = numberh1;
                nlgproducts.numberh2 = numberh2;
                nlgproducts.numberh3 = numberh3;
                nlgproducts.numberh4 = numberh4;
                nlgproducts.numberh5 = numberh5;

                nlgproducts.priceh1 = priceh1;
                nlgproducts.priceh2 = priceh2;
                nlgproducts.priceh3 = priceh3;
                nlgproducts.priceh4 = priceh4;
                nlgproducts.priceh5 = priceh5;

                nlgproducts.category = split[54];
                nlgproducts.category2 = split[55];
                nlgproducts.category3 = split[56];
                nlgproducts.category4 = split[57];
                nlgproducts.category5 = split[58];

                nlg.products = nlgproducts;

                #endregion

                Collection.Insert(nlg);
                Console.WriteLine("Nlg new product : " + nlg.products.articlenumber);

                if (nlg.products.ean.Length == 13)
                {
                    ProductSizeDB.ProductSizeCollection().Insert(new ProductSize { ean = nlg.products.ean, measure = new BsonDocument() });
                    
                    Console.WriteLine("Inserted to productsizes...");
                }
            

        }

        private void UpdateProduct(string line)
        {
            string[] split = line.Split(';');

            string articleNumber = split[0];

            int stock = 0;
            string eknetto = split[11];

            try { stock = Convert.ToInt32(split[14].Replace("\"", "")); }
            catch { }

            string title = split[2];

            string fig1 = "", fig2 = "", fig3 = "", fig4 = "";

            try
            {
                fig1 = split[25];
                fig2 = split[26];
                fig3 = split[27];
                fig4 = split[28];
            }
            catch { }

            if (fig1 == "")
            {
                Collection.Update(Query.EQ("products.articlenumber", articleNumber), Update.Set("products.stockbest", stock).Set("products.eknet", eknetto).Set("products.title", title));
            }
            else
            {
                Collection.Update(Query.EQ("products.articlenumber", articleNumber), Update.Set("products.stockbest", stock).Set("products.eknet", eknetto).Set("products.title", title)
                    .Set("products.fig1", fig1)
                    .Set("products.fig2", fig2)
                    .Set("products.fig3", fig3)
                    .Set("products.fig4", fig4));
            }
        }
    }
}
