﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SupplierUpdateSystem.Suppliers.Edding
{
    class Edding: ISupplier
    {
        public void Start()
        {
            IUpdate update = null;
            update = new EddingStockUpdate();
            update.DoUpdate();
        }
    }
}
