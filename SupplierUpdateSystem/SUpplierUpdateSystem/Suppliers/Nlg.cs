﻿using ApplicationStatus;
using MongoDB.Driver;
using MongoDB.Driver.Builders;
using ProcuctDB.Berk;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;

namespace SupplierUpdateSystem.Suppliers
{
    public class Nlg : ISupplier
    {
        public void Start()
        {
           
            IUpdate update = null;

            update = new NLGStockUpdate();
            update.DoUpdate();
  
        }

        public class NLGStockUpdate : IUpdate
        {
 
            public void DoUpdate()
            {
                Console.WriteLine("Downloading CSV file");

                DownloadFile();

                MongoDatabase db = Database.GetDB("PD_NLG");
                MongoCollection<BerkEntity> col = db.GetCollection<BerkEntity>("nlg");
                List<string> sources = System.IO.File.ReadAllLines("NLG_Dropshipping_full.csv").ToList();
                sources.RemoveAt(0);
                foreach (string source in sources)
                {
                    string articleNumber = source.Split(';')[0];
                    int stock = 0;
                    try { stock = Convert.ToInt32(source.Split(';')[14].Replace("\"", "")); }
                    catch { }
                    col.Update(Query.EQ("products.articlenumber", articleNumber), Update.Set("products.stockbest", stock));
                }
            }

            private void DownloadFile()
            {
                FtpWebRequest request = (FtpWebRequest)WebRequest.Create("ftp://NLGShop.de/NLG_Dropshipping_full.csv");
                request.Method = WebRequestMethods.Ftp.DownloadFile;
                request.Credentials = new NetworkCredential("nlgshw_23", "MFGiYNUE8ghZtdqv");
                request.KeepAlive = true;
                FtpWebResponse response = (FtpWebResponse)request.GetResponse();
                Stream responseStream = response.GetResponseStream();
                StreamReader reader = new StreamReader(responseStream);
                FileStream localFileStream = new FileStream("NLG_Dropshipping_full.csv", FileMode.Create);
                byte[] byteBuffer = new byte[2048];
                int bytesRead = responseStream.Read(byteBuffer, 0, 2048);
                try
                {
                    while (bytesRead > 0)
                    {
                        localFileStream.Write(byteBuffer, 0, bytesRead);
                        bytesRead = responseStream.Read(byteBuffer, 0, 2048);
                    }
                }
                catch (Exception ex) { Console.WriteLine(ex.ToString()); }
                Console.WriteLine("Download Complete, status {0}", response.StatusDescription);
                reader.Close();
                response.Close();

                localFileStream.Close();

                request = null;
            }
        }
    }


}
