﻿using DN_Classes.Logger;
using MongoDB.Driver;
using MongoDB.Driver.Builders;
using ProcuctDB.Suppliers.Playcom;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace SupplierUpdateSystem.Suppliers.Playcom
{
    public class PlaycomStockUpdate : IUpdate
    {
        string path = "ftp://213.136.88.154/invrpt/";
        string localpath = @"C:\DevNetworks\Uncategorized\Updater\SupplierUpdateSystem\Playcom\";
        //D:\Aljohn\Others\Playcom
        //ftp path C:\Consolidated\invrpt
      
        string username = "ftpuser";
        string password = "DevNetworks2015";
        PlaycomDB db = new PlaycomDB();
        List<string> allEansToBeUpdated = new List<string>();

        public void DoUpdate()
        {
            Console.WriteLine("Getting available files from ftp...");
            foreach (var filename in this.ListFilesByExtension(".CSV"))
            {
                Console.WriteLine(filename.Trim());
                //download all files from the server
                this.DownloadFile(filename.Trim());
                //delete all the file in the server
                //this.DeleteFileOnServer(filename);
            }

            Do();

            #region old code2
            ////open each file in the local directory
            //foreach (var file in files)
            //{
            //   //update each line of the file
            //    this.UpdateData(file.FullName);
            //    //get all the eans in the file
            //    allEansToBeUpdated.AddRange(File.ReadAllLines(file.FullName).Select(x => x.Split(';')[1]).ToList());
            //}

            //foreach (var entity in db.GetAllProducts())
            //{
            //    if (!this.isAlreadyUpdated(entity.product.ean))
            //    {
            //        db.UpdateQuantity(entity,true);
            //    }
            //}

            ////delete all old file in the local computer
            //foreach (var file in files)
            //{
            //    if (DateTime.Now - file.LastWriteTimeUtc > TimeSpan.FromDays(1))
            //    {
            //        File.Delete(file.FullName);
            //    }
            //}

            ////ArtNum;EAN-Code;ArtName;Systemkürzel;Verfügbar;Streetday;Lagerstatus;WE_Datum;PKG;EK_Netto;
            //this.product = new Product
            //{
            //    artnum = updateLine.Split(';')[0],
            //    ean = updateLine.Split(';')[1],
            //    artname = updateLine.Split(';')[2],
            //    systemkürzel = updateLine.Split(';')[3],
            //    verfugbar = updateLine.Split(';')[4],
            //    streetday = updateLine.Split(';')[5],
            //    lagerstatus = updateLine.Split(';')[6],
            //    we_datum = updateLine.Split(';')[7],
            //    pkg = Convert.ToDouble(updateLine.Split(';')[8].Replace(",", ".")),

            //};
            ////  BsonElement qty = Convert.ToInt32(updateLine.Split(';')[5], CultureInfo.InvariantCulture).ToBson();
            //this.plus = new BsonDocument();
            //plus["quantity"] = Convert.ToInt32(updateLine.Split(';')[6]);
            #endregion
            #region old code
            /*
            try
            {
                foreach (var file in new DirectoryInfo(localpath).GetFiles("*.CSV"))
                {
                    var length = file.Length;
                    if (file.Length > 1000000 && isFileForThisDay(file))
                    {
                        Console.WriteLine("Making zero for all quantity...");
                        foreach (var entity in db.GetAllProducts())
                        {
                             db.UpdateQuantity(entity, true);
                        }
                    }
                }

                var files = new DirectoryInfo(localpath).GetFiles("*.CSV");
                foreach (var file in files)
                {
                    if (DateTime.Now - file.LastWriteTimeUtc > TimeSpan.FromDays(1))
                    {
                        File.Delete(file.FullName);
                    }
                    else
                    {
                        this.UpdateData(file.FullName);

                        CopyFile(file);

                    }
                }
            }
          
            //Any or either of the foreach above got zero files.
            catch
            { }  */
#endregion
        }

        private void RemoveFileFromDirectory(FileInfo file)
        {
            File.Delete(file.FullName);
        }

        private void Do()
        {
            if (AllQuantityZeroActivator()) //if big file is on the list
            {
                SetAllToZero();
                CheckFileAndUpdate();
            }

            else //only small files
                CheckFileAndUpdate();
        }

        private void CheckFileAndUpdate()
        {
            ListOfFiles().ForEach(file =>
            {
                if (isFileForThisDay(file))
                    UpdateData(file.FullName);

                else
                    RemoveFileFromDirectory(file);
            });
        }

        private void SetAllToZero()
        {
            PlaycomDB.mongoCollection.Update(Query.Exists("plus.quantity"), Update.Set("plus.quantity", 0), UpdateFlags.Multi);
            //AppLogger.Log("Set all zero: success");
        }

        private List<FileInfo> ListOfFiles()
        {
            List<FileInfo> files = new List<FileInfo>();
            new DirectoryInfo(localpath).GetFiles("*.CSV").ToList() //check later if no files
             .ForEach(file =>
             {
                 files.Add(file);
                 CopyFile(file);

                 //AppLogger.Log("File: " +file.Name +" copied");
             });

            return files.OrderBy(file => file.LastWriteTime).ToList();
        }

        private bool AllQuantityZeroActivator()
        {
            bool flag = false;

            new DirectoryInfo(localpath).GetFiles("*.CSV").ToList() //check later if no files
            .ForEach(file =>
            {
                if (file.Length > 1000000 && isFileForThisDay(file)) //the big file
                {
                    flag = true;
                    //AppLogger.Log("Big file: " +file.Name);
                }
            });

            return flag;
        }

        private void UpdateData(string filename)
        {
            File.ReadAllLines(filename).Skip(1).ToList().ForEach(line =>
            {
                if (line.Split(';')[4].Trim() == "1" && Convert.ToInt32(line.Split(';')[6]) > 0)
                {
                    db.UpdateQuantity(new PlaycomEntity(line), false);
                    Console.WriteLine("Playcom" + "\t" + line.Split(';')[1]);

                    //AppLogger.Log("Updating: " + "artnr: " + line.Split(';')[1] + " stock: " + "3");
                }

                else
                {
                    db.UpdateQuantity(new PlaycomEntity(line), true);
                    Console.WriteLine("Playcom" + "\t" + line.Split(';')[1]);

                    //AppLogger.Log("Updating: " + "artnr: " + line.Split(';')[1] + " stock: " + "0");
                }
            });
        }

        private bool isAlreadyUpdated(string ean)
        {
            bool flag = false;
            foreach (var temp in allEansToBeUpdated)
            {
                if (temp == ean)
                {
                    flag = true;
                    break;
                }
            }
            return flag;
        }

        private static void CopyFile(FileInfo file)
        {
            string playcomPath = @"C:\Playcom\";
            if (!System.IO.Directory.Exists(playcomPath))
            {
                System.IO.Directory.CreateDirectory(playcomPath);
            }

            try
            {
                System.IO.File.Copy(file.FullName, playcomPath+file.Name, true);
            }

            catch { }
        }

        private bool isFileForThisDay(FileInfo file)
        {
            Match dateOfTheFile = new Regex(@"\d{8}").Match(file.ToString());
            DateTime _dateOfTheFile = DateTime.ParseExact(dateOfTheFile.Value,
                                        "yyyyMMdd",
                                        CultureInfo.InvariantCulture,
                                        DateTimeStyles.None);

            if (dateOfTheFile.Success && _dateOfTheFile.Day == DateTime.Now.Day)
                return true;

            else
                return false;

        }

        private List<string> ListFilesByExtension(string extension)
        {
            List<string> zipFiles = new List<string>();
            FtpWebRequest request = (FtpWebRequest)WebRequest.Create(path);
            request.Method = WebRequestMethods.Ftp.ListDirectory;
            request.Credentials = new NetworkCredential(username, password);
            FtpWebResponse response = (FtpWebResponse)request.GetResponse();
            Stream responseStream = response.GetResponseStream();
            StreamReader reader = new StreamReader(responseStream);
            zipFiles.AddRange(reader.ReadToEnd().Split('\n').Where(x => x.Contains(extension)));
            reader.Close();
            response.Close();
            return zipFiles;
        }

        public bool DeleteFileOnServer(string serverUri)
        {
            FtpWebRequest request = (FtpWebRequest)WebRequest.Create(path+serverUri);
            request.Method = WebRequestMethods.Ftp.DeleteFile;
            request.Credentials = new NetworkCredential(username, password);
            FtpWebResponse response = (FtpWebResponse)request.GetResponse();
            Console.WriteLine("Delete status: {0}", response.StatusDescription);
            response.Close();
            return true;
        }

        private void DownloadFile(string file)
        {
            FtpWebRequest request = (FtpWebRequest)WebRequest.Create(path + file);
            request.Method = WebRequestMethods.Ftp.DownloadFile;
            request.Credentials = new NetworkCredential(username, password);
            request.KeepAlive = true;
            FtpWebResponse response = (FtpWebResponse)request.GetResponse();
            Stream responseStream = response.GetResponseStream();
            StreamReader reader = new StreamReader(responseStream);
            FileStream localFileStream = new FileStream(localpath + file.Replace("\r", ""), FileMode.Create);
            byte[] byteBuffer = new byte[2048];
            int bytesRead = responseStream.Read(byteBuffer, 0, 2048);
            try
            {
                while (bytesRead > 0)
                {
                    localFileStream.Write(byteBuffer, 0, bytesRead);
                    bytesRead = responseStream.Read(byteBuffer, 0, 2048);
                }
            }
            catch (Exception ex) { Console.WriteLine(ex.ToString()); }
            Console.WriteLine("Download Complete, status {0}", response.StatusDescription);
            reader.Close();
            response.Close();

            localFileStream.Close();

            request = null;

            //AppLogger.Log("Download file: success");
        }


    }
}
