﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SupplierUpdateSystem.Suppliers.Playcom
{
    public class Playcom : ISupplier
    {
        private IUpdate update = null;

        public void Start()
        {
            update = new PlaycomStockUpdate();
            update.DoUpdate();
        }
    }
}
