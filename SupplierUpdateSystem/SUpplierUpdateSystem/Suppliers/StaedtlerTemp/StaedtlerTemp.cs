﻿using SupplierUpdateSystem.Suppliers.StaedtlerTemp.StaedtlerUpdate;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SupplierUpdateSystem.Suppliers.StaedtlerTemp
{
    class StaedtlerTemp: ISupplier
    {
        public void Start()
        {
            IUpdate update = null;
            update = new StaedtlerUpdateTemp();
            update.DoUpdate();

            //update = new StaedtlerProductUpdateTemp();
            //update.DoUpdate();
        }
    }
}
