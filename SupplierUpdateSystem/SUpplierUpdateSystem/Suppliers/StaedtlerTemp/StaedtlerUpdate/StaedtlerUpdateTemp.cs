﻿using DN_Classes.Logger;
using Limilabs.Client.IMAP;
using Limilabs.Mail;
using Limilabs.Mail.MIME;
using MongoDB.Bson;
using MongoDB.Driver;
using MongoDB.Driver.Builders;
using ProcuctDB.Suppliers.Staedtler;
using System;
using System.Collections.Generic;
using System.IO;
using System.IO.Compression;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SupplierUpdateSystem.Suppliers.StaedtlerTemp.StaedtlerUpdate
{
    public class StaedtlerUpdateTemp : IUpdate
    {
        private string hostname = string.Empty;
        private int port = 0;
        private bool useSsl = false;
        private string usrname = string.Empty;
        private string pswrd = string.Empty;
        //  private string filePath = ;
        private string fileName = string.Empty;
        private string zipFile = Properties.Settings.Default.staedtlerZipFile;
        private string unzipPath = string.Empty;
        private string theFile = string.Empty;
        private string kipFolder = "sm_kip\\";
        private string productFolder = "sm_u\\";
        private List<string> filesToDecompress = null;
        private List<string> filesToUpdate = new List<string>();
        private string logFile = Properties.Settings.Default.staedtlerLogFile;
        private string emailKIPFile = Properties.Settings.Default.emailKIPFile;
        private string emailProductFile = Properties.Settings.Default.emailProductFile;

        public StaedtlerUpdateTemp()
        {
            hostname = Properties.Settings.Default.hostname; // "smtp.gmail.com";
            port = Properties.Settings.Default.port; //995
            useSsl = true;
            usrname = Properties.Settings.Default.usr;
            pswrd = Properties.Settings.Default.psswrd;

            filesToDecompress = new List<string>()
            {
                zipFile + kipFolder,
                zipFile + productFolder
            };
        }

        public void DoUpdate()
        {
            DeleteFiles();

            if (ConnectAndDL())
            {
                Decompress();
                ReadTheFile();
                UpdateStaedtler();
            }

            else
            {
                //There is no file downloaded. No new files for update.
            }
        }

        private void UpdateStaedtler()
        {
            MongoDatabase db = Databases.GetDB("PD_Staedtler");
            MongoCollection<StaedtlerEntity> col = db.GetCollection<StaedtlerEntity>("staedtler");

            foreach (string file in filesToUpdate)
            {
                List<string> lines = null;

                //update KIP
                if (file.Contains(".008"))
                {
                    lines = File.ReadAllLines(file, Encoding.Default).ToList();
                    foreach (string line in lines)
                    {
                        LinesToUpdateKIP(col, line);
                    }

                    //AppLogger.Log("Updating: successs"); 
                    Console.WriteLine("Done staedtler kip update!");
                }

                //update product
                else if (file.Contains(".020"))
                {
                    lines = File.ReadAllLines(file, Encoding.Default).ToList();
                    foreach (string line in lines)
                    {
                        LinesToUpdateProduct(col, line);
                    }

                    //AppLogger.Log("Updating: successs"); 
                    Console.WriteLine("Done staedtler product update!");
                }
            }  
        }

        private static void LinesToUpdateProduct(MongoCollection<StaedtlerEntity> col, string line)
        {
            string[] splitted = line.Split(';');
            string ean = string.Empty;

            if (!string.IsNullOrEmpty(splitted[10]))
            {
                ean = splitted[10];
            }

            else if (!string.IsNullOrEmpty(splitted[11]))
            {
                ean = splitted[11];
            }

            else if (!string.IsNullOrEmpty(splitted[12]))
            {
                ean = splitted[12];
            }

            else { }

            IMongoQuery query = Query.And(Query.EQ("product.ean", ean));
            StaedtlerEntity item = col.FindOne(query);
            StaedtlerEntity staedtlerEntity = null;

            if (query != null)
            {
                ProductFieldsUpdate(col, splitted, query);
            }

            else 
            {
                staedtlerEntity = AddAsNewItem(splitted);
                col.Save(staedtlerEntity);
            }
        }

        private static void ProductFieldsUpdate(MongoCollection<StaedtlerEntity> col, string[] splitted, IMongoQuery query)
        {
            if (!string.IsNullOrEmpty(splitted[10]))
            {
                UpdateProduct(col, splitted, query, splitted[10]);
            }

            else if (!string.IsNullOrEmpty(splitted[11]))
            {
                UpdateProduct(col, splitted, query, splitted[11]);
            }

            else if (!string.IsNullOrEmpty(splitted[12]))
            {
                UpdateProduct(col, splitted, query, splitted[12]);
            }

            else { }
        }

        private static void UpdateProduct(MongoCollection<StaedtlerEntity> col, string[] splitted, IMongoQuery query, string eanFormat)
        {
            IMongoUpdate updateForEanSingleItem = Update.Set("product.ean", eanFormat)
                                                    .Set("product.supplierid", splitted[1])
                                                    .Set("product.glnreceiver", splitted[2])
                                                    .Set("product.pricelistnum", splitted[4])
                                                    .Set("product.docdate", splitted[5])
                                                    .Set("product.validfrom", splitted[6])
                                                    .Set("product.manufacturersku", splitted[14])
                                                    .Set("product.wholesalesku", splitted[15])
                                                    .Set("product.customstariffnum", splitted[16])
                                                    .Set("product.image1", splitted[21])
                                                    .Set("product.articletext1", splitted[41])
                                                    .Set("product.manufacturer", splitted[44])
                                                    .Set("product.netweight", splitted[45])
                                                    .Set("product.netweightunit", splitted[46])
                                                    .Set("product.grossweight", splitted[47])
                                                    .Set("product.grossweightunit", splitted[48])
                                                    .Set("product.diameter", splitted[49])
                                                    .Set("product.diameterunit", splitted[50])
                                                    .Set("product.height", splitted[51])
                                                    .Set("product.heightunit", splitted[52])
                                                    .Set("product.length", splitted[53])
                                                    .Set("product.lengthunit", splitted[54])
                                                    .Set("product.area", splitted[55])
                                                    .Set("product.areaunit", splitted[56])
                                                    .Set("product.volume", splitted[57])
                                                    .Set("product.volumeunit", splitted[58])
                                                    .Set("product.width", splitted[59])
                                                    .Set("product.widthunit", splitted[60])
                                                    .Set("product.minimumqty", splitted[61])
                                                    .Set("product.minimumqtyunit", splitted[62])
                                                    .Set("product.lotnumber", splitted[63])
                                                    .Set("product.lotnumberunit", splitted[64])
                                                    .Set("product.qtyperpack", splitted[67])
                                                    .Set("product.packunit", splitted[68])
                                                    .Set("product.countryoforigin", splitted[69])
                                                    .Set("product.specialcondition2", splitted[70])
                                                    .Set("product.vat", splitted[73])
                                                    .Set("product.lpptwvat", splitted[74])//list purchase price without vat
                                                    .Set("product.buyprice", splitted[74])
                                                    .Set("product.basisforlpptwvat", splitted[75])
                                                    .Set("product.unitforlpptwvat", splitted[76])
                                                    .Set("product.currencyforlpptwvat", splitted[77])
                                                    .Set("product.dateoflpptwvat", splitted[78])
                                                    .Set("product.packaging", splitted[267])
                                                    .Set("product.brandname", splitted[269])
                                                    .Set("product.orderunit", splitted[270])
                                                    .Set("product.classification", splitted[272]);

            col.Update(query, updateForEanSingleItem);
            //AppLogger.Log("Updating: " + "ean: " + splitted[11] + " buyprice: " + splitted[74] + " minimumQtyUnit: " + splitted[62] + " packingUnit: " + splitted[68]); 
            Console.WriteLine("Staedtler\t" + "supplierid: " + splitted[1] + "\tean: " + eanFormat);
        }

        private static void LinesToUpdateKIP(MongoCollection<StaedtlerEntity> col, string line)
        {
            string[] splitted = line.Split(';');
            IMongoQuery query= Query.EQ("product.ean", splitted[11]);
            StaedtlerEntity item = col.FindOne(query);
            
            StaedtlerEntity staedtlerEntity = null;

            if (item != null) 
            {
                col.Update(query, Updater(splitted));

                //AppLogger.Log("Updating: " + "ean: " + splitted[11] + " buyprice: " + splitted[12]); 
                Console.WriteLine("Staedtler\t" + "supplierid: " + splitted[1] + "\tean: " + splitted[11]);
            }

            //If item is not found.
            else 
            {
                //AddAsNewItem
                staedtlerEntity = AddAsNewItem(splitted);
                col.Save(staedtlerEntity);

                //AppLogger.Log("Updating: " + "ean: " + splitted[11] + " product.buyprice: " + splitted[12] + " kip.pricebasesbuyprice: " + splitted[13]);
                Console.WriteLine("Staedtler\t" + "supplierid: " + splitted[1] + "\tean: " + splitted[11]);
            }
        }

        private static StaedtlerEntity AddAsNewItem(string[] splitted)
        {
            StaedtlerEntity staedtlerEntity = new StaedtlerEntity();
            staedtlerEntity.id = new ObjectId();
            staedtlerEntity.product = new StaedtlerEntity.Product
            {
                ean = splitted[11],
                supplierid = splitted[0],
                glnreceiver = splitted[1],
                pricelistnum = splitted[2],
                docdate = splitted[3],
                validfrom = splitted[4],
                buyprice = Convert.ToDouble(splitted[12])
            };
            staedtlerEntity.kip = new StaedtlerEntity.KIP
            {
                ean = splitted[11],
                supplierid = splitted[0],
                glnreceiver = splitted[1],
                pricelistnum = splitted[2],
                docdate = splitted[3],
                validfrom = splitted[4],
                vertragsnummer = splitted[5],
                wahrungscode = splitted[6],
                positionsnummmer = splitted[9],
                verarbeitungskennzeichen = splitted[10],
                buyprice = Convert.ToDouble(splitted[12]),
                pricebasesbuyprice = splitted[13]
            };
            staedtlerEntity.plus = new BsonDocument();
            return staedtlerEntity;
        }

        private static IMongoUpdate Updater(string[] splitted)
        {
            IMongoUpdate update = Update.Set("kip.ean", splitted[11])
                                        .Set("kip.supplierid", splitted[0])
                                        .Set("kip.glnreceiver", splitted[1])
                                        .Set("kip.pricelistnum", splitted[2])
                                        .Set("kip.docdate", splitted[3])
                                        .Set("kip.validfrom", splitted[4])
                                        .Set("kip.vertragsnummer", splitted[5])
                                        .Set("kip.wahrungscode", splitted[6])
                                        .Set("kip.positionsnummmer", splitted[9])
                                        .Set("kip.verarbeitungskennzeichen", splitted[10])
                                        .Set("kip.buyprice", Convert.ToDouble(splitted[12]));

            return update;
        }   

        private bool ConnectAndDL()
        {
            bool haveFilesDownloaded = false;

            using (Imap imap = new Imap())
            {
                imap.ConnectSSL("imap.gmail.com"/*, 993*/); //No port and using only the default   // or ConnectSSL for SSL
                imap.Login(usrname, pswrd);
                imap.SelectInbox();
                List<long> uids = imap.Search(Limilabs.Client.IMAP.Flag.Unseen);

                if (uids.Count != 0)
                {
                    haveFilesDownloaded = ForeachFoundUids(imap, uids);
                }

                else
                {
                    uids = imap.Search(Limilabs.Client.IMAP.Flag.Seen);
                    haveFilesDownloaded = ForeachFoundUids(imap, uids);
                }

                imap.Close();

                //if (haveFilesDownloaded)
                //    //AppLogger.Log("Download file: successs");
                //else
                //    AppLogger.Log("Download file: fail");

            }
            return haveFilesDownloaded;
        }

        private bool ForeachFoundUids(Imap imap, List<long> uids)
        {
            bool haveFilesDownloaded = false;

            foreach (long uid in uids)
            {
                IMail email = new MailBuilder().CreateFromEml(imap.GetMessageByUID(uid));

                try
                {
                    MimeData mime = email.Attachments.First();
                    string subject = email.Subject.ToLower();
                    fileName = mime.SafeFileName;

                    if (mime.FileName.Contains("sm_kip") && subject.ToLower().Contains("kip_staedtler"))
                    {
                        using (StreamReader sr = new StreamReader(logFile + emailKIPFile))
                        {
                            //Filename not exists in log(list of zips already used to update)
                            if (!sr.ReadToEnd().Contains(fileName))
                            {
                                mime.Save(zipFile + kipFolder + mime.SafeFileName);
                                haveFilesDownloaded = true;
                                sr.Dispose();
                                using (StreamWriter sw = new StreamWriter(logFile + emailKIPFile, true))
                                {
                                    sw.WriteLine(fileName);
                                    sw.Dispose();
                                }

                                imap.MoveByUID(uid, "SeenByUpdaterTool");
                            }

                            else
                            {
                                sr.Dispose();
                            }
                        }
                    }
                }

                //No attachments.
                catch
                { }
            }

            return haveFilesDownloaded;
        }

        public void Decompress()
        {
            foreach (string file in filesToDecompress)
            {
                try
                {
                    string _zipFile = Directory.GetFiles(file).First();
                    unzipPath = zipFile;
                    ZipFile.ExtractToDirectory(_zipFile, file);

                    //Delete rar.
                    string fileToDel = Directory.GetFiles(file, "*.zip").First();
                    File.Delete(fileToDel);

                    //AppLogger.Log("Decompress and Delete rar: successs"); 
                }

                catch
                { }
            }
        }

        public void ReadTheFile()
        {
            foreach (string file in filesToDecompress)
            {
                try
                {
                    filesToUpdate.Add(Directory.GetFiles(file).First().ToString());
                }

                catch
                { }
            }
        }

        public void DeleteFiles()
        {
            try
            {
                var filePaths = filesToDecompress.Select(s => Directory.GetFiles(s, "*")).ToList();
                filePaths.ForEach(x => x.ToList().ForEach(y => File.Delete(y)));
            }

            catch
            {
                //No Files to Delete
            }
        }
    }
}
