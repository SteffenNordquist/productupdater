﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SupplierUpdateSystem.Suppliers.Faber2
{
    class Faber2: ISupplier
    {
        public void Start()
        {
            IUpdate update = null;

            update = new Faber2StockUpdate();

            update.DoUpdate();
        }

    }
}
