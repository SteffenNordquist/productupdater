﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SupplierUpdateSystem.Suppliers.SoundOfSpirit
{
    public class SoundOfSpirit: ISupplier
    {
        private IUpdate update = null;

        public void Start() 
        {
            update = new SoundOfSpiritStockUpdate();

            update.DoUpdate();
        }
    }
}
