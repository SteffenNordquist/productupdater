﻿using MongoDB.Driver;
using MongoDB.Driver.Builders;
using ProcuctDB.SoundOfSpirit;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using OpenQA;
using OpenQA.Selenium;
using OpenQA.Selenium.Firefox;
using OpenQA.Selenium.Support.UI;
using OpenQA.Selenium.Remote;
using DN_Classes.Logger;

namespace SupplierUpdateSystem.Suppliers.SoundOfSpirit
{
    public class SoundOfSpiritStockUpdate: IUpdate
    {
        private MongoDatabase Database = null;
        private MongoCollection<SoundOfSpiritEntity> Collection = null;

        private string csvpath = @"C:\DevNetworks\Uncategorized\Updater\SupplierUpdateSystem\preisliste.csv";

        public void DoUpdate() 
        {
            SetDB();

            SetCollection();

            DownloadFile();

            UpdateStock();

            UpdatePrice();

        }

        private void DownloadFile() {

            //param
            string param = "kundennummer=37197&plz=10117";

            //login url
            string url = "https://www.sound-spirit.de/shop/login.php";

            //will request through the link
            HttpWebRequest request = (HttpWebRequest)WebRequest.Create(url);
            request.Method = "POST";
            request.ContentLength = param.Length;
            request.ContentType = "application/x-www-form-urlencoded";
            request.CookieContainer = new CookieContainer();

                //initializing the response holder
                HttpWebResponse response = null;

                //will request through the link
                request = (HttpWebRequest)WebRequest.Create("https://www.sound-spirit.de/shop/preisliste.csv.php?sid=f4b5f2921392e91f68621ca674395af3");
                request.Timeout = 30000;  //8000 Not work //timeout property ni request => setting length of time before mo-brownout

                //kwaon ang responde ni 'request'
                response = (System.Net.HttpWebResponse)request.GetResponse();

                //kwaon ang ice Stream ni response
                Stream s = response.GetResponseStream();

                //initializing fileStream to write the stream
                FileStream fileStream = new FileStream(csvpath, FileMode.OpenOrCreate, FileAccess.Write);

                //nagsulat didto sa fileStream
                byte[] buff = new byte[102400];
                int c = 0;
                while ((c = s.Read(buff, 0, 10400)) > 0)
                {
                    fileStream.Write(buff, 0, c);
                    fileStream.Flush();
                }
                fileStream.Close();
                s.Close();

                Console.WriteLine("file downloaded");

                //AppLogger.Log("Download file: success");
            
        }

        private void SetDB()
        {
            Database = Databases.GetDB("PD_SoundOfSpirit");
        }

        private void SetCollection()
        {
            Collection = Database.GetCollection<SoundOfSpiritEntity>("soundofspirit");
        }

        private void UpdateStock()
        {
           

            string[] file = File.ReadAllText(csvpath, Encoding.Default).Split(new Char[] { ';', '\r', '\n' },
                StringSplitOptions.RemoveEmptyEntries);

            int itemsStock0 = 0;
            int itemsStock3 = 0;

            foreach (SoundOfSpiritEntity c in Collection.FindAll().SetFields("product.articlenumber"))
            {
                
                int checkStringInCsv = file.Where(i => i.ToLower() == c.product.articlenumber.ToLower()).Count();

                int quantity = 0;

                if (checkStringInCsv == 0)
                {
                    quantity = 0;

                    SetUpdater<SoundOfSpiritEntity> setUpdater = new SetUpdater<SoundOfSpiritEntity>(c, true);
                    setUpdater.UpdateForOrganizedEntities();
                    itemsStock0++;
                }
                else
                {
                    quantity = 3;
                    SetUpdater<SoundOfSpiritEntity> setUpdater = new SetUpdater<SoundOfSpiritEntity>(c, false);
                    setUpdater.UpdateForOrganizedEntities();
                    itemsStock3++;
                }

                Collection.Update(Query.EQ("product.articlenumber", c.product.articlenumber), Update.Set("plus.quantity", quantity));
                Console.WriteLine("soundofspirit update : " + c.product.articlenumber + " : " + quantity);

                //AppLogger.Log("Updating: " + "artnr: " + c.product.articlenumber + " stock: " + quantity); 
            }

          
            
        }

        private void UpdatePrice()
        {
            List<string> linesFromCsv = File.ReadAllLines(csvpath).ToList().Select(x => x).Where(x => x != String.Empty).ToList();

            linesFromCsv.RemoveAt(0);
            foreach (string lineFromCsv in linesFromCsv)
            {
                string[] split = lineFromCsv.Split(';');
                if (split.Count() > 7)
                {
                    string articleNumber = split[1];
                    double buyPrice = Convert.ToDouble(split[7].Replace(",", "."));
                    Console.WriteLine("SOS" +"\tartnr: "+ articleNumber);
                    Collection.Update(Query.EQ("product.articlenumber", articleNumber), Update.Set("product.preis", buyPrice));

                    //AppLogger.Log("Updating: " + "artnr: " + articleNumber + " price: " + buyPrice); 
                }

            }
        }
    }
}
