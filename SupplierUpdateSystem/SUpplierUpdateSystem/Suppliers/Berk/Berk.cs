﻿using MongoDB.Driver;
using MongoDB.Driver.Builders;
using MongoDB.Bson;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using ProcuctDB.Berk;
using DN_Classes.Logger;
using SupplierUpdateSystem.Suppliers.Sets;

namespace SupplierUpdateSystem.Suppliers.Berk
{
    public class BerkUpdater : ISupplier
    {
        public void Start()
        {          
            IUpdate update = null;

            update = new BerkStockUpdate();
            update.DoUpdate();
      
        }
    }

    public class BerkStockUpdate : IUpdate
    {

        public void DoUpdate()
        {
            if (DownloadFile())
            {
                new BerkDB();
                SetAllToZero();
                Do();
            }
        }

        private static void Do()
        {
            var col = BerkDB.mongoCollection;
            List<string> sources = System.IO.File.ReadAllLines("Berk_Lagerbestandexport.csv").ToList();
            foreach (string source in sources)
            {
                string articleNumber = source.Split(';')[0].Replace("\"", "");
                if (articleNumber == "HS-9")
                { }
                int stock = Convert.ToInt32(source.Split(';')[1].Replace("\"", ""));
                IMongoQuery query = Query.EQ("artikelnumber", articleNumber);
                col.Update(query, Update.Set("stock", stock));

                UpdateSets(col, stock, query);
     
                //AppLogger.Log("Updating: " +"artnr: " +articleNumber +" stock: " +stock); 
                Console.WriteLine("Berk" + "\tartnr: " + articleNumber);
            }

            //AppLogger.Log("Update: success");
        }

        private static void UpdateSets(MongoCollection<BerkEntity> col, int stock, IMongoQuery query)
        {
            var item = col.FindOne(query);

            if(item != null)
            {
                if (stock == 0)
                {
                    SetUpdater<BerkEntity> setUpdater = new SetUpdater<BerkEntity>(item, true);
                    setUpdater.UpdateForAsinOutsideOrInsidePlus();
                }

                else if (stock > 0)
                {
                    SetUpdater<BerkEntity> setUpdater = new SetUpdater<BerkEntity>(item, false);
                    setUpdater.UpdateForAsinOutsideOrInsidePlus();
                }
            }
        }



        private void SetAllToZero()
        {
            BerkDB.mongoCollection.Update(Query.Exists("plus.stock"), Update.Set("plus.stock", 0), UpdateFlags.Multi);
            //AppLogger.Log("Set all zero: success");
        }

        private static bool DownloadFile()
        {
            using (WebClient Client = new WebClient())
            {
                bool flag = false;
                try
                {
                    Client.DownloadFile("http://www.berk.de/von_hinten/lager_csv_statischer_name.php", "Berk_Lagerbestandexport.csv");
                    return flag = true;

                    //AppLogger.Log("Download file: success");
                }

                catch { /*AppLogger.Log("Download file: fail");*/ return flag; }
            }
        }
    }
}
