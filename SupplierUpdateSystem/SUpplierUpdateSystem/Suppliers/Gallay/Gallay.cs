﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SupplierUpdateSystem.Suppliers.Gallay
{
    class Gallay : ISupplier
    {
        public void Start()
        {
            IUpdate update = null;

            update = new GallayStockUpdate();

            update.DoUpdate();

        }
    }
}
