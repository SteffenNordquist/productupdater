﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ProcuctDB.Gallay;
using System.Net;
using DN_Classes.Logger;
using MongoDB.Driver.Builders;
using System.Globalization;
namespace SupplierUpdateSystem.Suppliers.Gallay
{
    class GallayStockUpdate : IUpdate
    {
        public void DoUpdate()
        {
            List<string> updateLines = new List<string>();
            using (WebClient Client = new WebClient())
            {
                Client.DownloadFile(@"http://uni.csv4you.de/NTcyfGJlc3RhbmRfMTcxLmNzdg==", @"C:\DevNetworks\Uncategorized\Updater\SupplierUpdateSystem\gallay.csv");
                updateLines = System.IO.File.ReadAllLines(@"C:\DevNetworks\Uncategorized\Updater\SupplierUpdateSystem\gallay.csv").ToList();
                updateLines.RemoveAt(0);
                
                Client.Dispose();

                //AppLogger.Log("Download file: success");
            }
            //instantiate
            GallayDB db = new GallayDB();
            List<GallayEntity> allGallays = db.GetAllProducts();
            List<string> logs = new List<string>();
            //read all rows
          //list all itemnos
            List<GallayEntity> setZero=new List<GallayEntity>();
            var itemnos = updateLines.Select(x => x.Split(';')[0]).ToList();

            Parallel.ForEach(allGallays, new ParallelOptions{ MaxDegreeOfParallelism = 5 },
            entity =>
                {
                    if (!itemnos.Contains(entity.product.itemno))
                    {
                        setZero.Add(entity);
                    }
                });
            #region
            //foreach (GallayEntity entity in allGallays) {
            //    if (!itemnos.Contains(entity.product.itemno))
            //    {
            //        setZero.Add(entity);
            //    }
            //}
            #endregion
            //set those items not in the CSV to 0

            Parallel.ForEach(setZero, new ParallelOptions { MaxDegreeOfParallelism  = 5 },
                entity =>
                {
                    entity.product.stock = 0;
                    db.Save(entity);
                    logs.Add(entity.product.itemno + "\t" + 0);
                    //AppLogger.Log("Set zero: " + entity.product.itemno +"\t" +0); 
                });

            #region
            //foreach (GallayEntity entity in setZero) {
            //    entity.product.stock = 0;
            //    db.Save(entity);
            //    logs.Add(entity.product.itemno + "\t" + 0);
            //}
            #endregion

            //update the qty

            Parallel.ForEach(updateLines, new ParallelOptions { MaxDegreeOfParallelism = 1 },
                line =>
                {
                    GallayEntity item = GallayDB.mongoCollection.FindOne(Query.EQ("product.itemno", line.Split(';')[0]));
                    
                    if (item != null)
                    {
                        item.product.itemno = line.Split(';')[0];
                        item.product.stock = Convert.ToInt32(line.Split(';')[1]);
                        item.product.lstpr = Convert.ToDouble(line.Split(';')[2].ToString().Replace(",", "."), CultureInfo.InvariantCulture);
                        item.product.status = Convert.ToInt16(line.Split(';')[3]);

                        GallayDB.mongoCollection.Save(item);
                        Console.WriteLine("Gallay" + "\titemnum: " + line.Split(';')[0]);
                        UpdateSets(item);
                    }
                  
                    //AppLogger.Log("Updating: " +line.Split(';')[0] + "\t" + line.Split(';')[1]);
                    //logs.Add(line.Split(';')[0] + "\t" + line.Split(';')[1]);
                });

            #region
            //foreach(var line in updateLines){
            //    db.Save(new GallayEntity(line));
            //    logs.Add(line.Split(';')[0] + "\t" + line.Split(';')[1]);
            //}
            #endregion

            System.IO.File.WriteAllLines(@"C:\DevNetworks\Uncategorized\Updater\SupplierUpdateSystem\gallaylog.txt", logs);
        }

        private static void UpdateSets(GallayEntity item)
        {
            if (item.product.stock == 0)
            {
                SetUpdater<GallayEntity> setUpdater = new SetUpdater<GallayEntity>(item, true);
                setUpdater.UpdateForOrganizedEntities();
            }

            else
            {
                SetUpdater<GallayEntity> setUpdater = new SetUpdater<GallayEntity>(item, false);
                setUpdater.UpdateForOrganizedEntities();
            }
        }
    }
}
