﻿using DN_Classes.Logger;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SupplierUpdateSystem.Suppliers.TestSupplier
{
    public class TestSupplier: ISupplier
    {
        public void Start()
        {
            IUpdate update = null;

            update = new TestSupplierUpdate();
            update.DoUpdate();

        }
    }

    public class TestSupplierUpdate: IUpdate
    {
        public void DoUpdate()
        {
            DoDownload();
            SetAllToZero();
                Do();
        }

        private bool DoDownload()
        {
            //AppLogger.Log("Download: success");
            return true;   
        }

        private bool SetAllToZero()
        {
            //AppLogger.Log("Set all to zero: success");
            return true;
        }

        private bool Do()
        {
            //AppLogger.Log("Update: success");
            return true;
        }
    }
}
