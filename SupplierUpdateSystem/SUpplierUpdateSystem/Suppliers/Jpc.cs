﻿using MongoDB.Bson;
using MongoDB.Driver;
using MongoDB.Driver.Builders;
using System;
using System.Collections.Generic;
using System.IO;
using System.IO.Compression;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using ProcuctDB.Jpc;
namespace SupplierUpdateSystem.Suppliers
{
    class Jpc : ISupplier
    {

        public void Start()
        {
            IUpdate update = null;

            update = new JpcStockUpdate();
            update.Set();
            update.Save();
            update.DoUpdate();

        }
    }

    public class JpcStockUpdate : IUpdate
    {
        public ProcuctDB.Jpc.JpcDB jpcs = new JpcDB();
        public List<ProcuctDB.Jpc.JpcEntity> entities;
        private Object thisLock = new Object();
        private MongoDatabase db = Database.GetDB("PD_JPC");
        private MongoCollection<JpcEntity> col;// = db.GetCollection<JpcEntity>("jpc");

        public void Set()
        {

        }

        public void Save()
        {

        }
        public void RemoveFromList(string ean)
        {
            for (int i = 0; i < entities.Count; i++)
            {
                if (entities[i].ean.Equals(ean)) {
                    entities.RemoveAt(i);
                }
            }
        }
        public void DoUpdate()
        {
            col = db.GetCollection<JpcEntity>("jpc");
            entities = jpcs.GetAllJpc(Query.GT("bestStaffel", 0));
            Console.WriteLine(entities.Count);
            string[] filePaths = Directory.GetFiles("C:\\SupplierUpdateSystem\\TSV");
            foreach (string filePath in filePaths)
                File.Delete(filePath);

            Console.WriteLine("The following files were present in the FTP:");
            foreach (string file in GetZipFiles())
            {
                Console.WriteLine("\t" + file);
            }
            Console.WriteLine("Downloading each File:");
            Parallel.ForEach(GetZipFiles(), currentFile =>
            {
                Console.WriteLine("\t{0} is currently being handled by thread {1}", currentFile,
                     Thread.CurrentThread.ManagedThreadId);
                DownloadFile(currentFile);

                System.Threading.Thread.Sleep(100);
                Decompress(currentFile);
                System.Threading.Thread.Sleep(100);

                UpdateQuantity(currentFile.Replace(".zip", ".tsv"));
               
            });

            Console.WriteLine(entities.Count());
            foreach (JpcEntity entity in entities)
            {
                col.Update(Query.EQ("ean", entity.ean), Update.Set("bestStaffel", 0));
            }

            Console.WriteLine("JPC Done");
        }


        public static void Decompress(string fileName)
        {
            //   fileName.Replace("\r", "");
            string zipPath = "C:\\SupplierUpdateSystem\\TSV\\" + fileName.Trim().Replace("\r", "");
            string extractPath = "C:\\SupplierUpdateSystem\\TSV\\";
            ZipFile.ExtractToDirectory(zipPath, extractPath);

        }

        private List<string> GetZipFiles()
        {
            List<string> zipFiles = new List<string>();
            FtpWebRequest request = (FtpWebRequest)WebRequest.Create("ftp://partnerftp.jpc.de/");
            request.Method = WebRequestMethods.Ftp.ListDirectory;
            request.Credentials = new NetworkCredential("M28423509", "jf88do41");
            FtpWebResponse response = (FtpWebResponse)request.GetResponse();
            Stream responseStream = response.GetResponseStream();
            StreamReader reader = new StreamReader(responseStream);
            zipFiles.AddRange(reader.ReadToEnd().Split('\n').Where(x => x.Contains(".zip")));
            reader.Close();
            response.Close();
            return zipFiles;
        }

        private void DownloadFile(string file)
        {
            FtpWebRequest request = (FtpWebRequest)WebRequest.Create("ftp://partnerftp.jpc.de/" + file);
            request.Method = WebRequestMethods.Ftp.DownloadFile;
            request.Credentials = new NetworkCredential("M28423509", "jf88do41");
            request.KeepAlive = true;
            FtpWebResponse response = (FtpWebResponse)request.GetResponse();
            Stream responseStream = response.GetResponseStream();
            StreamReader reader = new StreamReader(responseStream);
            FileStream localFileStream = new FileStream("C:\\SupplierUpdateSystem\\TSV\\" + file.Replace("\r", ""), FileMode.Create);
            byte[] byteBuffer = new byte[2048];
            int bytesRead = responseStream.Read(byteBuffer, 0, 2048);
            try
            {
                while (bytesRead > 0)
                {
                    localFileStream.Write(byteBuffer, 0, bytesRead);
                    bytesRead = responseStream.Read(byteBuffer, 0, 2048);
                }
            }
            catch (Exception ex) { Console.WriteLine(ex.ToString()); }
            Console.WriteLine("Download Complete, status {0}", response.StatusDescription);
            reader.Close();
            response.Close();

            localFileStream.Close();

            request = null;
        }


        private void UpdateQuantity(string file)
        {
          
            List<string> sources = System.IO.File.ReadAllLines("C:\\SupplierUpdateSystem\\TSV\\" + file.Replace("\r", "")).Where(x => !x.Contains("jpc-Bestellnummer") && Convert.ToInt32(x.Split('\t')[7].Trim().Replace("\"", ""))>0).ToList();
            foreach (string source in sources)
            {
                string ean = source.Split('\t')[6].Replace("\"", "");
                int stock = Convert.ToInt32(source.Split('\t')[7].Trim().Replace("\"", ""));
                int newStock = 0;
                 if (stock > 20)
                {
                    newStock = 21;
                }
                else if (stock < 20 && stock >= 11)
                {
                    newStock = 11;
                }
                else if (stock < 11 && stock >= 6)
                {
                    newStock = 6;
                }
                else if (stock < 5 && stock >= 3)
                {
                    newStock = 3;
                }
                else if (stock < 2 && stock >= 1)
                {
                    newStock = 1;
                }
                else { newStock = 0; }
                col.Update(Query.EQ("ean", ean), Update.Set("bestStaffel", newStock));
                Console.WriteLine("JPC update : " + ean + "\t Bestand=" + stock + "\t Angabe=" + newStock);
                lock (thisLock) {
                    RemoveFromList(ean);
                }

            }
        }



    }

  
}
