﻿using MongoDB.Bson;
using MongoDB.Driver;
using MongoDB.Driver.Builders;
using System;
using System.Collections.Generic;
using System.IO;
using System.IO.Compression;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using ProcuctDB.Jpc;
using System.Globalization;
using DN_Classes.Logger;

namespace SupplierUpdateSystem.JPC
{
    public class JpcStockUpdate : IUpdate
    {
        public ProcuctDB.Jpc.JpcDB jpcs = new JpcDB();
        public List<ProcuctDB.Jpc.JpcEntity> entities;
        private Object thisLock = new Object();
        private MongoDatabase db = Databases.GetDB("PD_JPC");
        private MongoCollection<JpcEntity> col;// = db.GetCollection<JpcEntity>("jpc");
        private static string supplierUpdateSystemPath = @"C:\DevNetworks\Uncategorized\Updater\SupplierUpdateSystem\JPC\";

        public void RemoveFromList(string ean)
        {
            for (int i = 0; i < entities.Count; i++)
            {
                if (entities[i].ean.Equals(ean))
                {
                    entities.RemoveAt(i);
                }
            }
        }

        public void DoUpdate()
        {
            //Test
            //string test = "Duke Ellington (1899-1974)	Jam-A-Ditty	CD	4,41	7,99	1000051	0636943281321	0";
            //string price = test.Split('\t')[3];
            //string ean = test.Split('\t')[6].Replace("\"", "");
            //double buyPrice = Convert.ToDouble(test.Split('\t')[3].Replace(",", ".").Trim().ToString(), CultureInfo.InvariantCulture);
            //Test

            CreateDirectory(supplierUpdateSystemPath);

            col = db.GetCollection<JpcEntity>("jpc");
            entities = jpcs.GetAllProduct(Query.GT("bestStaffel", 0));
            Console.WriteLine(entities.Count);
            string[] filePaths = Directory.GetFiles(supplierUpdateSystemPath);
            foreach (string filePath in filePaths)
                File.Delete(filePath);

            Console.WriteLine("The following files were present in the FTP:");


            var zipFileNamesFromFtp = GetZipFileNames();

            foreach (string file in zipFileNamesFromFtp)
            {
                Console.WriteLine("\t" + file);
            }
            Console.WriteLine("Downloading each File:");

            Parallel.ForEach(zipFileNamesFromFtp, currentFile =>
            {
                try
                {
                    Console.WriteLine("\t{0} is currently being handled by thread {1}", currentFile,
                         Thread.CurrentThread.ManagedThreadId);
                    DownloadFile(currentFile);

                    System.Threading.Thread.Sleep(100);
                    Decompress(currentFile);
                    System.Threading.Thread.Sleep(100);

                    UpdateQuantity(currentFile.Replace(".zip", ".tsv"));
                }
                catch
                {
                }
            }
            );

            Console.WriteLine(entities.Count());
            foreach (JpcEntity entity in entities)
            {
                col.Update(Query.EQ("ean", entity.ean), Update.Set("bestStaffel", 0));
            }

            Console.WriteLine("JPC Done");
        }

        private static void CreateDirectory(string path)
        {
            Directory.CreateDirectory(path);
        }


        public static void Decompress(string fileName)
        {
            //   fileName.Replace("\r", "");
            string zipPath = supplierUpdateSystemPath + fileName.Trim().Replace("\r", "");
            ZipFile.ExtractToDirectory(zipPath, supplierUpdateSystemPath);

        }

        private List<string> GetZipFileNames()
        {
            List<string> zipFiles = new List<string>();
            FtpWebRequest request = (FtpWebRequest)WebRequest.Create("ftp://partnerftp.jpc.de/");
            request.Method = WebRequestMethods.Ftp.ListDirectory;
            request.Credentials = new NetworkCredential("M28423509", "jf88do41");
            FtpWebResponse response = (FtpWebResponse)request.GetResponse();
            Stream responseStream = response.GetResponseStream();
            StreamReader reader = new StreamReader(responseStream);
            zipFiles.AddRange(reader.ReadToEnd().Split('\n').Where(x => x.Contains(".zip")));
            reader.Close();
            response.Close();
            return zipFiles;

            //AppLogger.Log("Download file: success");
        }

        private void DownloadFile(string file)
        {
            FtpWebRequest request = (FtpWebRequest)WebRequest.Create("ftp://partnerftp.jpc.de/" + file);
            request.Method = WebRequestMethods.Ftp.DownloadFile;
            request.Credentials = new NetworkCredential("M28423509", "jf88do41");
            request.KeepAlive = true;
            FtpWebResponse response = (FtpWebResponse)request.GetResponse();
            Stream responseStream = response.GetResponseStream();
            StreamReader reader = new StreamReader(responseStream);
            FileStream localFileStream = new FileStream(@"C:\DevNetworks\Uncategorized\Updater\SupplierUpdateSystem\JPC\" + file.Replace("\r", ""), FileMode.Create);
            byte[] byteBuffer = new byte[2048];
            int bytesRead = responseStream.Read(byteBuffer, 0, 2048);
            try
            {
                while (bytesRead > 0)
                {
                    localFileStream.Write(byteBuffer, 0, bytesRead);
                    bytesRead = responseStream.Read(byteBuffer, 0, 2048);
                }
            }
            catch (Exception ex) { Console.WriteLine(ex.ToString()); }
            Console.WriteLine("Download Complete, status {0}", response.StatusDescription);
            reader.Close();
            response.Close();

            localFileStream.Close();

            request = null;
            //AppLogger.Log("Download file: success");
        }


        private void UpdateQuantity(string fileName)
        {
            //if the zipfile is not empty
            try
            {
                List<string> sourceLines = System.IO.File.ReadAllLines(@"C:\DevNetworks\Uncategorized\Updater\SupplierUpdateSystem\JPC\" + fileName.Replace("\r", ""), Encoding.UTF8).ToList();

                var sourceLinesForQuantityUpdate = sourceLines.ToList();
                UpdateQuantity(sourceLinesForQuantityUpdate);

                UpdatePrices(sourceLines);
            }
            catch
            {
            }

        }

        private void UpdatePrices(List<string> sourceLines)
        {

        }

        private void UpdateQuantity(List<string> sourceLines)
        {
            sourceLines.RemoveAt(0);
            foreach (string sourceLine in sourceLines)
            {
                string ean = sourceLine.Split('\t')[6].Replace("\"", "");
                IMongoQuery query = Query.EQ("ean", ean);
                int stock = Convert.ToInt32(sourceLine.Split('\t')[7].Trim().Replace("\"", ""));
                int newStock = 0;
                double buyPrice = Convert.ToDouble(sourceLine.Split('\t')[3].Replace(",", ".").Trim().ToString(), CultureInfo.InvariantCulture);
                if (stock > 20)
                {
                    newStock = 21;
                }
                else if (stock < 20 && stock >= 11)
                {
                    newStock = 11;
                }
                else if (stock < 11 && stock >= 6)
                {
                    newStock = 6;
                }
                else if (stock < 5 && stock >= 3)
                {
                    newStock = 3;
                }
                else if (stock < 2 && stock >= 1)
                {
                    newStock = 1;
                }
                else { 
                    newStock = 0;       
                }

                UpdateSets(query, newStock);

                col.Update(query, Update.Set("bestStaffel", newStock).Set("nettoEK", buyPrice));

                //AppLogger.Log("Updated: " + "ean: " + ean + " stock: " + stock + " nettoEK: " + buyPrice);
                Console.WriteLine("JPC update : " + ean + "\t Bestand=" + stock + "\t Angabe=" + newStock + "\t nettoEK=" + buyPrice);
                lock (thisLock)
                {
                    RemoveFromList(ean);
                }

            }
        }

        private void UpdateSets(IMongoQuery query, int newStock)
        {
            if (newStock == 0)
            {
                SetUpdater<JpcEntity> setUpdater = new SetUpdater<JpcEntity>(col.FindOne(query), true);
                setUpdater.UpdateForAsinOutsideOrInsidePlus();
            }

            else if (newStock > 0)
            {
                SetUpdater<JpcEntity> setUpdater = new SetUpdater<JpcEntity>(col.FindOne(query), false);
                setUpdater.UpdateForAsinOutsideOrInsidePlus();
            }
        }


    }
}
