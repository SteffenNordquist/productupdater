﻿using MongoDB.Driver;
using MongoDB.Driver.Builders;
using MongoDB.Bson;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;

namespace SupplierUpdateSystem.Suppliers
{
    public class Berk : ISupplier
    {
        
            
        public void Start()
        {
           
            IUpdate update = null;

            update = new BerkStockUpdate();
            update.Set();
            update.Save();
            update.DoUpdate();
      
        }
    }

    public class BerkStockUpdate : IUpdate
    {
        public void Set()
        {
            
        }

        public void Save()
        {
            
        }

        public void DoUpdate()
        {
            using (WebClient Client = new WebClient())
            {
                Client.DownloadFile("http://www.berk.de/von_hinten/lager_csv_statischer_name.php", "Berk_Lagerbestandexport.csv");
            }

            MongoDatabase db = Database.GetDB("PD_Berk");
            MongoCollection<BerkEntity> col = db.GetCollection<BerkEntity>("berks");
            List<string> sources = System.IO.File.ReadAllLines("Berk_Lagerbestandexport.csv").ToList();
            foreach (string source in sources)
            {
                string articleNumber = source.Split(';')[0].Replace("\"", "");
                int stock = Convert.ToInt32(source.Split(';')[1].Replace("\"", ""));
                Console.WriteLine("berk update : " + articleNumber);
                col.Update(Query.EQ("artikelnumber", articleNumber), Update.Set("stock", stock));                
            }
        }
    }

    public class BerkEntity{
        public ObjectId id { get; set; }
        public string ean { get; set; }
        public string artikelnumber { get; set; }
        public double gewicht { get; set; }
        public string price { get; set; }
        public string ve { get; set; }
        public int stock { get; set; }
        public List<string> asins { get; set; }
        public string einheit { get; set; }
        public string bezeichnung1 { get; set; }
        public string USER_WebshopText { get; set; }
        public string USER_MetaDescription { get; set; }
        public string USER_MetaKeywords { get; set; }
        public string USER_MetaTitle { get; set; }
        public string USER_WebshopText_EN { get; set; }
        public string USER_MetaDescription_EN { get; set; }
        public string USER_MetaKeywords_EN { get; set; }
        public string USER_MetaTitle_EN { get; set; }
        public BsonDocument plus { get; set; }
    }

}
