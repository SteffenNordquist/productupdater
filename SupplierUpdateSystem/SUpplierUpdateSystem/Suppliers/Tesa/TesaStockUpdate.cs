﻿using DN_Classes.Logger;
using Limilabs.Client.IMAP;
using Limilabs.Mail;
using Limilabs.Mail.MIME;
using MongoDB.Bson;
using MongoDB.Driver;
using MongoDB.Driver.Builders;
using ProcuctDB.Tesa;
using System;
using System.Collections.Generic;
using System.IO;
using System.IO.Compression;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SupplierUpdateSystem.Suppliers.Tesa
{
    class TesaStockUpdate: IUpdate
    {
       
        private string hostname = string.Empty;
        private int port = 0;
        private bool useSsl = false;
        private string usrname = string.Empty;
        private string pswrd = string.Empty;
      //  private string filePath = ;
        private string fileName = string.Empty;
        private string zipFile = Properties.Settings.Default.tesaZipFile;
        private string kipFolder = "fc_kip\\";
        private string productFolder = "fc_u\\";
        private string unzipPath = string.Empty;
        private string theFile = string.Empty;
        //logFile + emailKIPFile
        //mime.Save(zipFile +kipFolder + fileName);
        public TesaStockUpdate()
        {
            hostname = Properties.Settings.Default.hostname; // "smtp.gmail.com";
            port = Properties.Settings.Default.port; //995
            useSsl = true;
            usrname = Properties.Settings.Default.usr;
            pswrd = Properties.Settings.Default.psswrd;
        }

        public void DoUpdate()
        {
            DeleteFiles();

            if (ConnectAndDL())
            {
                Decompress();
                ReadTheFile();
                TesaUpdate();
            }

            else
            {
                //There is no file downloaded. No new files for update.
            }
        }

        private void TesaUpdate()
        {
            MongoDatabase db = Databases.GetDB("PD_TESA");
            MongoCollection<TesaEntity> col = db.GetCollection<TesaEntity>("tesa");
            List<string> lines = File.ReadAllLines(theFile, Encoding.Default).ToList();

            Parallel.ForEach(lines, new ParallelOptions { MaxDegreeOfParallelism = 5 },
                line =>
                {
                    string[] splitted = line.Split(';');
                    IMongoQuery query = Query.EQ("product.ean", splitted[11]);
                    TesaEntity item = col.FindOne(query);
                    TesaEntity tesaEntity = null;
                    if (query != null)
                    {
                        col.Update(query, Updater(splitted));

                        //AppLogger.Log("Updating: successs"); 
                        Console.WriteLine("Tesa\t" + "supplierid: " + splitted[0] + "\tean: " + splitted[11]);
                    }

                    else
                    {
                        //AddAsNewItem
                        tesaEntity = AddAsNewItem(splitted);
                        col.Save(tesaEntity);

                        //AppLogger.Log("Updating: successs"); 
                        Console.WriteLine("Tesa\t" + "supplierid: " + splitted[0] + "\tean: " + splitted[11]);
                    }
                });

        }

        private static IMongoUpdate Updater(string[] splitted)
        {
            IMongoUpdate update = Update.Set("kip.ean", splitted[11])
                                        .Set("kip.supplierid", splitted[0])
                                        .Set("kip.glnreceiver", splitted[1])
                                        .Set("kip.pricelistnum", splitted[2])
                                        .Set("kip.docdate", splitted[3])
                                        .Set("kip.validfrom", splitted[4])
                                        .Set("kip.vertragsnummer", splitted[5])
                                        .Set("kip.wahrungscode", splitted[6])
                                        .Set("kip.positionsnummmer", splitted[9])
                                        .Set("kip.verarbeitungskennzeichen", splitted[10])
                                        .Set("kip.buyprice", Convert.ToDouble(splitted[12]));

            return update;
        }   

        private static TesaEntity AddAsNewItem(string[] splitted)
        {
            TesaEntity tesaEntity = new TesaEntity();
            tesaEntity.id = new ObjectId();
            tesaEntity.product = new ProcuctDB.Tesa.TesaEntity.Product
            {
                ean = splitted[11],
                supplierid = splitted[0],
                glnreceiver = splitted[1],
                pricelistnum = splitted[2],
                docdate = splitted[3],
                validfrom = splitted[4],
                buyprice = Convert.ToDouble(splitted[12])
            };
            tesaEntity.kip = new ProcuctDB.Tesa.TesaEntity.KIP
            {
                ean = splitted[11],
                supplierid = splitted[0],
                glnreceiver = splitted[1],
                pricelistnum = splitted[2],
                docdate = splitted[3],
                validfrom = splitted[4],
                vertragsnummer = splitted[5],
                wahrungscode = splitted[6],
                positionsnummmer = splitted[9],
                verarbeitungskennzeichen = splitted[10],
                buyprice = Convert.ToDouble(splitted[12]),
                pricebasesbuyprice = splitted[13]
            };
            tesaEntity.plus = new BsonDocument();
            return tesaEntity;
        }

        private static void AddKIPDoc(TesaEntity query)
        {
            query.kip = new ProcuctDB.Tesa.TesaEntity.KIP
            {
                ean = string.Empty,
                supplierid = string.Empty,
                glnreceiver = string.Empty,
                pricelistnum = string.Empty,
                docdate = string.Empty,
                validfrom = string.Empty,
                vertragsnummer = string.Empty,
                wahrungscode = string.Empty,
                positionsnummmer = string.Empty,
                verarbeitungskennzeichen = string.Empty,
                buyprice = 0.0,
                pricebasesbuyprice = string.Empty
            };
        }

        private bool ConnectAndDL()
        {
            bool haveFilesDownloaded = false;

            using (Imap imap = new Imap())
            {
                imap.ConnectSSL("imap.gmail.com"/*, 993*/); //No port and using only the default   // or ConnectSSL for SSL
                imap.Login(usrname, pswrd);
                imap.SelectInbox();

                //AppLogger.Log("Login: successs"); 

                List<long> uids = imap.Search(Limilabs.Client.IMAP.Flag.Unseen);

                if (uids.Count != 0)
                {
                    haveFilesDownloaded = ForeachFoundUids(imap, uids);
                }

                else
                {
                    uids = imap.Search(Limilabs.Client.IMAP.Flag.Seen);
                    haveFilesDownloaded = ForeachFoundUids(imap, uids);
                }

                imap.Close();

                //if (haveFilesDownloaded)
                //    AppLogger.Log("Download file: successs");
                //else
                //    AppLogger.Log("Download file: fail");

            }
            return haveFilesDownloaded;
        }

        private bool ForeachFoundUids(Imap imap, List<long> uids)
        {
            bool haveFilesDownloaded = false;

            foreach (long uid in uids)
            {
                IMail email = new MailBuilder().CreateFromEml(imap.GetMessageByUID(uid));
                //Console.WriteLine(email.Subject);

                try
                {
                    MimeData mime = email.Attachments.First();
                    string subject = email.Subject.ToLower();
                    fileName = mime.SafeFileName;

                    if (mime.FileName.Contains("te_kip") && subject.ToLower().Contains("kip_tesa"))
                    {
                        using (StreamReader sr = new StreamReader(@"C:\DevNetworks\Uncategorized\Updater\SupplierUpdateSystem\Tesa\logs\emailKIPFiles.txt"))
                        {
                            //Filename not exists in log(list of zips already used to update)
                            if (!sr.ReadToEnd().Contains(fileName))
                            {
                                mime.Save(zipFile +kipFolder + fileName);
                                haveFilesDownloaded = true;
                            }

                            sr.Dispose();

                            using (StreamWriter sw = new StreamWriter(@"C:\DevNetworks\Uncategorized\Updater\SupplierUpdateSystem\Tesa\logs\emailKIPFiles.txt", true))
                            {
                                //sw.WriteLine(fileName);
                                sw.Dispose();
                            }                                
                        }

                        imap.MoveByUID(uid, "SeenByUpdaterTool");
                    }
                }

                //No attachments.
                catch
                { }
            }

            return haveFilesDownloaded;
        }

        public void Decompress()
        {
            var _zipFile = Directory.GetFiles(zipFile + kipFolder)[0];
            unzipPath = zipFile + kipFolder;
            ZipFile.ExtractToDirectory(_zipFile, unzipPath);

            //AppLogger.Log("Decompress: successs"); 
        }

        public void ReadTheFile()
        {
            theFile = Directory.GetFiles(zipFile +kipFolder, "*.008").First().ToString();
        }

        public void DeleteFiles()
        {
            try
            {
                List<string> filePaths = Directory.GetFiles(zipFile + kipFolder, "*").ToList();
                filePaths.ForEach(x => File.Delete(x));
            }

            catch
            { }
        }
    }
}
