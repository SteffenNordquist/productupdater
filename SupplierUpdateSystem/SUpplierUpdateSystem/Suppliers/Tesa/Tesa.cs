﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SupplierUpdateSystem.Suppliers.Tesa
{
    class Tesa : ISupplier
    {
        public void Start()
        {
            IUpdate update = null;

            update = new TesaStockUpdate();

            update.DoUpdate();
        }

    }
}
