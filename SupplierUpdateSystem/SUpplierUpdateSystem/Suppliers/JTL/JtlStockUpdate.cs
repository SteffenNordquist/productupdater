﻿using MongoDB.Driver;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Net;
using System.Diagnostics;
using System.IO;
using System.Text.RegularExpressions;
using DN_Classes.JTL_Error;
using MongoDB.Driver.Builders;
using MongoDB.Driver.Wrappers;
using OpenQA.Selenium.IE;
using ProcuctDB;
using ProcuctDB.JTL;
using WhereToBuy;
using DN_Classes.Logger;
using DN_Classes.Entities.Warehouse;
namespace SupplierUpdateSystem.Suppliers.JTL
{
    public class JtlStockUpdate : IUpdate
    {
        private static int countGoodEans = 0;
        private static int needsLeadingZeroEans = 0;
        private static int countPrefix = 0;
        private List<string> AllUpdatedEanList = new List<string>();


        public void DoUpdate()
        {
            try
            {
                JTLDB db = new JTLDB();
                using (WebClient Client = new WebClient())
                {
                    Client.DownloadFile(@"http://136.243.60.22:81/jtl.csv",
                        @"C:\DevNetworks\Uncategorized\Updater\SupplierUpdateSystem\jtl.csv");
                    Client.Dispose();

                    //AppLogger.Log("Download file: success");
                }
                var lines =
                    System.IO.File.ReadAllLines(@"C:\DevNetworks\Uncategorized\Updater\SupplierUpdateSystem\jtl.csv")
                        .Select(x => x.Replace("\"", ""))
                        .ToList();
                lines.RemoveAt(0);
                CheapestPrice whereToBuy = new CheapestPrice();
                int counter = 0;
                Console.WriteLine(lines.Count);
                //  int counter
                counter = 0;
                foreach (var line in lines)
                {
                    counter++;
                    string ean = FormatEan(line.Split(';')[0]);
                    IMongoQuery query = Query.EQ("product.ean", ean);
                    BuyPriceRowWithIProduct rowWithQtyIproduct = whereToBuy.GetCheapestPriceByEanForJTL(ean);
                    BuyPriceRowWithIProduct rowWithoutQtyIproduct = whereToBuy.GetCheapestPriceNoExcludedSupplier(ean);
                    string whereToBuyLine = "";
                    try
                    {
                        whereToBuyLine = rowWithQtyIproduct.getLine();
                    }
                    catch
                    {
                    }
                    string whereToBuyNoExcludedSupplier = "";
                    try { whereToBuyNoExcludedSupplier = rowWithoutQtyIproduct.getLine(); }
                    catch { }
                    IProduct iproductwithqty = null;
                    IProduct iproductwithoutqty = null;
                    try
                    {
                        iproductwithqty = rowWithQtyIproduct.product;
                    }
                    catch
                    {
                    }
                    try
                    {
                        iproductwithoutqty = rowWithoutQtyIproduct.product;
                    }
                    catch { }
                    JTLEntity entity;
                    Console.WriteLine(counter + "\t" + ean);
                    if (rowWithQtyIproduct != null)
                    {
                        try
                        {
                            entity = new JTLEntity(ean, line, whereToBuyLine, iproductwithqty);

                            UpdateSets(query, entity);

                            db.Save(entity);
                            //AppLogger.Log("Updated: " +ean +" " +line);
                        }
                        catch
                        {
                            entity = new JTLEntity(ean, line, "", iproductwithqty);
                            //AppLogger.Log("Updated: " + ean + " " + line);
                            db.Save(entity);
                        }
                    }
                    else
                    {
                        try
                        {
                            entity = new JTLEntity(ean, line, whereToBuyNoExcludedSupplier, iproductwithoutqty);
                            //AppLogger.Log("Updated: " + ean + " " + line);
                            db.Save(entity);

                        }
                        catch
                        {
                            entity = new JTLEntity(ean, line, "", iproductwithoutqty);
                            //AppLogger.Log("Updated: " + ean + " " + line);
                            db.Save(entity);
                        }
                    }
                    AllUpdatedEanList.Add(ean);
                }


                foreach (JTLEntity entity in db.GetAllProducts())
                {
                    if (!isInUpdate(entity.product.ean))
                    {
                        string whereToBuyNoExcludedSupplier = "";
                        try
                        {
                            whereToBuyNoExcludedSupplier = whereToBuy.GetCheapestPriceNoExcludedSupplier(entity.product.ean).getLine();
                        }
                        catch
                        {
                        }
                        entity.plus["quantity"] = 0;
                        try
                        {
                            entity.product.buyprice =
                                Convert.ToDouble(whereToBuyNoExcludedSupplier.Split('\t')[2].ToString());
                        }
                        catch
                        {
                        }

                        try
                        {
                            entity.product.supplier = whereToBuyNoExcludedSupplier.Split('\t')[6].ToString();
                        }
                        catch
                        {
                            entity.product.supplier = "";
                        }
                        db.Save(entity);
                        //AppLogger.Log("Updated: " +entity.getSKU() +" " + entity.GetEan() + " " + entity.getStock());
                    }
                }
            }
            catch (Exception ex)
            {
                JTLErrorLog log = new JTLErrorLog();
                //AppLogger.Log(ex.Message);
                log.AddError(ex.Message, "SupplierUpdateSystem");
                throw;
            }
            
        }

        private static void UpdateSets(IMongoQuery query, JTLEntity entity)
        {
            if (entity.plus["quantity"].AsInt32 == 0)
            {
                SetUpdater<JTLEntity> setUpdater = new SetUpdater<JTLEntity>(JTLDB.mongoCollection.FindOne(query), true);
                setUpdater.UpdateForOrganizedEntities();
            }

            else if (entity.plus["quantity"].AsInt32 != 0)
            {
                SetUpdater<JTLEntity> setUpdater = new SetUpdater<JTLEntity>(JTLDB.mongoCollection.FindOne(query), false);
                setUpdater.UpdateForOrganizedEntities();
            }
        }

        private bool isInUpdate(string entityEan)
        {
            bool flag = false;
            foreach (var ean in AllUpdatedEanList)
            {
                if (entityEan == ean)
                {
                    return true;
                    break;
                }
            }
            return flag;
        }

        private static string FormatEan(string old_ean)
        {
            Regex regex = new Regex(@"\d{11,13}");
        //    Regex regexWirthPrefix = new Regex(@"[A,L]\d*-.*");
            string ean = old_ean;
            if (regex.Match(old_ean).Success)
            {
                ean = regex.Match(old_ean).Value;
                while (ean.Length < 13)
                    ean = "0" + ean;
            }
            return ean;
        }

    }
}
