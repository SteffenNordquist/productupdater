﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SupplierUpdateSystem.Suppliers.JTL
{
    public class Jtl : ISupplier
    {
        private IUpdate update = null;

        public void Start()
        {
            update = new JtlStockUpdate();
            update.DoUpdate();
        }
    }
}
