﻿using DN_Classes.Logger;
using MongoDB.Driver;
using MongoDB.Driver.Builders;
using ProcuctDB.Rwgh;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;

namespace SupplierUpdateSystem.Suppliers.Rwgh
{
    public class RwghStockUpdate : IUpdate
    {

        public void DoUpdate()
        {
            //DownloadFile();
            if (DownloadFile())
            {
                new RwghDB();
                SetAllToZero();
                Do();
            }
            
        }

        private static void Do()
        {
            var col = RwghDB.mongoCollection;

            List<string> sources = System.IO.File.ReadAllLines("rwupdate.csv", Encoding.Default).Skip(1).ToList();

            foreach (string s in sources)
            {
                try
                {
                    RwghEntity entityFromDB = col.FindOne(Query.EQ("product.ean", s.Split(';')[1]));


                    col.Save(EntityBuilder(entityFromDB, s));
                    Console.WriteLine("Rwgh\t" + s.Split(';')[1] +"\tupdated");

                    //AppLogger.Log("Updating: " + "ean: " + s.Split(';')[1] + " stock: " + entityFromDB.plus["quantity"].AsInt32);
                }

                catch
                { }

                #region fields from downloaded file
                ////artnr
                ////product-name //title
                ////imagename
                ////shortdesc
                //product-description //description
                //size
                //color
                //variationname
                //parent-child //parentchild
                //parent-intern-nr //parentinternnr
                //relationship-type //relationshiptype
                //variation-theme //variationtheme
                //main-image-url //mainimageurl
                //shipping-weight //shippingweight
                //item-price //buyprice
                //currency
                //products_status  //activeinshop
                //categorie
                //manufacturer
                //recommended_retail_price //recommendedretailprice
                //attrib_agio //attribagio
                //imagepath //wala
                //image2 //wala
                //image3 //wala
                //image4 //wala
                //image5 //wala
                //image6 //wala
                //image7 //wala
                //image8 //wala
                //rw_rabatt //rwrabatt
                //new-item-price //newitemprice
                //image9 //wala
                //image10 //wala
                //image11 //wala
                //image12 //wala
                //image13 //wala
                //image14 //wala
                //image15 //wala
                //image16 //wala
                //image17 //wala
                #endregion
            }
        }

        private void SetAllToZero()
        {
            RwghDB.mongoCollection.Update(Query.Exists("plus.quantity"), Update.Set("plus.quantity", 0), UpdateFlags.Multi);
            //AppLogger.Log("Set all zero: success");
        }

        private static bool DownloadFile()
        {
            bool flag = false;
            using (WebClient Client = new WebClient())
            {
                try
                {
                    Client.DownloadFile("http://www.rw-grosshandel.com/export/rwgh_products_mp.csv", "rwupdate.csv");
                    //AppLogger.Log("Download file: success");
                    return flag = true;
                }

                catch { return flag; }
            }
        }

        private static RwghEntity EntityBuilder(RwghEntity entityFromDB, string line)
        { 
            var artnr = line.Split(';')[0];
            var ean = line.Split(';')[1];
            var title = line.Split(';')[2];
            var imagename = line.Split(';')[3];
            var shortdescription = line.Split(';')[4];
            var description = line.Split(';')[5];
            var size = line.Split(';')[6];
            var color = line.Split(';')[7];
            var variationname = line.Split(';')[8];
            var parentchild = line.Split(';')[9];
            var parentinternnr = line.Split(';')[10];
            var relationshiptype = line.Split(';')[11];
            var variationtheme = line.Split(';')[12];
            var mainimageurl = line.Split(';')[13];
            double shippingweight = Convert.ToDouble(line.Split(';')[14]);
            double buyprice = Convert.ToDouble(line.Split(';')[15]);
            var currency = line.Split(';')[16];
            var activeinshop = line.Split(';')[17];
            var categorie = line.Split(';')[18];
            var manufacturer = line.Split(';')[19];
            double recommendedretailprice = Convert.ToDouble(line.Split(';')[20]);
            var attribagio = line.Split(';')[21];
            var rwrabatt = line.Split(';')[30];
            double newitemprice= Convert.ToDouble(line.Split(';')[31]);

            entityFromDB.product.ean = ean;
            entityFromDB.product.artnr = artnr;
            entityFromDB.product.title= title;
            entityFromDB.product.shortdesc= shortdescription;
            entityFromDB.product.productdescription= description;
            entityFromDB.product.size= size;
            entityFromDB.product.color= color;
            entityFromDB.product.variationname= variationname;
            //entityFromDB.product.parentchild= parentchild;
            entityFromDB.product.parent_intern_nr= parentinternnr;
            entityFromDB.product.relationshiptype= relationshiptype;
            entityFromDB.product.variationtheme= variationtheme;
            entityFromDB.product.shippingweight= shippingweight;
            entityFromDB.product.buyprice= buyprice;
            entityFromDB.product.currency= currency;
            entityFromDB.product.active_in_shop= activeinshop;
            entityFromDB.product.categorie= categorie;
            entityFromDB.product.manufacturer= manufacturer;
            entityFromDB.product.recommended_retail_price= recommendedretailprice;
            entityFromDB.product.attrib_agio= attribagio;
            entityFromDB.product.rw_rabatt= rwrabatt;
            entityFromDB.product.newitemprice= newitemprice;
            entityFromDB.plus["imagename"]= imagename;
            entityFromDB.plus["mainimageurl"]= mainimageurl;        
            int quantity = 0;
            quantity = (Convert.ToInt32(activeinshop) > 0)? 3: 0; 
            entityFromDB.plus["quantity"] = quantity;

            UpdateSets(entityFromDB, quantity);

            return entityFromDB;

            #region
            //UpdateBuilder update = Update
            //    entityFromDBtitle", title)
            //    entityFromDBshortdescription", shortdescription)
            //    entityFromDBdescription", description)
            //    entityFromDBsize", size)
            //    entityFromDBcolor", color)
            //    entityFromDBvariationname", variationname)
            //    entityFromDBparentchild", parentchild)
            //    entityFromDBparentinternnr", parentinternnr)
            //    entityFromDBrelationshiptype", relationshiptype)
            //    entityFromDBvariationtheme", variationtheme)
            //    entityFromDBshippingweight", shippingweight)
            //    entityFromDBbuyprice", buyprice)
            //    entityFromDBcurrency", currency)
            //    entityFromDBactiveinshop", activeinshop)
            //    entityFromDBcategorie", categorie)
            //    entityFromDBmanufacturer", manufacturer)
            //    entityFromDBrecommendedretailprice", recommendedretailprice)
            //    entityFromDBattribagio", attribagio)
            //    entityFromDBrwrabatt", rwrabatt)
            //    entityFromDBnewitemprice", newitemprice)
            //    .Set("plus.imagename", imagename)
            #endregion
        }

        private static void UpdateSets(RwghEntity entityFromDB, int quantity)
        {
            if (quantity == 0)
            {
                SetUpdater<RwghEntity> setUpdater = new SetUpdater<RwghEntity>(entityFromDB, true);
                setUpdater.UpdateForAsinOutsideOrInsidePlus();
            }

            else if (quantity != 0)
            {
                SetUpdater<RwghEntity> setUpdater = new SetUpdater<RwghEntity>(entityFromDB, false);
                setUpdater.UpdateForAsinOutsideOrInsidePlus();
            }
        }



      
    }
}
