﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SupplierUpdateSystem.Suppliers.Rwgh
{
    public class Rwgh : ISupplier
    {
        private IUpdate update = null;

        public void Start()
        {
            update = new RwghStockUpdate();
            update.DoUpdate();
        }
    }
}
