﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SupplierUpdateSystem.Suppliers.Hkm
{
    public class Hkm : ISupplier
    {
        public void Start()
        {
            IUpdate update = null;

            update = new HkmStockUpdate();
            update.DoUpdate();

        }
    }
}
