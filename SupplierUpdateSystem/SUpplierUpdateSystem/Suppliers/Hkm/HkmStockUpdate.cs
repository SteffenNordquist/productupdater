﻿using DN_Classes.Logger;
using MongoDB.Driver;
using MongoDB.Driver.Builders;
using ProcuctDB.Suppliers.Hkm;
using ProcuctDB.Suppliers.Orion;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;

namespace SupplierUpdateSystem.Suppliers.Hkm
{
    public class HkmStockUpdate: IUpdate
    {
        public static string path = @"C:\DevNetworks\Uncategorized\Updater\SupplierUpdateSystem\Hkm\Verfuegbarkeiten.csv";

        /// <summary>
        /// No need to set all to zero, since the file has more lines than in our collection
        /// </summary>


        public void DoUpdate()
        {
            DownLoadFile();
            ReadFile();//And Update
        }

        private static void ReadFile()
        {
            new HkmDB();

            var lines = File.ReadAllLines(path, Encoding.Default).ToList();
            lines
                .ForEach(x =>
                {
                    string ean = x.Split(';')[4].Trim();
                    string stock = x.Split(';')[2].Trim();

                    if (ean != string.Empty)
                    {

                        try
                        {

                            HkmDB.mongoCollection.Update(Query.EQ("product.ean", ean), Update.Set("plus.quantity", double.Parse(stock)), UpdateFlags.Multi);

                            //AppLogger.Log("Updating: " + "ean: " + ean + " stock: " + stock); 
                            Console.WriteLine("ean updated" + ean + " " + stock);
                        }
                        catch
                        {
                            Console.WriteLine("error" + ean + " " + stock);
                        }

                    }

                    else
                    {
                        Console.WriteLine("empty ean");
                    }
                    
                });

            File.Delete(path);
        }

        private static void UpdateStock(string ean, IMongoQuery query, IMongoUpdate update, double stock)
        {
            UpdateSets(query, stock);

            HkmDB.mongoCollection.Update(query, update);
            Console.WriteLine(ean + "updated to " +stock.ToString());
        }

        private static void UpdateSets(IMongoQuery query, double stock)
        {
            if (stock == 0)
            {
                SetUpdater<HkmEntity> setUpdater = new SetUpdater<HkmEntity>(HkmDB.mongoCollection.FindOne(query), true);
                setUpdater.UpdateForAsinOutsideOrInsidePlus();
            }

            else if (stock > 0)
            {
                SetUpdater<HkmEntity> setUpdater = new SetUpdater<HkmEntity>(HkmDB.mongoCollection.FindOne(query), false);
                setUpdater.UpdateForAsinOutsideOrInsidePlus();
            }
        }

        private static void DownLoadFile()
        {
            string urlAddress = "http://www.hkm-sportsequipment.eu/web/Verfuegbarkeiten/verfuegbar/Verfuegbarkeiten.csv";
            string filePath = @"C:\DevNetworks\Uncategorized\Updater\SupplierUpdateSystem\Hkm\Verfuegbarkeiten.csv";

            try
            {
                System.Net.HttpWebRequest request = null;
                System.Net.HttpWebResponse response = null;
                request = (System.Net.HttpWebRequest)System.Net.HttpWebRequest.Create(urlAddress);
               // request.Credentials = new NetworkCredential("mail@protect-this-house.com", "7I7noriP");
                request.Timeout = 30000;
                response = (System.Net.HttpWebResponse)request.GetResponse();
                Stream s = response.GetResponseStream();
                if (File.Exists(filePath))
                {
                    File.Delete(filePath);
                }
                FileStream os = new FileStream(filePath, FileMode.OpenOrCreate, FileAccess.Write);
                byte[] buff = new byte[102400];
                int c = 0;
                while ((c = s.Read(buff, 0, 10400)) > 0)
                {
                    os.Write(buff, 0, c);
                    os.Flush();
                }
                os.Close();
                s.Close();

                //AppLogger.Log("Download file: success");

            }
            catch
            {
                return;
            }
            finally
            {
            }
        }
    }
}
