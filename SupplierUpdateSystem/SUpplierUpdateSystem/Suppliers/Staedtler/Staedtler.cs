﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SupplierUpdateSystem.Suppliers.Staedtler
{
    class Staedtler: ISupplier
    {
        public void Start()
        {
            IUpdate update = null;
            update = new StaedtlerKIPUpdate();
            update.DoUpdate();

            update = new StaedtlerProductUpdate();
            //update.DoUpdate();
        }
    }
}
