﻿using Limilabs.Client.IMAP;
using Limilabs.Mail;
using Limilabs.Mail.MIME;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SupplierUpdateSystem.Suppliers.Staedtler.Update.KIPUpdate
{
    public class FileDownload
    {
        private EmailConnect connect = null;
        private Imap imap = null;
        private List<long> uids = null;

        private string staedtlerZipPath = string.Empty;
        private string staedtlerLogPath = string.Empty;
        private string emailKIPFile = string.Empty;
        private string emailKIPFolder = string.Empty;

        public FileDownload()
        {
            connect = new EmailConnect();
            connect.Connect();

            imap = connect.imap;
            uids = connect.uids;

            staedtlerZipPath = Properties.Settings.Default.staedtlerZipFilePath;
            staedtlerLogPath = Properties.Settings.Default.staedtlerLogFilePath;
            emailKIPFile = Properties.Settings.Default.emailKIPFile;
            emailKIPFolder = "fc_kip\\";
        }

        public void Download()
        {
            foreach (long uid in uids)
            {
                

                try
                {
                    IMail email = new MailBuilder().CreateFromEml(imap.GetMessageByUID(uid));

                    MimeData mime = email.Attachments.First();
                    string subject = email.Subject.ToLower();
                    string fileName = mime.SafeFileName;

                    if (mime.FileName.Contains("sm_kip") && subject.ToLower().Contains("kip_staedtler"))
                    {
                        using (StreamReader sr = new StreamReader(staedtlerLogPath + emailKIPFile))
                        {
                            //Filename not exists in log(list of zips already used to update)
                            if (!sr.ReadToEnd().Contains(fileName))
                            {
                                mime.Save(staedtlerZipPath + emailKIPFolder + mime.SafeFileName);

                                using (StreamWriter sw = new StreamWriter(staedtlerLogPath + emailKIPFile, true))
                                {
                                    sw.WriteLine(fileName);
                                    sw.Dispose();
                                }
                            }
                            sr.Dispose();
                        }
                    }
                }

                catch
                { }
            }
        }
    }
}
