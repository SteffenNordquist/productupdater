﻿using SupplierUpdateSystem.Email;
using SupplierUpdateSystem.Suppliers.Staedtler.Update.KIPUpdate;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SupplierUpdateSystem.Suppliers.Staedtler
{
    class StaedtlerKIPUpdate : IUpdate
    {
        private EmailConnect connect = null;
        private FileDownload dl = null;
        private FileDecompress dc = null;
        private FileRead fr = null;
        private FileDelete fd = null;

        private List<string> filesToUpdate = null;

        public StaedtlerKIPUpdate()
        {
            //connect = new EmailConnect();
            //connect.Connect();

            dl = new FileDownload();
            dl.Download();

            dc = new FileDecompress();
            dc.Decompress();

            fr = new FileRead();
            filesToUpdate = fr.Read();

            fd = new FileDelete(filesToUpdate);
            fd.Delete();
            
        }

        public void DoUpdate()
        {
            //ConnectAndDL();
            //Decompress();
            //ReadTheFile();
            //Update();
            //DeleteFiles();
        }

        
    }
}
