﻿using DN_Classes.Logger;
using HtmlAgilityPack;
using MongoDB.Bson;
using MongoDB.Driver;
using MongoDB.Driver.Builders;
using ProcuctDB.Moses;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace SupplierUpdateSystem.Suppliers.Moses
{
    public class MosesStockUpdate : IUpdate
    {
        public void DoUpdate() {
            Console.WriteLine("Downloading CSV file");

            DownloadFile();
         }

        public void DownloadFile(){
            MongoDatabase db = Databases.GetDB("PD_Moses");
            MongoCollection<MosesEntity> col = db.GetCollection<MosesEntity>("moses");

            foreach(MosesEntity c in col.FindAll().SetFlags(QueryFlags.NoCursorTimeout)){
                WebClient wc = new WebClient() { Encoding = Encoding.UTF8 };
                wc.Headers.Add("user-agent", "Mozilla/4.0 (compatible; MSIE 6.0; Windows NT 5.2; .NET CLR 1.0.3705;)");
                //WebClient wc = new WebClient();
                string html = wc.DownloadString("http://shop.moses-verlag.de/suche/?url=shop&q=" + c.product.ean);//9783897771390
                wc.Encoding = Encoding.UTF8;

                HtmlDocument hd = new HtmlDocument();
                hd.LoadHtml(html);

                try
                {
                    HtmlNode productNode = hd.DocumentNode.SelectSingleNode(".//span[@class='product_list_price_value']/span");
                    Regex priceRegex = new Regex(@"\d{1,4}(\,\d{1,2})?"); //to get double
                    Match matchPrice = priceRegex.Match(productNode.InnerText);
                    HtmlNode productLinkNode = hd.DocumentNode.SelectSingleNode(".//div[@class='box_product_list f_l']/a");
                    string productLink = productLinkNode.Attributes["href"].Value.ToString();
                    Console.WriteLine("moses" + "\t" + c.product.ean + "\t" + "link found");
                    col.Update(Query.EQ("product.ean", c.product.ean), Update.Set("plus.productlink", productLink));

                    //AppLogger.Log("Updating: " + "ean: " + c.product.ean + " stock: " + stock); 
                    #region
                    //col.Update(Query.EQ("product.ean", c.product.ean), Update.Set("plus.quantity", 3));
                    //col.Update(Query.EQ("product.ean", c.product.ean), Update.Set("product.vk", matchPrice.ToString().Replace(',', '.')));
                    //Console.WriteLine("moses update : " + c.product.ean + "quantity to 3");
                    #endregion
                }

                catch
                {
                    #region
                    //col.Update(Query.EQ("product.ean", c.product.ean), Update.Set("plus.quantity", 0));
                    //Console.WriteLine("moses update:" + c.product.ean + "quantity to 0");
                    #endregion
                    Console.WriteLine("moses" + "\t" + c.product.ean + "\t" + "no link found");
                }
            }

            Console.WriteLine("Moses Done");
          
        }
    }   
}
