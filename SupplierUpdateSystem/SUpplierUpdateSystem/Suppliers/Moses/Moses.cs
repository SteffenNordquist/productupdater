﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SupplierUpdateSystem.Suppliers.Moses
{
    public class Moses: ISupplier
    {
        public void Start() {
            IUpdate update = null;
            update = new MosesStockUpdate();
             update.DoUpdate();
        }
    }
}
