﻿using MongoDB.Bson;
using MongoDB.Driver;
using MongoDB.Driver.Builders;
using ProcuctDB;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SupplierUpdateSystem.Suppliers.Sets
{

    public class SetsStockUpdate : IUpdate
    {
        private MongoClient client;
        private MongoServer server;
        private MongoDatabase db;
        private MongoCollection<BsonDocument> col;
        private string ean = "";
        private string asin = "";
        private bool zero = false;

        public SetsStockUpdate(string ean, string asin, bool zero)
        {
            this.ean = ean;
            this.asin = asin;
            this.zero = zero;
        }

        public void DoUpdate()
        {
            client = new MongoClient(@"mongodb://client148:client148devnetworks@136.243.44.111/PD_Sets");
            server = client.GetServer();
            db = server.GetDatabase("PD_Sets");
            col = db.GetCollection<BsonDocument>("sets");

            Do();
        }

        public void Do()
        {
            IMongoQuery query = Query.And(Query.Exists("items"), Query.EQ("items.ean", ean), Query.EQ("items.asin", asin));

            var items = col.Find(query);

            foreach (BsonDocument set in items)
            {
                BsonArray tempItems = set["items"].AsBsonArray;

                if (zero)
                    set["plus"]["quantity"] = 0;//set overall quantity to zero.
                else
                    set["plus"]["quantity"] = 3; 

                #region
                //foreach (var tempItem in tempItems)
                //{
                //    if (tempItem["ean"].AsString == ean && tempItem["asin"].AsString == asin)
                //    {
                //        //tempItem["quantity"] = "0";
                //        set["plus"]["quantity"] = 0; //set overall quantity to zero.
                //    }                    
                //}

                //set["items"] = tempItems; //set each item inside items to tempItems
                #endregion
                col.Save(set);
            }

            
        }
    }

}
