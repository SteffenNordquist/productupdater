﻿using DN_Classes.Logger;
using MongoDB.Bson;
using MongoDB.Driver;
using MongoDB.Driver.Builders;
using ProcuctDB.Suppliers.Orion;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;

namespace SupplierUpdateSystem.Suppliers.Orion
{
    public class OrionStockUpdate: IUpdate
    {
        public void DoUpdate()
        {
            DownLoadFile();
            new OrionDB();
            SetAllToZero();
            Do();
        }

        private static void SetAllToZero()
        {
            OrionDB.mongoCollection.Update(Query.Exists("plus.quantity"), Update.Set("plus.quantity", 0), UpdateFlags.Multi);
            //AppLogger.Log("Set all zero: success");
        }

        private static void Do()
        {
            var lines = File.ReadAllLines(@"C:\DevNetworks\Uncategorized\Updater\SupplierUpdateSystem\Orion\Orion.csv", Encoding.Default).Skip(1).ToList();
            lines
                .ForEach(x =>
                {
                    //var splits = x.Split(';');
                    string ean = x.Split(';')[23].Replace("\"", "");
                    string asdf = x.Split(';')[12];
                    IMongoQuery query = Query.EQ("product.ean", ean);

                    OrionEntity item = OrionDB.mongoCollection.FindOne(query);

                    if (item != null)
                        UpdateSpecificItem(x, query, item);

                    else
                        InsertNewItem(x, item);
                });
        }

        static void InsertNewItem(string x, OrionEntity item)
        {
            item = new OrionEntity();
            item.product = new OrionEntity.Product();
            item.plus = new BsonDocument();
            item.id = new ObjectId();

            EntityBuilder(x, item);

            OrionDB.mongoCollection.Save(item);
            Console.WriteLine(item.product.ean_code + "\tadded");
            //AppLogger.Log("Updating: " + "ean_code: " + item.product.ean_code + " stock: " + item.plus["quantity"].AsDouble); 
        }

        static void UpdateSpecificItem(string x, IMongoQuery query, OrionEntity item)
        {
            item = new OrionEntity();
            item.product = new OrionEntity.Product();
            item.plus = new BsonDocument();
            EntityBuilder(x, item);

            #region
            IMongoUpdate update = MongoDB.Driver.Builders.Update
                                 .Set("product.export_version", item.product.export_version)
                                .Set("product.current_version", item.product.current_version)
                                .Set("product.timestamp", item.product.timestamp)
                                .Set("product.product_id", item.product.product_id)
                                .Set("product.product_class", item.product.product_class)
                                .Set("product.product_name_DE", item.product.product_name_DE)
                                .Set("product.title_DE", item.product.title_DE)
                                .Set("product.product_name_EN", item.product.product_name_EN)
                                .Set("product.title_EN", item.product.title_EN)
                                .Set("product.title_IT", item.product.title_IT)
                                .Set("product.product_keyword", item.product.product_keyword)
                                .Set("product.special_price_flag", item.product.special_price_flag)
                                .Set("product.buyprice", item.product.buyprice)
                                .Set("product.sellprice", item.product.sellprice)
                                .Set("product.full_text_DE", item.product.full_text_DE)
                                .Set("product.full_text_EN", item.product.full_text_EN)
                                .Set("product.full_text_IT", item.product.full_text_IT)
                                .Set("product.label_name", item.product.label_name)
                                .Set("product.category_path", item.product.category_path)
                                .Set("product.category", item.product.category)
                                .Set("product.group_product", item.product.group_product)
                                .Set("product.single_image", item.product.single_image)
                                .Set("product.tax_id", item.product.tax_id)
                                .Set("product.ean", item.product.ean)
                                .Set("product.ean_code", item.product.ean_code)
                                .Set("product.battery_supply", item.product.battery_supply)
                                .Set("product.battery_combination", item.product.battery_combination)
                                .Set("product.product_image_100", item.product.product_image_100)
                                .Set("product.product_image_250", item.product.product_image_250)
                                .Set("product.product_image_500", item.product.product_image_500)
                                .Set("product.product_image_1100", item.product.product_image_1100)
                                .Set("product.material_description", item.product.material_description)
                                .Set("product.material_thickness", item.product.material_thickness)
                                .Set("product.total_length", item.product.total_length)
                                .Set("product.diameter", item.product.diameter)
                                .Set("product.weight", item.product.weight)
                                .Set("product.length", item.product.length)
                                .Set("product.width", item.product.width)
                                .Set("product.height", item.product.height)
                                .Set("product.contents", item.product.contents)
                                .Set("product.availability", item.product.availability)
                                .Set("product.delivery_week", item.product.delivery_week)
                                .Set("product.dress_size", item.product.dress_size)
                                .Set("product.shoe_size", item.product.shoe_size)
                                .Set("product.main_color", item.product.main_color)
                                .Set("product.color_description", item.product.color_description)
                                .Set("product.battery_type", item.product.battery_type)
                                .Set("product.packaging_size", item.product.packaging_size)
                                .Set("product.aroma", item.product.aroma)
                                .Set("product.isbn", item.product.isbn)
                                .Set("product.number_of_pages", item.product.number_of_pages)
                                .Set("product.total_duration", item.product.total_duration)
                                .Set("product.hardcore_flag", item.product.hardcore_flag)
                                .Set("product.age_rating", item.product.age_rating)
                                .Set("product.product_languages", item.product.product_languages)
                                .Set("product.country_of_origin_code", item.product.country_of_origin_code)
                                .Set("product.series_name", item.product.series_name)
                                .Set("product.reference_quantity", item.product.reference_quantity)
                                .Set("product.reference_price_factor", item.product.reference_price_factor)
                                .Set("product.promotional_packing", item.product.promotional_packing)
                                .Set("product.product_icons", item.product.product_icons)
                                .Set("product.selling_unit", item.product.selling_unit)
                                .Set("product.voc_percentage", item.product.voc_percentage)
                                //no barcode
                                .Set("product.barcode_type", item.product.barcode_type)
                                .Set("product.novelty_flag", item.product.novelty_flag)
                                .Set("product.food_information_flag", item.product.food_information_flag)
                                .Set("product.detailed_full_text_de", item.product.detailed_full_text_de)
                                .Set("product.group_id", item.product.group_id)
                                .Set("plus.quantity", item.plus["quantity"]);
            #endregion

            OrionDB.mongoCollection.Update(query, update, UpdateFlags.Multi);
            Console.WriteLine(item.product.ean_code + "\tupdated");
            //AppLogger.Log("Updating: " + "ean_code: " + item.product.ean_code + " stock: " + item.plus["quantity"].AsDouble); 
        }

        private static void EntityBuilder(string x, OrionEntity item)
        {
            try
            {
                item.product.export_version = x.Split(';')[0].Replace("\"", "");
                item.product.current_version = x.Split(';')[1].Replace("\"", "");
                item.product.timestamp = x.Split(';')[2].Replace("\"", "");
                item.product.product_id = x.Split(';')[3].Replace("\"", "");
                item.product.product_class = x.Split(';')[4].Replace("\"", "");
                item.product.product_name_DE = x.Split(';')[5].Replace("\"", "");
                item.product.title_DE = x.Split(';')[6].Replace("\"", "");
                item.product.product_name_EN = x.Split(';')[7].Replace("\"", "");
                item.product.title_EN = x.Split(';')[8].Replace("\"", "");
                item.product.title_IT = x.Split(';')[9].Replace("\"", "");
                item.product.product_keyword = x.Split(';')[10].Replace("\"", "");
                item.product.special_price_flag = x.Split(';')[11].Replace("\"", "");
                item.product.buyprice = Convert.ToDouble(x.Split(';')[12].Replace("\"", ""));
                item.product.sellprice = Convert.ToDouble(x.Split(';')[13].Replace("\"", ""));
                item.product.full_text_DE = x.Split(';')[14].Replace("\"", "");
                item.product.full_text_EN = x.Split(';')[15].Replace("\"", "");
                item.product.full_text_IT = x.Split(';')[16].Replace("\"", "");
                item.product.label_name = x.Split(';')[17].Replace("\"", "");
                item.product.category_path = x.Split(';')[18].Replace("\"", "");
                item.product.category = x.Split(';')[19].Replace("\"", "");
                item.product.group_product = x.Split(';')[20].Replace("\"", "");
                item.product.single_image = x.Split(';')[21].Replace("\"", "");
                item.product.tax_id = x.Split(';')[22].Replace("\"", "");
                item.product.ean_code = x.Split(';')[23].Replace("\"", "");
                item.product.ean = x.Split(';')[23].Replace("\"", "");
                item.product.battery_supply = x.Split(';')[24].Replace("\"", "");
                item.product.battery_combination = x.Split(';')[25].Replace("\"", "");
                item.product.product_image_100 = x.Split(';')[26].Replace("\"", "");
                item.product.product_image_250 = x.Split(';')[27].Replace("\"", "");
                item.product.product_image_500 = x.Split(';')[28].Replace("\"", "");
                item.product.product_image_1100 = x.Split(';')[29].Replace("\"", "");
                item.product.material_description = x.Split(';')[30].Replace("\"", "");
                item.product.material_thickness = x.Split(';')[31].Replace("\"", "");
                item.product.total_length = x.Split(';')[32].Replace("\"", "");
                item.product.diameter = x.Split(';')[33].Replace("\"", "");
                item.product.weight = x.Split(';')[34].Replace("\"", "");
                item.product.length = x.Split(';')[35].Replace("\"", "");
                item.product.width = x.Split(';')[36].Replace("\"", "");
                item.product.height = x.Split(';')[37].Replace("\"", "");
                item.product.contents = x.Split(';')[38].Replace("\"", "");
                item.product.availability = x.Split(';')[39].Replace("\"", "");
                item.product.delivery_week = x.Split(';')[40].Replace("\"", "");
                item.product.dress_size = x.Split(';')[41].Replace("\"", "");
                item.product.shoe_size = x.Split(';')[42].Replace("\"", "");
                item.product.main_color = x.Split(';')[43].Replace("\"", "");
                item.product.color_description = x.Split(';')[44].Replace("\"", "");
                item.product.battery_type = x.Split(';')[45].Replace("\"", "");
                item.product.packaging_size = x.Split(';')[46].Replace("\"", "");
                item.product.aroma = x.Split(';')[47].Replace("\"", "");
                item.product.isbn = x.Split(';')[48].Replace("\"", "");
                item.product.number_of_pages = x.Split(';')[49].Replace("\"", "");
                item.product.total_duration = x.Split(';')[50].Replace("\"", "");
                item.product.hardcore_flag = x.Split(';')[51].Replace("\"", "");
                item.product.age_rating = x.Split(';')[52].Replace("\"", "");
                item.product.product_languages = x.Split(';')[53].Replace("\"", "");
                item.product.country_of_origin_code = x.Split(';')[54].Replace("\"", "");
                item.product.series_name = x.Split(';')[55].Replace("\"", "");
                item.product.reference_quantity = x.Split(';')[56].Replace("\"", "");
                item.product.reference_price_factor = x.Split(';')[57].Replace("\"", "");
                item.product.promotional_packing = x.Split(';')[58].Replace("\"", "");
                item.product.product_icons = x.Split(';')[59].Replace("\"", "");
                item.product.selling_unit = x.Split(';')[60].Replace("\"", "");
                item.product.voc_percentage = x.Split(';')[61].Replace("\"", "");
                //no barcode
                item.product.barcode_type = x.Split(';')[63].Replace("\"", "");
                item.product.novelty_flag = x.Split(';')[64].Replace("\"", "");
                item.product.food_information_flag = x.Split(';')[65].Replace("\"", "");
                item.product.detailed_full_text_de = x.Split(';')[66].Replace("\"", "");
                item.product.group_id = x.Split(';')[67].Replace("\"", "");
                //no detailed_full_text_en

                string availability = item.product.availability.ToLower();
                UpdateSets(item, availability);
            }

            catch 
            { }

        }

        private static void UpdateSets(OrionEntity item, string availability)
        {
            if (availability == "j")
            {
                item.plus["quantity"] = 3.0;
                SetUpdater<OrionEntity> setUpdater = new SetUpdater<OrionEntity>(item, true);
                setUpdater.UpdateForAsinOutsideOrInsidePlus();
            }

            else
            {
                item.plus["quantity"] = 0.0;
                SetUpdater<OrionEntity> setUpdater = new SetUpdater<OrionEntity>(item, false);
                setUpdater.UpdateForAsinOutsideOrInsidePlus();
            }
        }

        private void DownLoadFile()
        {
            string urlAddress = "http://www.orion-grosshandel-shop.com/isroot/restricted/downloads/DE/productdata_v4_05_01.csv";
            string filePath = @"C:\DevNetworks\Uncategorized\Updater\SupplierUpdateSystem\Orion\Orion.csv";

            try
            {
                System.Net.HttpWebRequest request = null;
                System.Net.HttpWebResponse response = null;
                request = (System.Net.HttpWebRequest)System.Net.HttpWebRequest.Create(urlAddress);
                request.Credentials = new NetworkCredential("mail@protect-this-house.com", "7I7noriP");
                request.Timeout = 30000; 
                response = (System.Net.HttpWebResponse)request.GetResponse();
                Stream s = response.GetResponseStream();
                if (File.Exists(filePath))
                {
                    File.Delete(filePath);
                }
                FileStream os = new FileStream(filePath, FileMode.OpenOrCreate, FileAccess.Write);
                byte[] buff = new byte[102400];
                int c = 0;
                while ((c = s.Read(buff, 0, 10400)) > 0)
                {
                    os.Write(buff, 0, c);
                    os.Flush();
                }
                os.Close();
                s.Close();

                //AppLogger.Log("Download file: success");
            }
            catch
            {
                return;
            }
            finally
            {
            }


        }

    }
}
