﻿using Microsoft.Office.Interop.Excel;
using MongoDB.Driver;
using MongoDB.Driver.Builders;
using ProcuctDB.Zebco;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.OleDb;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;

namespace SupplierUpdateSystem.Suppliers.Zebco
{
    public class ZebcoStockUpdate : IUpdate
    {
        ZebcoDB zebcoDB = new ZebcoDB();
        private readonly MongoCollection<ZebcoEntity> col = ZebcoDB.mongoCollection;
        private string fileName = "Zebco.xls";
        private string newZebcoFile = "ZebcoNew.csv";
        private string filePath = @"C:\DevNetworks\Uncategorized\Updater\SupplierUpdateSystem\Zebco\";
        string urlToDownload = "http://webservice.zebco-europe.net/TradeWebUI/LoadFile.aspx?FileName=DataService%5CDE%5CZebco_Europe_Verf%C3%BCgbarkeit.xls";

        public void DoUpdate()
        {      

            Download(urlToDownload, filePath+fileName);
            SetZero();
            Update(GetLines(filePath));
        }

        private void SetZero()
        {
            Parallel.ForEach(col.FindAll().SetFields("product.artnr", "product.ean").ToList(), new ParallelOptions { MaxDegreeOfParallelism = 5}, x =>
            {
                col.Update(Query.EQ("product.artnr", x.product.artnr), MongoDB.Driver.Builders.Update.Set("plus.quantity", 0.0));
            });
        }

        private void Update(List<string> items)
        {
            items.ForEach(item =>
            {
                item = item.Replace("\"", "");
                string artnr = item.Split(',')[0].Trim();
                double quantity = Convert.ToDouble(item.Split(',')[1].Trim());
                IMongoQuery query = Query.EQ("product.artnr", artnr);          

                ZebcoEntity zebco = col.FindOne(query);

                if (zebco != null)
                    zebco.plus["quantity"] = quantity;

                col.Save(zebco);

                UpdateSets(quantity, query);
            });
        }

        private void UpdateSets(double quantity, IMongoQuery query)
        {
            var item = col.FindOne(query);

            if (item != null)
            {
                if (quantity == 0)
                {
                    SetUpdater<ZebcoEntity> setUpdater = new SetUpdater<ZebcoEntity>(item, true);
                    setUpdater.UpdateForOrganizedEntities();
                }

                else if (quantity > 0)
                {
                    SetUpdater<ZebcoEntity> setUpdater = new SetUpdater<ZebcoEntity>(item, false);
                    setUpdater.UpdateForOrganizedEntities();
                }
            }
        }

        private static void Download(string urlToDownload, string filePath)
        {
            using (WebClient Client = new WebClient())
            {
                try
                {
                    Client.DownloadFile(urlToDownload, filePath);
                }

                catch { }
            }
        }

        private List<string> GetLines(string path)
        {
            string connstring = "Provider=Microsoft.ACE.OLEDB.12.0;Data Source=" + path +fileName + ";Extended Properties='Excel 8.0;HDR=NO;IMEX=1';";
            List<string> itemsFromCsv = new List<string>();

            ConvertXlsToCsv();

            itemsFromCsv = File.ReadAllLines(filePath + newZebcoFile).Skip(1).ToList();

            #region
            //using (OleDbConnection conn = new OleDbConnection(connstring))
            //{
            //    conn.Open();
            //    DataTable sheetsName = conn.GetOleDbSchemaTable(OleDbSchemaGuid.Tables, new object[] { null, null, null, "Table" });
            //    string firstSheetName = sheetsName.Rows[0][2].ToString();

            //    //Query String 
            //    string sql = string.Format("SELECT * FROM [{0}]", firstSheetName);
            //    OleDbDataAdapter ada = new OleDbDataAdapter(sql, connstring);
            //    DataSet set = new DataSet();
            //    ada.Fill(set);

            //    DataTable table = set.Tables[0];
            //    foreach (DataRow row in table.Rows)
            //    {
            //        string _item = "";
            //        foreach (var item in row.ItemArray) 
            //        {
            //            _item += item.ToString() +"\t";
            //        }

            //        itemsFromXls.Add(_item);
            //    }
            //}
            #endregion

            return itemsFromCsv;
        }

        private void ConvertXlsToCsv()
        {
            Microsoft.Office.Interop.Excel._Application app = new Microsoft.Office.Interop.Excel.Application();
            Microsoft.Office.Interop.Excel.Workbook wbWorkbook = app.Workbooks.Open(filePath + fileName);
            app.DisplayAlerts = false;
            wbWorkbook.SaveAs(filePath + fileName, Microsoft.Office.Interop.Excel.XlFileFormat.xlCSVWindows, Type.Missing, Type.Missing, true, false, XlSaveAsAccessMode.xlNoChange, XlSaveConflictResolution.xlLocalSessionChanges, Type.Missing, Type.Missing);
            //wbWorkbook.SaveAs(filePath  + newZebcoFile, Microsoft.Office.Interop.Excel.XlFileFormat.xlCSVWindows, Type.Missing, Type.Missing, Type.Missing, Type.Missing, Microsoft.Office.Interop.Excel.XlSaveAsAccessMode.xlShared, Type.Missing, Type.Missing, Type.Missing, Type.Missing, Type.Missing);
            wbWorkbook.SaveAs(filePath + newZebcoFile, Microsoft.Office.Interop.Excel.XlFileFormat.xlCSVWindows, Type.Missing, Type.Missing, true, false, XlSaveAsAccessMode.xlNoChange, XlSaveConflictResolution.xlLocalSessionChanges, Type.Missing, Type.Missing);
            wbWorkbook.Close(false, "", true);
        }
    }




    public class CookieAwareWebClient : WebClient
    {
        public CookieContainer CookieContainer { get; set; }
        public Uri Uri { get; set; }

        public CookieAwareWebClient()
            : this(new CookieContainer())
        {
        }

        public CookieAwareWebClient(CookieContainer cookies)
        {
            this.CookieContainer = cookies;
        }

        protected override WebRequest GetWebRequest(Uri address)
        {
            WebRequest request = base.GetWebRequest(address);
            if (request is HttpWebRequest)
            {
                (request as HttpWebRequest).CookieContainer = this.CookieContainer;
            }
            HttpWebRequest httpRequest = (HttpWebRequest)request;
            httpRequest.AutomaticDecompression = DecompressionMethods.GZip | DecompressionMethods.Deflate;
            return httpRequest;
        }

        protected override WebResponse GetWebResponse(WebRequest request)
        {
            WebResponse response = base.GetWebResponse(request);
            String setCookieHeader = response.Headers[HttpResponseHeader.SetCookie];

            if (setCookieHeader != null)
            {
                //do something if needed to parse out the cookie.
                if (setCookieHeader != null)
                {
                    Cookie cookie = new Cookie(); //create cookie
                    this.CookieContainer.Add(cookie);
                }
            }
            return response;
        }
    }
}
