﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SupplierUpdateSystem.Suppliers.Zebco
{
    public class Zebco : ISupplier
    {
        public void Start()
        {
            IUpdate update = null;
            update = new ZebcoStockUpdate();
            update.DoUpdate();
        }
    }
}
