﻿using MongoDB.Driver;
using OpenQA.Selenium;
using OpenQA.Selenium.Firefox;
using OpenQA.Selenium.Remote;
using OpenQA.Selenium.Support.UI;
using System.Threading;
using System.IO;
using System.Text.RegularExpressions;
using OpenQA.Selenium.PhantomJS;
using ProcuctDB.Suppliers.Jobo;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Net;
using MongoDB.Driver.Builders;
using System.Globalization;
using MongoDB.Bson;
using DN_Classes.Logger;

namespace SupplierUpdateSystem.Suppliers.Jobo
{
    public class JoboStockUpdate: IUpdate
    {
        private static MongoDatabase db = Databases.GetDB("PD_Jobo");
        private static MongoCollection<JoboEntity> col = db.GetCollection<JoboEntity>("jobo");

        public void DoUpdate()
        {

            Console.WriteLine("Downloading CSV File");
            DownloadFile();
            //Make zero all quantity before update
            AllQuantityToZero();
            UpdateFields();
            UpdateNewStocks();
        }

        private void AllQuantityToZero()
        {
            long count = col.Count();
            Parallel.ForEach(col.FindAll().SetFields("product.bestand", "product.ean").ToList(), new ParallelOptions { MaxDegreeOfParallelism = 5 }, x =>
                {
                    col.Update(Query.EQ("product.ean", x.product.ean), Update.Set("product.bestand", 0));
                });

            //AppLogger.Log("Set all zero: success");
        }

        static FileWebResponse myFileWebResponse;

        private static void showUsage()
        {
            Console.WriteLine("\nPlease enter file name:");
            Console.WriteLine("Usage: cs_getresponse <systemname>/<sharedfoldername>/<filename>");
        }      

        public void DownloadFile()
        {
            //IWebDriver driver;
            PhantomJSDriver driver;

            var driverService = PhantomJSDriverService.CreateDefaultService();
            driverService.HideCommandPromptWindow = true;
            using (driver = new PhantomJSDriver(driverService) /*FirefoxDriver()*/)
            {
                driver = Login(driver);
                var client = new WebClient();
                client.Headers[HttpRequestHeader.Cookie] = cookieString(driver);
                client.DownloadFile("http://www.schmuckhandel.de/infocenter_download.php?f1=artikel.csv", @"C:\DevNetworks\Uncategorized\Updater\SupplierUpdateSystem\Jobo\artikel.txt");
                client.DownloadFile("http://www.schmuckhandel.de/infocenter_download.php?f1=bestand.csv", @"C:\DevNetworks\Uncategorized\Updater\SupplierUpdateSystem\Jobo\bestand.txt");
                //AppLogger.Log("Download file: success");           
            }

            driver.Quit();
        }

        void UpdateFields()
        {
            File.ReadAllLines(@"C:\DevNetworks\Uncategorized\Updater\SupplierUpdateSystem\Jobo\artikel.txt", Encoding.Default).ToList().Select(x => x.Replace("\"", "")).Skip(1).ToList()
                .ForEach(line =>
                {
                    //JoboEntity item = EntityBuilder(line);
                    JoboEntity item = col.FindOne(Query.EQ("product.artnr", line.Split(';')[0]));

                    if (item != null)
                        col.Save(EntityBuilder(line, item, ""));

                    else
                        col.Save(EntityBuilder(line, item, "new"));
                });
        
        }

        JoboEntity EntityBuilder(string line, JoboEntity entity, string newOld)
        {
            #region splits
            string[] splitted = line.Split(';');

            string artnr = splitted[0];
            string artindgr = splitted[1];
            string ean = splitted[2];
            string artkern = splitted[3];
            string arttext = splitted[4];
            double artvk = Convert.ToDouble(splitted[5].Replace(',','.'));
            double artuvp = Convert.ToDouble(splitted[6].Replace(',', '.'));
            double artmwst = Convert.ToDouble(splitted[7].Replace(',', '.'));
            string artgra = splitted[8];
            string artgrb = splitted[9];
            string artgrc = splitted[10];
            string kategorie = splitted[11];
            string bildindex1 = splitted[12];
            string bildindex2 = splitted[13];
            string bildindex3 = splitted[14];
            string bildindex4 = splitted[15];
            string bildindex5 = splitted[16];
            string bildlieflogo = splitted[17];
            string marke = splitted[18];
            string artgravt1 = splitted[19];
            string artgravt2 = splitted[20];
            string artgravpr1 = splitted[21];
            string artgravpr2 = splitted[22];
            string groesse = splitted[23];
            string groessepr = splitted[24];
            int bestand = Convert.ToInt32(splitted[25]);
            string sonderbest = splitted[26];
            string lieferzeit = splitted[27];
            string bestandstext = splitted[28];
            string artindg1 = splitted[29];
            string artindg2 = splitted[30];
            string artindg3 = splitted[31];
            string artindg4 = splitted[32];
            string artindz1 = splitted[33];
            string artindz2 = splitted[34];
            string aenderungsdatum = splitted[35];
            #endregion
            if (newOld == "new")
            {
                entity = new JoboEntity();
                entity.id = new ObjectId();
                entity.product = new JoboEntity.Product();
                entity.plus = new BsonDocument();

            }

            entity.product.artnr = artnr;
            entity.product.artindgr = artindgr;
            entity.product.ean = ean;
            entity.product.artkern = artkern;
            entity.product.arttext = arttext;
            entity.product.artvk = artvk;
            entity.product.artuvp = artuvp;
            entity.product.artmwst = artmwst;
            entity.product.artgra = artgra;
            entity.product.artgrb = artgrb;
            entity.product.artgrc = artgrc;
            entity.product.kategorie = kategorie;
            entity.product.bildindex1 = bildindex1;
            entity.product.bildindex2 = bildindex2;
            entity.product.bildindex3 = bildindex3;
            entity.product.bildindex4 = bildindex4;
            entity.product.bildindex5 = bildindex5;
            entity.product.bildlieflogo = bildlieflogo;
            entity.product.marke = marke;
            entity.product.artgravt1 = artgravt1;
            entity.product.artgravt2 = artgravt2;
            entity.product.artgravpr1 = artgravpr1;
            entity.product.artgravpr2 = artgravpr2;
            entity.product.groesse = groesse;
            entity.product.groessepr = groessepr;
            entity.product.bestand = bestand;
            entity.product.sonderbest = sonderbest;
            entity.product.lieferzeit = lieferzeit;
            entity.product.bestandstext = bestandstext;
            entity.product.artindg1 = artindg1;
            entity.product.artindg2 = artindg2;
            entity.product.artindg3 = artindg3;
            entity.product.artindg4 = artindg4;
            entity.product.artindz1 = artindz1;
            entity.product.artindz2 = artindz2;
            entity.product.aenderungsdatum = aenderungsdatum;

            return entity;
        }

        void UpdateNewStocks()
        {
            List<string> lines = File.ReadAllLines(@"C:\DevNetworks\Uncategorized\Updater\SupplierUpdateSystem\Jobo\bestand.txt", Encoding.Default).ToList().Select(x => x.Replace("\"", "")).Skip(1).ToList();
            //List<string> emptyEans = new List<string>();

            int count = 1;
            
            Parallel.ForEach(lines, new ParallelOptions{ MaxDegreeOfParallelism = 7},
                line =>
                {
                    try
                    {
                        string[] c = line.Split(';');
                        string artnr = c[0];
                        string ean = c[1];
                        double vk = Double.Parse(c[2].Replace(",", "."), CultureInfo.InvariantCulture);
                        int bestand = Convert.ToInt32(c[3]);
                        if (bestand < 0)
                            bestand = 0;
                        string sonderbest = c[4];

                        string lieferzeit = c[5];
                        string bestandstext = c[6];
                        IMongoQuery query = Query.EQ("product.artnr", artnr);
                        JoboEntity jobo = col.FindOne(query);
            
                        if (jobo != null)
                        {
                            UpdateSets(bestand, query);

                            UpdateItem(col, artnr, ean, vk, bestand, sonderbest, lieferzeit, bestandstext);
                            //AppLogger.Log("Updated: " + "ean: " +artnr + " stock: " + bestand); 
                        }

                        //else
                        //{
                        //    AddAsNew(col, artnr, ean, vk, bestand, sonderbest, lieferzeit, bestandstext);
                        //    AppLogger.Log("Updated: " +"new " + "ean: " + artnr + " stock: " + bestand); 
                        //}

                        count++;
                    }

                    catch
                    { }
                });
        }

        private static void UpdateSets(int bestand, IMongoQuery query)
        {
            if (bestand == 0)
            {
                SetUpdater<JoboEntity> setUpdater = new SetUpdater<JoboEntity>(col.FindOne(query), true);
                setUpdater.UpdateForAsinOutsideOrInsidePlus();
            }

            else if (bestand > 0)
            {
                SetUpdater<JoboEntity> setUpdater = new SetUpdater<JoboEntity>(col.FindOne(query), false);
                setUpdater.UpdateForAsinOutsideOrInsidePlus();
            }
        }

        private void UpdateItem(MongoCollection<JoboEntity> col, string artnr, string ean, double vk, int bestand, string sonderbest, string lieferzeit, string bestandstext)
        {
            UpdateBuilder update = UpdateBuilder(vk, bestand, sonderbest, lieferzeit, bestandstext);
            col.Update(Query.EQ("product.ean", ean), update);
            Console.WriteLine("Jobo\t" +"ean: " + ean + " or " + artnr + " : " + bestand);

        }

        private static void AddAsNew(MongoCollection<JoboEntity> col, string artnr, string ean, double vk, int bestand, string sonderbest, string lieferzeit, string bestandstext)
        {
            JoboEntity joboEntity = new JoboEntity();
            joboEntity.id = new MongoDB.Bson.ObjectId();
            joboEntity.product = new JoboEntity.Product
            {
                artnr = artnr,
                artvk = vk,
                ean = ean,
                bestand = bestand,
                sonderbest = sonderbest,
                lieferzeit = lieferzeit,
                bestandstext = bestandstext,
            };
            joboEntity.plus = new BsonDocument();

            col.Insert(joboEntity);
            Console.WriteLine("Jobo\t" +"ean: " +ean  +" or " + artnr + "\tadded as new");

        }

        private static UpdateBuilder UpdateBuilder(double vk, int bestand, string sonderbest, string lieferzeit, string bestandstext)
        {
            UpdateBuilder update = Update
                                .Set("product.artvk", vk)
                                .Set("product.bestand", bestand)
                                .Set("product.sonderbest", sonderbest)
                                .Set("product.lieferzeit", lieferzeit)
                                .Set("product.bestandstext", bestandstext)
                                .Set("plus.quantity", bestand);
            return update;
        }

        string cookieString(IWebDriver driver)
        {
            var cookies = driver.Manage().Cookies.AllCookies;
            return string.Join("; ", cookies.Select(c => string.Format("{0}={1}", c.Name, c.Value)));
        }

        public PhantomJSDriver Login(PhantomJSDriver driver)
        {
            driver.Navigate().GoToUrl("http://www.schmuckhandel.de/login.php");
            IWebElement acc = driver.FindElement(By.XPath(".//input[@class='loginbox_text']"));
            acc.Clear();
            acc.SendKeys("101725");

            IWebElement pass = driver.FindElement(By.XPath(".//input[@class='loginbox_pass']"));
            pass.Clear();
            pass.SendKeys("10117");

            IWebElement loginBtn = driver.FindElement(By.XPath(".//tbody/tr/td/input[@alt='Anmelden']"));
            loginBtn.Click();

            driver.Navigate().GoToUrl("www.schmuckhandel.de/infocenter_download.php?f1=bestand.csv");

            return driver;
        }
    }
}
