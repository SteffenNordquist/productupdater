﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SupplierUpdateSystem.Suppliers.Styro
{
    class Styro : ISupplier
    {
        public void Start()
        {
            IUpdate update = null;
            update = new StyroStockUpdate();
            update.DoUpdate();
        }
    }
}
