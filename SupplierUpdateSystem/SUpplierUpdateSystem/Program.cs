﻿using SupplierUpdateSystem.Suppliers;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DN_Classes.AppStatus;
using System.IO;
using ProcuctDB.JTL;
using SupplierUpdateSystem.JPC;
using SupplierUpdateSystem.Suppliers.Berk;
using SupplierUpdateSystem.Suppliers.Berk2;
using SupplierUpdateSystem.Suppliers.Moses;
using SupplierUpdateSystem.Suppliers.SoundOfSpirit;
using SupplierUpdateSystem.Suppliers.JTL;
using SupplierUpdateSystem.Suppliers.Jobo;
using SupplierUpdateSystem.Suppliers.Faber2;
using SupplierUpdateSystem.Suppliers.Gallay;
using SupplierUpdateSystem.Suppliers.Tesa;
using SupplierUpdateSystem.Suppliers.Staedtler;
using SupplierUpdateSystem.Suppliers.StaedtlerTemp;
using SupplierUpdateSystem.Suppliers.Herweck;
using SupplierUpdateSystem.Suppliers.Edding;
using SupplierUpdateSystem.Suppliers.Styro;
using SupplierUpdateSystem.Suppliers.Playcom;
using SupplierUpdateSystem.Suppliers.JoboAms;
using SupplierUpdateSystem.Suppliers.Orion;
using SupplierUpdateSystem.Suppliers.Hkm;
using SupplierUpdateSystem.Suppliers.Rwgh;
using SupplierUpdateSystem.Suppliers.Sets;
namespace SupplierUpdateSystem
{
    class Program
    {
        static void Main(string[] args)
        {
            //Test Here
            SupplierAvailabilityUpdater();
            SupplierUpdater();

        }

        private static void SupplierAvailabilityUpdater()
        {
            runUpdater(new SupplierAvailabilityDate.Suppliers(), "SuppliersAvailabilityUpdater");
        }

        private static void SupplierUpdater()
        {
            List<ISupplier> supplierList = new List<ISupplier>();

            supplierList.Add(new BerkUpdater());
            supplierList.Add(new Berk2());
            supplierList.Add(new Jtl());
            supplierList.Add(new Gallay());
            supplierList.Add(new Jpc());
            supplierList.Add(new Herweck()); //ftp    
            supplierList.Add(new JoboAms());
            supplierList.Add(new Jobo());
            supplierList.Add(new Hkm());
            supplierList.Add(new Orion());
            supplierList.Add(new Rwgh());
            supplierList.Add(new SoundOfSpirit());


            supplierList.Add(new Edding());//email
            supplierList.Add(new Styro()); //email           
            supplierList.Add(new Tesa());//email
            supplierList.Add(new Faber2()); //email

            //supplierList.Add(new Playcom()); //mubalik rani siya              

            #region
            //supplierList.Add(new StaedtlerTemp()); //temporarily removed 030416
            // supplierList.Add(new Glorex());     

            //moses removed
            //nlg commented
            #endregion

            foreach (var supplier in supplierList)
            {
                runUpdater(supplier, "SupplierUpdate");
            }
        }

        private static void runUpdater(ISupplier supplier, string updateType)
        {
            DN_Classes.AppStatus.ApplicationStatus s = new DN_Classes.AppStatus.ApplicationStatus(updateType + supplier.GetType().Name +"_148");
            s.AddMessagLine("");
            s.Start();
            try
            {
               // s.Start();
                //  try
                //    {
                Console.WriteLine(updateType +":" + supplier.GetType().Name);
                supplier.Start();
                s.Successful();
                s.AddMessagLine("Suppliers having field(plus.availabilityDate) are updated");
                s.Stop();
                s.Dispose();

                #region
                //   }
                //catch (Exception ex)
                //{
                //    if (ex.Message.Contains("Could not find a part of the path"))
                //    {
                //        s.Successful();
                //        s.Stop();
                //        s.Dispose();
                //    }

                //    else
                //        s.AddMessagLine(ex.InnerException + "\n" + ex.Message);
                //        s.Stop();
                //        s.Dispose();
                //}
                #endregion
            }

            catch (Exception ex)
            {
                s.Stop();
                s.AddMessagLine(ex.Message + "\n" + ex.StackTrace);
                s.Dispose();
            }
        
        }
    }
}
