﻿using Limilabs.Client.IMAP;
using Limilabs.Mail;
using Limilabs.Mail.MIME;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SupplierUpdateSystem.Suppliers.Staedtler.Update.KIPUpdate
{
    public class FileDownload
    {
        private EmailConnect connect = null;
        private Imap imap = null;
        private List<long> uids = null;

        private string supplierZipPath = string.Empty;
        private string supplierLogPath = string.Empty;
        private string emailKIPFile = string.Empty;
        private string emailKIPFolder = string.Empty;

        public FileDownload()
        {
            connect = new EmailConnect();
            connect.Connect();

            imap = connect.imap;
            uids = connect.uids;

            supplierZipPath = Properties.Settings.Default.staedtlerZipFile;
            supplierLogPath = Properties.Settings.Default.staedtlerLogFile;
            emailKIPFile = Properties.Settings.Default.emailKIPFile;
            emailKIPFolder = "fc_kip\\";
        }

        public void Download()
        {
            foreach (long uid in uids)
            {
                
                try
                {
                    IMail email = new MailBuilder().CreateFromEml(imap.GetMessageByUID(uid));

                    MimeData mime = email.Attachments.First();
                    string subject = email.Subject.ToLower();
                    string fileName = mime.SafeFileName;

                    if (mime.FileName.Contains("sm_kip") && subject.ToLower().Contains("kip_staedtler"))
                    {
                        using (StreamReader sr = new StreamReader(supplierLogPath + emailKIPFile))
                        {
                            //Filename not exists in log(list of zips already used to update)
                            if (!sr.ReadToEnd().Contains(fileName))
                            {
                                mime.Save(supplierZipPath + emailKIPFolder + mime.SafeFileName);

                                using (StreamWriter sw = new StreamWriter(supplierLogPath + emailKIPFile, true))
                                {
                                    sw.WriteLine(fileName);
                                    sw.Dispose();
                                }
                            }
                            sr.Dispose();
                        }
                    }
                }

                catch
                { }
            }
        }
    }
}
