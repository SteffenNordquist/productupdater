﻿using MongoDB.Driver;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SupplierUpdateSystem
{
    public static class Database
    {
        public static MongoDatabase GetDB(string database) {
            return new MongoClient(String.Format("mongodb://client148:client148devnetworks@148.251.0.235/{0}", database)).GetServer().GetDatabase(database);
        }
    }
}
