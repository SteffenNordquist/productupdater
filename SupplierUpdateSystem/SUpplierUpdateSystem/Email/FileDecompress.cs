﻿using System;
using System.Collections.Generic;
using System.IO;
using System.IO.Compression;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SupplierUpdateSystem.Suppliers.Staedtler.Update.KIPUpdate
{
    public class FileDecompress
    {
        private string staedtlerZipPath = string.Empty;
        private string staedtlerLogPath = string.Empty;
        private string emailKIPFile  = string.Empty;
        private string emailKIPFolder = string.Empty;

        public FileDecompress()
        { 
            staedtlerZipPath = Properties.Settings.Default.staedtlerZipFile;
            emailKIPFolder = "fc_kip\\";
        }

        public void Decompress()
        {
            string _zipFile = Directory.GetFiles(staedtlerZipPath + emailKIPFolder)[0];
            string unzipPath = _zipFile;
            ZipFile.ExtractToDirectory(_zipFile, unzipPath);
        }
    }
}
