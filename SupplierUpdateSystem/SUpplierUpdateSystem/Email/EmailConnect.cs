﻿using Limilabs.Client.IMAP;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SupplierUpdateSystem
{
    public class EmailConnect
    {
        private string usrname = string.Empty;
        private string pswrd = string.Empty;
        public Imap imap = null;
        public List<long> uids = null;

        public EmailConnect()
        {
            usrname = Properties.Settings.Default.usr;
            pswrd = Properties.Settings.Default.psswrd;
            imap = new Imap();
        }

        public void Connect()
        {
            using (imap)
            {
                imap.ConnectSSL("imap.gmail.com"/*, 993*/); //No port and using only the default   // or ConnectSSL for SSL
                imap.Login(usrname, pswrd);
                imap.SelectInbox();
                uids = imap.Search(Limilabs.Client.IMAP.Flag.Unseen); ;

                //Making sure it has inbox stored.
                if (uids.Count == 0)
                {
                    uids = imap.Search(Limilabs.Client.IMAP.Flag.Seen);
                }

                //imap.Close();
            }
        }
    }
}
