﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SupplierUpdateSystem.Email
{
    public class FileDelete
    {
        private List<string> fileToDelete = null;

        public FileDelete(List<string> fileToDelete)
        {
            this.fileToDelete = fileToDelete;
        }

        public void Delete()
        {
            var filePaths = fileToDelete.Select(s => Directory.GetFiles(s, "*")[0]).ToList();
            filePaths.ForEach(x => File.Delete(x)); 
        }
    }
}
