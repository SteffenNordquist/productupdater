﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SupplierUpdateSystem.Email
{
    public class FileRead
    {
        private string staedtlerZipPath = string.Empty;
        private string emailKIPFile = string.Empty;
        private string emailKIPFolder = string.Empty;
        private List<string> filesToDecompress = null;

        public FileRead()
        {
            staedtlerZipPath = Properties.Settings.Default.staedtlerZipFile;
            emailKIPFolder = "fc_kip\\";

            filesToDecompress = new List<string>()
            {
                staedtlerZipPath + emailKIPFolder
            };
        }

        public List<string> Read()
        {
            List<string> filesToUpdate = new List<string>();

            foreach (string file in filesToDecompress)
            {
                filesToUpdate.Add(Directory.GetFiles(file).First().ToString());
            }

            return filesToUpdate;
        }
    }
}
