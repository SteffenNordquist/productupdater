﻿using MongoDB.Driver;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SupplierUpdateSystem
{
    public class ProductSizeDB
    {
        private static MongoCollection<ProductSize> Collection;
        public static MongoCollection<ProductSize> ProductSizeCollection()
        {
            if (Collection == null)
            {
                Collection = new MongoClient(@"mongodb://client148:client148devnetworks@136.243.44.111/ProductSize")
                    .GetServer()
                    .GetDatabase("ProductSize")
                    .GetCollection<ProductSize>("productsizes");
            }

            return Collection;
        }
    }
}
