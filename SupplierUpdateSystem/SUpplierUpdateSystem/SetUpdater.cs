﻿using MongoDB.Bson;
using SupplierUpdateSystem.Suppliers.Sets;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SupplierUpdateSystem
{
    public class SetUpdater<T>
    {
        private dynamic entity = null;
        private bool zero = false;

        public SetUpdater(T entity, bool zero)
        {
            this.entity = entity;
            this.zero = zero;
        }

        public void UpdateForOrganizedEntities()
        {
            try
            {
                foreach (var asin in entity.plus["asins"])
                {
                    SetsStockUpdate setStockUpdate = new SetsStockUpdate(entity.ean, asin.ToString(), zero);
                    setStockUpdate.DoUpdate();
                }
            }

            catch
            { 
            }
        }

        public void UpdateForAsinOutsideOrInsidePlus()
        {
            BsonArray asins = new BsonArray();
            if (entity.asins != null)
            {
                foreach(var asin in entity.asins)
                {asins.Add(asin);}
            }

            else
            {
                asins = entity.plus["asins"].AsBsonArray;
            }

            foreach (var asin in asins)
            {
                SetsStockUpdate setsStockUpdate = new SetsStockUpdate(entity.ean, asin.ToString(), zero);
                setsStockUpdate.DoUpdate();
            }
            
        }
    }
}
