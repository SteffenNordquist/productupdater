﻿using ProcuctDB;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SupplierUpdateSystem.SupplierAvailabilityDate
{
    class Suppliers : ISupplier
    {
        public void Start()
        {
            new AvailabilityUpdater().DoUpdate();
        }
    }

    class AvailabilityUpdater:IUpdateAvailability
    {
        public void DoUpdate()
        {
            #region
            //List<string> withAvailabilityField = new List<string>()
            //{
            //    "BAER",
            //    "BIECO",
            //    "MOLESKINE",
            //    "SCHMIDT",
            //    "STAEDTLER"
            //};
            #endregion

            ProductFinder productFinder = new ProductFinder();
            productFinder.getAllDatabases()
                //.Where(x => withAvailabilityField.Contains(x.GetType().Name.ToUpper().Replace("DB", "")))
                .Select(supplier => supplier)
                .ToList<IDatabase>()
                .ForEach(x =>
                {
                    x.UpdateIsAvailable();
                    Console.WriteLine(x.GetType().Name);
                });
        }

        #region
        //private static void AppStatus(IDatabase x)
        //{
        //    DN_Classes.AppStatus.ApplicationStatus s = new DN_Classes.AppStatus.ApplicationStatus("AvailabilityDateUpdater" + x.GetType().Name.Replace("DB", "") + "_148");
        //    s.AddMessagLine("");
        //    s.Start();
        //    try
        //    {
        //        Console.WriteLine("AvailabilityDateUpdater" + x.GetType().Name.Replace("DB", "") + "_148");

        //        x.UpdateIsAvailable();

        //        s.Successful();
        //        s.Stop();
        //        s.Dispose();

        //    }

        //    catch (Exception ex)
        //    {
        //        s.Stop();
        //        s.AddMessagLine(ex.Message + "\n" + ex.StackTrace);
        //        s.Dispose();
        //    }
        //}
        #endregion
    }
}
