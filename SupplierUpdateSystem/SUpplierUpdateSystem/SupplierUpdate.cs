﻿using SupplierUpdateSystem.Email;
using SupplierUpdateSystem.Suppliers.Staedtler.Update.KIPUpdate;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SupplierUpdateSystem
{
    public class SupplierUpdate: IUpdate
    {
        private FileDelete fileDelete = null;
        private FileDownload fileDownload = null;
        private EmailConnect emailConnect = null;
        private FileDecompress fileDecompress = null;
        private FileRead fileRead = null;

        public SupplierUpdate()
        { 
        }

        public void DoUpdate()
        { 
            
        }

        public void FileDelete()
        {
            //fileDelete = new FileDelete();
        }

        public void Connect()
        {
            emailConnect = new EmailConnect();
        }

        public void FileDownLoad()
        {
            fileDownload = new FileDownload();
        }

        public void FileDecompress()
        {
            fileDecompress = new FileDecompress();
        }
    }
}
