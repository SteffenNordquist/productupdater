### What is this repository for? ###
For updating every item on databases, specially for quantity field.

### Summary ###
There are lots of ways we update every item on databases; from email, ftp sites or from supplier websites. These are the sources in the form of files(csv,txt,08,02), web request for everyday updating.

### UML ###
Go to https://app.box.com/s/hrap0vo3t5qdpmez37yl0ioiwou4rujx (this diagram is too big won't fit for screenshot).

### How to setup ###
Go to https://bitbucket.org/SteffenNordquist/supplierupdatesystem for new updates.
Extract the folder debug on C:\DevNetworks\Uncategorized\Updater\SupplierUpdateSystem\.

### Dependencies/Configuration ###
Product.dll, mongoddb and DN_Classes.dll


### Deployment Instructions ###
Deploy it on server 144 under C:\DevNetworks\Uncategorized\Updater\SupplierUpdateSystem\

### Who to contact ###
Aljohn or Sir Jay