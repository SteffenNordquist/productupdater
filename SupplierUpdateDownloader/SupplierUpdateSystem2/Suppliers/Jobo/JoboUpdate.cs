﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DN_Classes.Supplier;
using MongoDB.Driver.Builders;
using ProcuctDB.Suppliers.Jobo;
using System.IO;

namespace SupplierUpdateSystem2.Suppliers.Jobo
{
    public class JoboUpdate:Updater
    {
        private string fileLocation = "";
        public List<string> UpdatedList = new List<string>();
        private JoboDbChild db = new JoboDbChild();

        public JoboUpdate()
        {
            var supplierDb = new SupplierDB();
            var contents = "";
            fileLocation = supplierDb.GetSupplier("JOBO").getSupplierUpdateInfo().credentials.path;
            Console.WriteLine(fileLocation);
        }

        public override void SetZero()
        {
            foreach (var entity in db.GetAllProduct())
            {
                if (!isInUpdate(entity.GetEanList()[0], UpdatedList))
                {
                   db.SetZero(entity.GetEanList()[0]);
                }
            }
        }

        public override void DoUpdate()
        {
            List<string> lines = File.ReadAllLines(fileLocation, Encoding.Default).ToList().Select(x => x.Replace("\"", "")).Skip(1).ToList();

            int count = 1;

            Parallel.ForEach(lines, new ParallelOptions { MaxDegreeOfParallelism = 7 },
                line =>
                {
                    try
                    {
                        string[] c = line.Split(';');
                        string artnr = c[0];
                        string ean = c[1];
                        double vk = Double.Parse(c[2].Replace(",", "."), CultureInfo.InvariantCulture);
                        int bestand = Convert.ToInt32(c[3]);
                        if (bestand < 0)
                            bestand = 0;
                        string sonderbest = c[4];

                        string lieferzeit = c[5];
                        string bestandstext = c[6];
                        db.Update(ean,artnr, vk, bestand, sonderbest, lieferzeit , bestandstext);
                        count++;
                        UpdatedList.Add(ean);
                    }

                    catch
                    { }
                });
        }

        public override string FolderLocation()
        {
            return this.fileLocation;
        }
    }
}
