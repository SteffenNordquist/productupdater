﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Win32;
using ProcuctDB.Suppliers.Jobo;
using MongoDB.Driver.Builders;
using ProcuctDB.Suppliers.Teppich2;

namespace SupplierUpdateSystem2.Suppliers.Jobo
{
    public class JoboDbChild : JoboDB
    {
        public void SetZero(string ean)
        {
            JoboEntity temp = mongoCollection.FindOne(Query.EQ("product.ean", ean));
            if (temp != null)
            {
                temp.plus["quantity"] = 0;
                mongoCollection.Save(temp);
            }
        }

        public void Update(string ean, string artnr,
                            double artvk, int bestand,
                            string sonderbest, string lieferzeit,
                            string bestandstext){
           
            JoboEntity temp = mongoCollection.FindOne(Query.EQ("product.ean", ean));
            if (temp != null)
            {
                temp.product.artnr = artnr;
                temp.product.artvk = artvk;
                temp.product.bestand = bestand;
                temp.product.sonderbest = sonderbest;
                temp.product.lieferzeit = lieferzeit;
                temp.product.bestandstext = bestandstext;
                temp.plus["quantity"] = bestand;
                mongoCollection.Save(temp);
            }

        }
    }
}
