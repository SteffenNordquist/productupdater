﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MongoDB.Driver;
using ProcuctDB.Rwgh;
using MongoDB.Driver.Builders;

namespace SupplierUpdateSystem2.Suppliers.RWGH
{
    public class RwghDBChild : RwghDB
    {

        public void UpdateDB(RwghEntity entity)
        {
            RwghEntity temp = mongoCollection.FindOne(Query.EQ("product.ean", entity.product.ean));
            if (temp != null)
            {

                temp.product.title = entity.product.title;
                temp.product.shortdesc = entity.product.shortdesc;
                temp.product.productdescription = entity.product.productdescription;
                temp.product.size = entity.product.size;
                temp.product.color = entity.product.color;
                temp.product.variationname = entity.product.variationname;
                temp.product.parent_intern_nr = entity.product.parent_intern_nr;
                temp.product.relationshiptype = entity.product.relationshiptype;
                temp.product.variationtheme = entity.product.variationtheme;
                temp.product.shippingweight = entity.product.shippingweight;
                temp.product.buyprice = entity.product.buyprice;
                temp.product.currency = entity.product.currency;
                temp.product.products_status = entity.product.products_status;
                temp.product.categorie = entity.product.categorie;
                temp.product.manufacturer = entity.product.manufacturer;
                temp.product.recommended_retail_price = entity.product.recommended_retail_price;
                temp.product.attrib_agio = entity.product.attrib_agio;
                temp.product.rw_rabatt = entity.product.rw_rabatt;
                temp.product.newitemprice = entity.product.newitemprice;
                temp.product.active_in_shop = entity.product.active_in_shop;
                if (temp.plus["imagename"] != null)
                    temp.plus["imagename"] = entity.plus["imagename"];
                if (temp.plus["mainimageurl"] != null)
                    temp.plus["mainimageurl"] = entity.plus["mainimageurl"];
                int quantity = 0;
                quantity = (Convert.ToInt32(entity.product.active_in_shop) > 0) ? 3 : 0;
                temp.plus["quantity"] = quantity;
                mongoCollection.Save(temp);
            }
            else
            {
                mongoCollection.Save(entity);
            }
        }
    }
}
