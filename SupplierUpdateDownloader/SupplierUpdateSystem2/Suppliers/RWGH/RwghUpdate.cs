﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Policy;
using System.Text;
using System.Threading.Tasks;
using DN_Classes.Supplier;
using MongoDB.Driver;
using MongoDB.Driver.Builders;
using ProcuctDB.Rwgh;

namespace SupplierUpdateSystem2.Suppliers.RWGH
{
    public class RwghUpdate : Updater
    {
        private string fileLocation = "";

        public RwghUpdate()
        {
            SupplierDB supplierDb = new SupplierDB();
            string contents = "";
            fileLocation = supplierDb.GetSupplier("RWGH").getSupplierUpdateInfo().credentials.path;
            Console.WriteLine(fileLocation);
        }

        public override void SetZero()
        {
            RwghDB.mongoCollection.Update(Query.Exists("plus.quantity"), Update.Set("plus.quantity", 0), UpdateFlags.Multi);
        }

        public override void DoUpdate()
        {
            var col = RwghDB.mongoCollection;
            List<string> sources = System.IO.File.ReadAllLines(fileLocation, Encoding.Default).Skip(1).ToList();
            RwghDBChild rwghdb=new RwghDBChild();
            foreach (string s in sources)
            {
                try
                {
                    rwghdb.UpdateDB(new RwghEntity(s));
                    Console.WriteLine("Rwgh\t" + s.Split(';')[1] + "\tupdated");
                }
                catch
                {
                }
            }
        }

        public override string FolderLocation()
        {
            return this.fileLocation;
        }

        public override void PerformUpdate()
        {
            SetZero();
            DoUpdate();
            DeleteFiles();
        }
    }
}
