﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DN_Classes.Supplier;
using MongoDB.Driver;
using MongoDB.Driver.Builders;
using ProcuctDB.Suppliers.Orion;
using System.IO;

namespace SupplierUpdateSystem2.Suppliers.Orion
{
    public class OrionUpdate : Updater
    {
        private string fileLocation = "";

        public OrionUpdate()
        {
            SupplierDB supplierDb = new SupplierDB();
            string contents = "";
            fileLocation = supplierDb.GetSupplier("ORION").getSupplierUpdateInfo().credentials.path;
            Console.WriteLine(fileLocation);
        }

        public override void SetZero()
        {
            OrionDB.mongoCollection.Update(Query.Exists("plus.quantity"), Update.Set("plus.quantity", 0), UpdateFlags.Multi);
        }

        public override void DoUpdate()
        {
            OrionDBChild oriondb = new OrionDBChild();
            var lines = File.ReadAllLines(fileLocation, Encoding.Default).Skip(1).ToList();
            foreach (var line in lines)
            {
                oriondb.UpdateDB(new OrionEntity(line));
                Console.WriteLine("Orion:" + line.Split(';')[23].Replace("\"", ""));
            }
        }

        public override void PerformUpdate()
        {
            SetZero();
            DoUpdate();
            DeleteFiles();
        }

        public override string FolderLocation()
        {
            return this.fileLocation;
        }
    }
}
