﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ProcuctDB.Suppliers.Orion;
using MongoDB.Driver.Builders;

namespace SupplierUpdateSystem2.Suppliers.Orion
{
    public class OrionDBChild : OrionDB
    {
        public void UpdateDB(OrionEntity entity)
        {
            OrionEntity temp = mongoCollection.FindOne(Query.EQ("product.ean", entity.product.ean));
            if (temp != null)
            {
                temp.product.export_version = entity.product.export_version;
                temp.product.current_version = entity.product.current_version;
                temp.product.timestamp = entity.product.timestamp;
                temp.product.product_id = entity.product.product_id;
                temp.product.product_class = entity.product.product_class;
                temp.product.product_name_DE = entity.product.product_name_DE;
                temp.product.title_DE = entity.product.title_DE;
                temp.product.product_name_EN = entity.product.product_name_EN;
                temp.product.title_EN = entity.product.title_EN;
                temp.product.title_IT = entity.product.title_IT;
                temp.product.product_keyword = entity.product.product_keyword;
                temp.product.special_price_flag = entity.product.special_price_flag;
                temp.product.buyprice = entity.product.buyprice;
                temp.product.sellprice = entity.product.sellprice;
                temp.product.full_text_DE = entity.product.full_text_DE;
                temp.product.full_text_EN = entity.product.full_text_EN;
                temp.product.full_text_IT = entity.product.full_text_IT;
                temp.product.label_name = entity.product.label_name;
                temp.product.category_path = entity.product.category_path;
                temp.product.category = entity.product.category;
                temp.product.group_product = entity.product.group_product;
                temp.product.single_image = entity.product.single_image;
                temp.product.tax_id = entity.product.tax_id;
                temp.product.ean_code = entity.product.ean_code;

                temp.product.battery_supply = entity.product.battery_supply;
                temp.product.battery_combination = entity.product.battery_combination;
                temp.product.product_image_100 = entity.product.product_image_100;
                temp.product.product_image_250 = entity.product.product_image_250;
                temp.product.product_image_500 = entity.product.product_image_500;
                temp.product.product_image_1100 = entity.product.product_image_1100;
                temp.product.material_description = entity.product.material_description;
                temp.product.material_thickness = entity.product.material_thickness;
                temp.product.total_length = entity.product.total_length;
                temp.product.diameter = entity.product.diameter;
                temp.product.weight = entity.product.weight;
                temp.product.length = entity.product.length;
                temp.product.width = entity.product.width;
                temp.product.height = entity.product.height;
                temp.product.contents = entity.product.contents;
                temp.product.availability = entity.product.availability;
                temp.product.delivery_week = entity.product.delivery_week;
                temp.product.dress_size = entity.product.dress_size;
                temp.product.shoe_size = entity.product.shoe_size;
                temp.product.main_color = entity.product.main_color;
                temp.product.color_description = entity.product.color_description;
                temp.product.battery_type = entity.product.battery_type;
                temp.product.packaging_size = entity.product.packaging_size;
                temp.product.aroma = entity.product.aroma;
                temp.product.isbn = entity.product.isbn;
                temp.product.number_of_pages = entity.product.number_of_pages;
                temp.product.total_duration = entity.product.total_duration;
                temp.product.hardcore_flag = entity.product.hardcore_flag;
                temp.product.age_rating = entity.product.age_rating;
                temp.product.product_languages = entity.product.product_languages;
                temp.product.country_of_origin_code = entity.product.country_of_origin_code;
                temp.product.series_name = entity.product.series_name;
                temp.product.reference_quantity = entity.product.reference_quantity;
                temp.product.reference_price_factor = entity.product.reference_price_factor;
                temp.product.promotional_packing = entity.product.promotional_packing;
                temp.product.product_icons = entity.product.product_icons;
                temp.product.selling_unit = entity.product.selling_unit;
                temp.product.voc_percentage = entity.product.voc_percentage;
                temp.product.barcode_type = entity.product.barcode_type;
                temp.product.novelty_flag = entity.product.novelty_flag;
                temp.product.food_information_flag = entity.product.food_information_flag;
                temp.product.detailed_full_text_de = entity.product.detailed_full_text_de;
                temp.product.group_id = entity.product.group_id;
                string availability = entity.product.availability.ToLower();
                if (availability == "j")
                    temp.plus["quantity"] = 3.0;
                else
                    temp.plus["quantity"] = 0.0;
                mongoCollection.Save(temp);
            }

            else
            {
                mongoCollection.Save(entity);
            }
        }
    }
}
