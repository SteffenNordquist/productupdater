﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MongoDB.Driver;
using MongoDB.Driver.Builders;
using ProcuctDB.Suppliers.Hkm;

namespace SupplierUpdateSystem2.Suppliers.HKM
{
    public class HKMDbChild : HkmDB
    {
        public void UpdateQty(string ean, int qty)
        {
            mongoCollection.Update(Query.EQ("product.ean", ean), Update.Set("plus.quantity", qty), UpdateFlags.Multi);
        }
    }
}
