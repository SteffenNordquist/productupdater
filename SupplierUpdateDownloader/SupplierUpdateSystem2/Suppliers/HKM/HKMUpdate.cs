﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DN_Classes.Supplier;
using ProcuctDB.Suppliers.Hkm;
using System.IO;


namespace SupplierUpdateSystem2.Suppliers.HKM
{
    public class HKMUpdate : Updater
    {
        private string fileLocation = "";
        public List<string> UpdatedList = new List<string>();
        private HKMDbChild db = new HKMDbChild();

        public HKMUpdate()
        {
            var supplierDb = new SupplierDB();
            var contents = "";
            fileLocation = supplierDb.GetSupplier("HKM").getSupplierUpdateInfo().credentials.path;
            Console.WriteLine(fileLocation);
        }

        public override void SetZero()
        {
            foreach (var entity in db.GetAllProduct())
            {
                if (!isInUpdate(entity.GetEanList()[0], UpdatedList))
                {
                   db.UpdateQty( entity.GetEanList()[0],0);
                }
            }
        }

        public override void DoUpdate()
        {
            var lines = File.ReadAllLines(fileLocation, Encoding.Default).ToList();
            foreach (var line in lines)
            {
                string ean = line.Split(';')[4].Trim();
                int stock = Convert.ToInt32(line.Split(';')[2].Trim());
                try
                {
                    db.UpdateQty(ean, stock);
                    UpdatedList.Add(ean);
                }
                catch
                {
                }
            }

        }

        public override string FolderLocation()
        {
            return this.fileLocation;
        }
    }
}
