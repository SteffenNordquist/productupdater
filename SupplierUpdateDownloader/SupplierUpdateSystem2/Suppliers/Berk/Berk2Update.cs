﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DN_Classes.Supplier;
using MongoDB.Driver;
using MongoDB.Driver.Builders;
using ProcuctDB.Berk;
using ProcuctDB.Berk2;

namespace SupplierUpdateSystem2.Suppliers.Berk
{
    class Berk2Update:Updater
    {
        private Berk2DB db = new Berk2DB();
        private List<BerkEntity> UpdatedList = new List<BerkEntity>();
        private readonly string fileLocation = "";

        public Berk2Update()
        {
            var supplierDb = new SupplierDB();
            var contents = "";
            fileLocation = supplierDb.GetSupplier("BERK2").getSupplierUpdateInfo().credentials.path;
            Console.WriteLine(fileLocation);
        }

        public override void SetZero()
        {
            BerkDB.mongoCollection.Update(Query.Exists("plus.stock"), Update.Set("plus.stock", 0), UpdateFlags.Multi);
        }

        public override string FolderLocation()
        {
            return fileLocation;
        }

        public override void DoUpdate()
        {
          //  var berk2DB = new Berk2DB();
            var col = Berk2DB.mongoCollection;
            List<string> sources = System.IO.File.ReadAllLines(fileLocation).ToList();
            foreach (string source in sources)
            {
                string articleNumber = source.Split(';')[0].Replace("\"", "");
                int stock = Convert.ToInt32(source.Split(';')[1].Replace("\"", ""));
                col.Update(Query.EQ("artikelnumber", articleNumber), Update.Set("stock", stock));
                Console.WriteLine("Berk2" + "\tartnr: " + source);
            }
        }

        public override void PerformUpdate()
        {
            SetZero();
            DoUpdate();
            DeleteFiles();
        }
    }
}
