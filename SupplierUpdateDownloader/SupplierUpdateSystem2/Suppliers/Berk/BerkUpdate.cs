﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using DN_Classes.Supplier;
using MongoDB.Driver;
using MongoDB.Driver.Builders;
using ProcuctDB.Berk;

namespace SupplierUpdateSystem2.Suppliers.Berk
{
    internal class BerkUpdate : Updater
    {
        private BerkDB db = new BerkDB();
        private List<BerkEntity> UpdatedList = new List<BerkEntity>();
        private readonly string fileLocation = "";

        public BerkUpdate()
        {
            var supplierDb = new SupplierDB();
            var contents = "";
            fileLocation = supplierDb.GetSupplier("BERK2").getSupplierUpdateInfo().credentials.path;
            Console.WriteLine(fileLocation);
        }

        public override void SetZero()
        {
            BerkDB.mongoCollection.Update(Query.Exists("plus.stock"), Update.Set("plus.stock", 0), UpdateFlags.Multi);
        }

        public override string FolderLocation()
        {
            return fileLocation;
        }

        public override void DoUpdate()
        {
            var berkDB = new BerkDB();
            var col = BerkDB.mongoCollection;

            System.IO.DirectoryInfo di = new DirectoryInfo(fileLocation);
            foreach (FileInfo file in di.GetFiles())
            {
                List<string> sources = System.IO.File.ReadAllLines(file.FullName).ToList();
                foreach (string source in sources)
                {
                    string articleNumber = source.Split(';')[0].Replace("\"", "");
                    int stock = Convert.ToInt32(source.Split(';')[1].Replace("\"", ""));
                    col.Update(Query.EQ("artikelnumber", articleNumber), Update.Set("stock", stock));
                    Console.WriteLine("Berk" + "\tartnr: " + source);
                }
            }
         
        }

        public override void PerformUpdate()
        {
            SetZero();
            DoUpdate();
            DeleteFiles();
        }
    }
}