﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ProcuctDB.Jpc;
using MongoDB.Driver.Builders;

namespace SupplierUpdateSystem2.Suppliers.JPC
{
    class JPCDBChild:JpcDB
    {
        public void Save(JpcEntity entity)
        {
            JpcEntity temp = mongoCollection.FindOne(Query.EQ("ean", entity.ean));
            if (temp != null)
            {
                temp.bestStaffel = entity.bestStaffel;
                temp.nettoEK = entity.nettoEK;
            }
        }

        public void Update(string ean, int qty, double price)
        {
            JpcEntity temp = mongoCollection.FindOne(Query.EQ("ean", ean));
            if (temp != null)
            {
                temp.bestStaffel = qty;
                temp.nettoEK = price;
                mongoCollection.Save(temp);
            }
        }
    }
}
