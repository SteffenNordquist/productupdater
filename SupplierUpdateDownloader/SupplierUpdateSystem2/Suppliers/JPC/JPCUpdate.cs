﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;
using System.IO.Compression;
using DN_Classes.Supplier;
using MongoDB.Driver.Builders;
using ProcuctDB.Jpc;

namespace SupplierUpdateSystem2.Suppliers.JPC
{
    public class JPCUpdate : Updater
    {
        private string fileLocation = "";
        public List<string> UpdatedList = new List<string>();
        private JPCDBChild db = new JPCDBChild();

        public JPCUpdate()
        {
            var supplierDb = new SupplierDB();
            var contents = "";
            fileLocation = supplierDb.GetSupplier("JPC").getSupplierUpdateInfo().credentials.path;
            Console.WriteLine(fileLocation);
        }

        private void Decompress(string fileName)
        {
            System.IO.DirectoryInfo di = new DirectoryInfo(fileLocation);
            foreach (FileInfo file in di.GetFiles())
            {
                ZipFile.ExtractToDirectory(file.FullName, fileLocation);
                File.Delete(file.FullName);
            }

        }

        public override void SetZero()
        {
            foreach (var entity in db.GetAllProduct())
            {
                if (!isInUpdate(entity.GetEanList()[0], UpdatedList))
                {
                    JpcEntity temp = (JpcEntity)entity;
                    temp.bestStaffel = 0;
                    db.Save(temp);
                }
            }
        }

        public override void DoUpdate()
        {
            System.IO.DirectoryInfo di = new DirectoryInfo(fileLocation);
            foreach (FileInfo file in di.GetFiles())
            {
                var lines = File.ReadAllLines(file.FullName).ToList();
                lines.RemoveAt(0);
                foreach (var line in lines)
                {
                    string ean = line.Split('\t')[6].Replace("\"", "");
                    int stock = Convert.ToInt32(line.Split('\t')[7].Trim().Replace("\"", ""));
                    int newStock = 0;
                    double buyPrice = Convert.ToDouble(line.Split('\t')[3].Replace(",", ".").Trim().ToString(), CultureInfo.InvariantCulture);
                    if (stock > 20)
                    {
                        newStock = 21;
                    }
                    else if (stock < 20 && stock >= 11)
                    {
                        newStock = 11;
                    }
                    else if (stock < 11 && stock >= 6)
                    {
                        newStock = 6;
                    }
                    else if (stock < 5 && stock >= 3)
                    {
                        newStock = 3;
                    }
                    else if (stock < 2 && stock >= 1)
                    {
                        newStock = 1;
                    }
                    else { newStock = 0; }
                    db.Update(ean, newStock, buyPrice);
                    UpdatedList.Add(ean);
                }

            }
        }

        public override string FolderLocation()
        {
            return this.fileLocation;
        }
    }
}
