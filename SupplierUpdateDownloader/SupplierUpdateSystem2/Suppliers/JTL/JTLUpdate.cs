﻿using ProcuctDB;
using ProcuctDB.JTL;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DN_Classes.Supplier;
using System.Text.RegularExpressions;
using WhereToBuy;

namespace SupplierUpdateSystem2.Suppliers.JTL
{
    public class JTLUpdate : Updater
    {
        public List<JTLEntity> UpdatedList = new List<JTLEntity>();
        private string fileLocation = "";
        private JTLDB db = new JTLDB();

        CheapestPrice whereToBuy = new CheapestPrice();

        public JTLUpdate()
        {
            SupplierDB supplierDb = new SupplierDB();
            string contents = "";
            fileLocation = supplierDb.GetSupplier("JTL").getSupplierUpdateInfo().credentials.path;
            Console.WriteLine(fileLocation);
        }

        public override void SetZero()
        {
            foreach (JTLEntity entity in db.GetAllProducts())
            {
                if (!isInUpdate(entity.product.ean, UpdatedList.ToList<IProduct>()))
                {
                    string whereToBuyNoExcludedSupplier = "";
                    try
                    {
                        whereToBuyNoExcludedSupplier = whereToBuy.GetCheapestPriceNoExcludedSupplier(entity.product.ean).getLine();
                    }
                    catch
                    {
                    }
                    entity.plus["quantity"] = 0;
                    try
                    {
                        entity.product.buyprice =
                            Convert.ToDouble(whereToBuyNoExcludedSupplier.Split('\t')[2].ToString());
                    }
                    catch
                    {
                    }

                    try
                    {
                        entity.product.supplier = whereToBuyNoExcludedSupplier.Split('\t')[6].ToString();
                    }
                    catch
                    {
                        entity.product.supplier = "";
                    }
                    db.Save(entity);

                }
            }
        }

        public override string FolderLocation()
        {
            return this.fileLocation;
        }

        //private bool isInUpdate(string entityEan)
        //{
        //    bool flag = false;
        //    foreach (var entity in UpdatedList)
        //    {
        //        if (entityEan == entity.GetEan())
        //        {
        //            return true;
        //            break;
        //        }
        //    }
        //    return flag;
        //}

        public override void DoUpdate()
        {
            var lines =
                  System.IO.File.ReadAllLines(fileLocation)
                      .Select(x => x.Replace("\"", ""))
                      .ToList();
            lines.RemoveAt(0);
            int counter = 0;
            Console.WriteLine(lines.Count);
            counter = 0;
            foreach (var line in lines)
            {
                counter++;
                string ean = FormatEan(line.Split(';')[0]);
                BuyPriceRowWithIProduct rowWithQtyIproduct = whereToBuy.GetCheapestPriceByEanForJTL(ean);
                BuyPriceRowWithIProduct rowWithoutQtyIproduct = whereToBuy.GetCheapestPriceNoExcludedSupplier(ean);
                string whereToBuyLine = "";
                try
                {
                    whereToBuyLine = rowWithQtyIproduct.getLine();
                }
                catch
                {
                }
                string whereToBuyNoExcludedSupplier = "";
                try { whereToBuyNoExcludedSupplier = rowWithoutQtyIproduct.getLine(); }
                catch { }
                IProduct iproductwithqty = null;
                IProduct iproductwithoutqty = null;
                try
                {
                    iproductwithqty = rowWithQtyIproduct.product;
                }
                catch
                {
                }
                try
                {
                    iproductwithoutqty = rowWithoutQtyIproduct.product;
                }
                catch { }
                JTLEntity entity;
                Console.WriteLine(counter + "\t" + ean);
                if (rowWithQtyIproduct != null)
                {
                    try
                    {
                        entity = new JTLEntity(ean, line, whereToBuyLine, iproductwithqty);
                     //     db.Save(entity);
                    }
                    catch
                    {
                        entity = new JTLEntity(ean, line, "", iproductwithqty);
                    //    db.Save(entity);
                    }
                }
                else
                {
                    try
                    {
                        entity = new JTLEntity(ean, line, whereToBuyNoExcludedSupplier, iproductwithoutqty);
                    //    db.Save(entity);
                    }
                    catch
                    {
                        entity = new JTLEntity(ean, line, "", iproductwithoutqty);
                   //     db.Save(entity);
                    }
                }
                db.Save(entity);
                UpdatedList.Add(entity);
            }
        }

        private static string FormatEan(string old_ean)
        {
            Regex regex = new Regex(@"\d{11,13}");
            string ean = old_ean;
            if (regex.Match(old_ean).Success)
            {
                ean = regex.Match(old_ean).Value;
                while (ean.Length < 13)
                    ean = "0" + ean;
            }
            return ean;
        }


    }
}
