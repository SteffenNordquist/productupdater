﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DN_Classes.Supplier;
using ProcuctDB.Berk;
using ProcuctDB.Berk2;
using ProcuctDB.Gallay;

namespace SupplierUpdateSystem2.Suppliers.Gallay
{
    public class GallayUpdate : Updater
    {
        private GallayDB db = new GallayDB();
        private List<GallayEntity> UpdatedList = new List<GallayEntity>();
        private readonly string fileLocation = "";

        public GallayUpdate()
        {
            var supplierDb = new SupplierDB();
            var contents = "";
            fileLocation = supplierDb.GetSupplier("GALLAY").getSupplierUpdateInfo().credentials.path;
            Console.WriteLine(fileLocation);
        }

        public override void SetZero()
        {
            
            foreach (var entity in db.GetAllProduct())
            {
                GallayEntity tempEntity = (GallayEntity) entity;
                if (!isUpdated(tempEntity))
                {
                    tempEntity.product.stock = 0;
                    db.Save(tempEntity);
                }
            }
        }

        private bool isUpdated(GallayEntity CheckEntity)
        {
            bool flag = false;
            foreach (var entity in UpdatedList)
            {
                if (CheckEntity.product.ean == entity.product.ean)
                {
                    flag = true;
                    break;
                }
            }
            return flag;
        }

        public override void DoUpdate()
        {
            var berkDB = new BerkDB();
            var col = BerkDB.mongoCollection;

            System.IO.DirectoryInfo di = new DirectoryInfo(fileLocation);
            foreach (FileInfo file in di.GetFiles())
            {
                List<string> sources = System.IO.File.ReadAllLines(file.FullName).ToList();
                foreach (var line in sources)
                {
                    db.Save(new GallayEntity(line));
                    UpdatedList.Add(new GallayEntity(line));
                    Console.WriteLine("Gallay" + "\titemnum: " + line.Split(';')[0]);

                }

            }
        }

        public override string FolderLocation()
        {
            return this.fileLocation;
        }
    }
}
