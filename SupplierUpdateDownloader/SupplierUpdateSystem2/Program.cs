﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ProcuctDB.Berk;
using ProcuctDB.JTL;
using SupplierUpdateSystem2.Suppliers.Berk;
using SupplierUpdateSystem2.Suppliers.Gallay;
using SupplierUpdateSystem2.Suppliers.HKM;
using SupplierUpdateSystem2.Suppliers.Jobo;
using SupplierUpdateSystem2.Suppliers.JPC;
using SupplierUpdateSystem2.Suppliers.JTL;
using SupplierUpdateSystem2.Suppliers.Orion;
using SupplierUpdateSystem2.Suppliers.RWGH;
using WhereToBuy;

namespace SupplierUpdateSystem2
{
    class Program
    {
        static void Main(string[] args)
        {
            List<Updater> supplierList = new List<Updater>
            {
                new BerkUpdate(),   new Berk2Update(),  new GallayUpdate(), new HKMUpdate(),    
                new JoboUpdate(),   new JPCUpdate(),    new JTLUpdate(),    new OrionUpdate(),  
                new RwghUpdate()
                //add new suppliers here
            };


            foreach (var supplier in supplierList)
            {
               Console.WriteLine(supplier.ToString());
                supplier.PerformUpdate();
            }

        }
    }
}
