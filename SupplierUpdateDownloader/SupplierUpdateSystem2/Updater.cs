﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DN_Classes.Logger;
using ProcuctDB;

namespace SupplierUpdateSystem2
{
    public abstract class Updater
    {
         //must override this methods
        public abstract void SetZero();
        public abstract void DoUpdate();
        public abstract string FolderLocation();

        public void DeleteFiles()
        {
            System.IO.DirectoryInfo di = new DirectoryInfo(this.FolderLocation());
            foreach (FileInfo file in di.GetFiles())
            {
                file.Delete();
            }
        }

        public virtual void PerformUpdate()
        {
            this.DoUpdate();
            this.SetZero();
            this.DeleteFiles();
        }

        public bool isInUpdate(string entityEan, List<IProduct> productlist)
        {
            bool flag = false;
            foreach (var entity in productlist)
            {
                if (entityEan == entity.GetEanList()[0])
                {
                    return true;
                    break;
                }
            }
            return flag;
        }

        public bool isInUpdate(string entityEan, List<string> eansList)
        {
            bool flag = false;
            foreach (var ean in eansList)
            {
                if (entityEan == ean)
                {
                    return true;
                    break;
                }
            }
            return flag;
        }
    }
}
