﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SupplierUpdateDownloader
{
    public enum DownloadType
    {
        FTP,
        Email,
        Website,
        DirectDownload

    }
}
