﻿using Limilabs.Client.IMAP;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SupplierUpdateDownloader.Classes.Email
{
    public class EmailConnect
    {
        private static Imap imap = null;
        public static Imap Connect(string username, string password)
        {
            imap = new Imap();
            imap.ConnectSSL("imap.gmail.com"/*, 993*/);
            imap.Login(username, password);
            //imap.SelectInbox();
            imap.Select("SeenByUpdaterTool");

            return imap;
        }

        public static List<long> GetUids()
        {
            List<long> uids = imap.Search(Limilabs.Client.IMAP.Flag.Unseen);
            uids.Reverse();

            if (uids.Count() != 0)
                return uids;

            else
            {
                uids = imap.Search(Limilabs.Client.IMAP.Flag.Seen);
                uids = uids.GetRange(uids.Count()-15, 15);
                uids.Reverse();
                return uids;
            }
        }
    }
}
