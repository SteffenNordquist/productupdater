﻿using Limilabs.Client.IMAP;
using Limilabs.Mail;
using Limilabs.Mail.MIME;
using ProcuctDB;
using SupplierUpdateDownloader.Classes.Entity;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SupplierUpdateDownloader.Classes.Email
{
    public class EmailFile
    {
        private List<long> uids = null;
        private Imap imap = null;
        private IEmailDownload supplierEmailDownloadInfo = null;

        public EmailFile(EmailDownloadEntity emailDownloadEntity)
        {
            this.uids = emailDownloadEntity.uids;
            this.imap = emailDownloadEntity.imap;
            this.supplierEmailDownloadInfo = emailDownloadEntity.supplierEmailDownloadInfo;
        }

        public void SaveFile(string file)
        {
            ForeachUids(file);
        }

        private void ForeachUids(string file)
        {
            foreach (long uid in uids)
            {
                try
                {
                    IMail email = new MailBuilder().CreateFromEml(imap.GetMessageByUID(uid));

                        MimeData mime = email.Attachments.First();
                        string subject = email.Subject.ToLower();
                        string fileName = mime.SafeFileName;
                        string zipDirectory = supplierEmailDownloadInfo.getZipFile();
                        string kipFolder = supplierEmailDownloadInfo.getKipFileName() + "\\";
                        string prodFolder = supplierEmailDownloadInfo.getProdFileName() + "\\";

                        ///kip
                        if (mime.FileName.Contains(supplierEmailDownloadInfo.getKipFileName())
                            /* && subject.ToLower().Contains(supplierEmailDownloadInfo.getKipSubject())*/
                            && file == "kip")
                        {
                            string kipDirectory = zipDirectory + kipFolder;
                            if (!File.Exists(kipDirectory))
                                Directory.CreateDirectory(kipDirectory);

                            mime.Save(zipDirectory + kipFolder + mime.SafeFileName);
                            imap.MoveByUID(uid, "SeenByUpdaterTool");
                            Console.WriteLine(mime.SafeFileName + " downloaded");
                        }

                        ///product
                        else if (mime.FileName.Contains(supplierEmailDownloadInfo.getProdFileName())
                            /*&& subject.ToLower().Contains(supplierEmailDownloadInfo.getProdSubject())*/
                            && file == "prod")
                        {
                            string prodDirectory = zipDirectory + prodFolder;
                            if (!File.Exists(prodDirectory))
                                Directory.CreateDirectory(prodDirectory);

                            mime.Save(zipDirectory + prodFolder + mime.SafeFileName);
                            imap.MoveByUID(uid, "SeenByUpdaterTool");
                            Console.WriteLine(mime.SafeFileName + " downloaded");
                        }
                    }

                catch { }
            }
            
        }
    }
}
