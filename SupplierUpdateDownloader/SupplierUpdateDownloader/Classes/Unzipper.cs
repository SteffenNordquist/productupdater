﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO.Compression;

namespace SupplierUpdateDownloader
{
    public static class Unzipper
    {

        public static void Decompress(string zipFilePath, string unzipPath)
        {
            ZipFile.ExtractToDirectory(zipFilePath, unzipPath);
        }

     
    }
}
