﻿using Limilabs.Client.IMAP;
using Limilabs.Mail;
using Limilabs.Mail.MIME;
using ProcuctDB;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SupplierUpdateDownloader.Classes.Email;

namespace SupplierUpdateDownloader.Classes.Entity
{
    public class EmailDownloadEntity
    {
        public Imap imap {get;set;}
        public List<long> uids { get; set; }
        public IEmailDownload supplierEmailDownloadInfo { get; set; }
    }
}
