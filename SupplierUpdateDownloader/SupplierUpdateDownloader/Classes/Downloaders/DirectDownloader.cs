﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using DN_Classes.Entities.Supplier;
using DN_Classes.Supplier;

namespace SupplierUpdateDownloader
{
    public class DirectDownloader:IDownloader
    {
        public string path { get; set; }
        public string url { get; set; }
        public string name { get; set; }
        public DirectDownloader(string name, suCredentials credentials)
        {
            this.url = credentials.url;
            this.path = credentials.path;
            this.name = name;
        }
        public DirectDownloader(string name, string path, string url)
        {
            this.path = path;
            this.url = url;
            this.name = name;
        }
        /**
         * download single file
         */
        public void Download()
        {

            try
            {
                this.CreateFolder();
            }
            catch
            {
            }
            using (WebClient Client = new WebClient())
            {
                Client.DownloadFile(url, path + this.GetFileName());
            }
            Console.WriteLine(name + ". Download Complete");
        }

        public void CreateFolder()
        {
            Directory.CreateDirectory(path);
        }
        //public string getFileName()
        //{
        //    string filename = ""; 
        //    Uri uri = new Uri(url);
        //    if (uri.IsFile)
        //    {
        //        filename = System.IO.Path.GetFileName(uri.LocalPath);
        //    }
        //    return getFileName();
        //}
        private String GetFileName()
        {
            return Path.GetFileName(Uri.UnescapeDataString(url).Replace("/", "\\"));
        }
        public string GetName()
        {
            return this.name;
        }

    }
}
