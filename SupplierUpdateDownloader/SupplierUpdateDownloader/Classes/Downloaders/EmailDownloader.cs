﻿using DN_Classes.Entities.Supplier;
using Limilabs.Client.IMAP;
using Limilabs.Mail;
using Limilabs.Mail.MIME;
using ProcuctDB;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ProcuctDB.Suppliers.Styro;
using System.Reflection;
using SupplierUpdateDownloader.Classes.Entity;
using SupplierUpdateDownloader.Classes.Email;

namespace SupplierUpdateDownloader.Downloaders
{
    class EmailDownloader:IDownloader
    {
        public string url { get; set; }
        public string username { get; set; }
        public string password { get; set; }
        public string path { get; set; }
        public string name { get; set; }

        public EmailDownloader(string name, suCredentials credentials)
        {
            this.url = credentials.url;
            this.username = credentials.username;
            this.password = credentials.password;
            this.path = credentials.path;
            this.name = name;
        }

        public void Download()
        {
            EmailDownloadEntity emailDownloadEntity = new EmailDownloadEntity()
            {
                imap = EmailConnect.Connect(username, password),
                uids = EmailConnect.GetUids(),
                supplierEmailDownloadInfo = new ProductFinder().getEmailUpdateSupplier(UppercaseFirst(name))
            };

            EmailFile emailFile = new EmailFile(emailDownloadEntity);
            emailFile.SaveFile("kip");
            emailFile.SaveFile("prod");

            emailDownloadEntity.imap.Dispose();         
        }

        public string GetName()
        {
            return this.name;
        }

        private static string UppercaseFirst(string s)
        {
            s = s.ToLower();
            if (string.IsNullOrEmpty(s))
            {
                return string.Empty;
            }
            char[] a = s.ToCharArray();
            a[0] = char.ToUpper(a[0]);
            return new string(a);
        }
    }
}
