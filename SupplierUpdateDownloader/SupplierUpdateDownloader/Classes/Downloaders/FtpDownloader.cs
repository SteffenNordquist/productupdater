﻿using DN_Classes.Entities.Supplier;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace SupplierUpdateDownloader
{
    public class FtpDownloader : IDownloader
    {
        public string url { get; set; }
        public string username { get; set; }
        public string password { get; set; }
        public string path { get; set; }
        public string name { get; set; }
        public FtpDownloader(string name, suCredentials credentials)
        {
            this.url = credentials.url;
            this.username = credentials.username;
            this.password = credentials.password;
            this.path = credentials.path;
            this.name = name;
        }

        public FtpDownloader(string name, string url, string username, string password, string path)
        {
            this.url = url;
            this.username = username;
            this.password = password;
            this.path = path;
            this.name = name;
        }

        private List<string> GetFileNames(string extension = "*")
        {
            List<string> zipFiles = new List<string>();
            FtpWebRequest request = (FtpWebRequest)WebRequest.Create(url);
            request.Method = WebRequestMethods.Ftp.ListDirectory;
            request.Credentials = new NetworkCredential(username, password);
            FtpWebResponse response = (FtpWebResponse)request.GetResponse();
            Stream responseStream = response.GetResponseStream();
            StreamReader reader = new StreamReader(responseStream);
            if (extension.Contains("*"))
                zipFiles.AddRange(reader.ReadToEnd().Split('\n'));
            else
                zipFiles.AddRange(reader.ReadToEnd().Split('\n').Where(x => x.Contains(extension.ToLower())));
            reader.Close();
            response.Close();
            return zipFiles;
        }

        public void Download()
        {
            foreach (var file in this.GetFileNames())
            {
                try
                {
                    if (!file.Trim().Equals(""))
                        this.DownloadFile(file);
                }
                catch { }
            }
        }
        public void CreateFolder()
        {
            Directory.CreateDirectory(path);
        }

        private void DownloadFile(string file)
        {
            try
            {
                this.CreateFolder();
            }
            catch
            {
            }


            FtpWebRequest request = (FtpWebRequest)WebRequest.Create(url + file);
            request.Method = WebRequestMethods.Ftp.DownloadFile;
            request.Credentials = new NetworkCredential(username, password);
            request.KeepAlive = true;
            FtpWebResponse response = (FtpWebResponse)request.GetResponse();
            Stream responseStream = response.GetResponseStream();
            StreamReader reader = new StreamReader(responseStream);
            FileStream localFileStream = new FileStream(path + file.Replace("\r", ""), FileMode.Create);
            byte[] byteBuffer = new byte[2048];
            int bytesRead = responseStream.Read(byteBuffer, 0, 2048);
            try
            {
                while (bytesRead > 0)
                {
                    localFileStream.Write(byteBuffer, 0, bytesRead);
                    bytesRead = responseStream.Read(byteBuffer, 0, 2048);
                }
            }
            catch (Exception ex) { Console.WriteLine(ex.ToString()); }
            Console.WriteLine(name + ". Download Complete, status {0}", response.StatusDescription);
            reader.Close();
            response.Close();
            localFileStream.Close();
            request = null;
        }

        public string GetName()
        {
            return this.name;
        }
    }
}
