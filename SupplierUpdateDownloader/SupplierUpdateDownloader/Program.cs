﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DN_Classes.Entities.Supplier;
using DN_Classes.Supplier;
using SupplierUpdateDownloader.Downloaders;

namespace SupplierUpdateDownloader
{
    class Program
    {
        private static void Main(string[] args)
        {
            List<IDownloader> supplierDownloaders= new List<IDownloader>();
            SupplierDB supplierDb = new SupplierDB();
            foreach (var supplier in supplierDb.Suppliers.Where(x=>x.getSupplierUpdateInfo().value!="None").ToList())
            {
                SupplierUpdate entity = supplier.getSupplierUpdateInfo();
                switch (entity.value)
                {
                    case "Email":
                        supplierDownloaders.Add(new EmailDownloader(supplier._id, entity.credentials));
                        break;
                    case "Direct Download":
                        supplierDownloaders.Add(new DirectDownloader(supplier._id, entity.credentials));
                        break;
                    case "FTP":
                        supplierDownloaders.Add(new FtpDownloader(supplier._id, entity.credentials));
                        break;
                }
                
            }
            foreach (var supplierDownloader in supplierDownloaders)
            {
                try
                {
                    Console.WriteLine(supplierDownloader.GetName());
                    supplierDownloader.Download();
                    Console.WriteLine(supplierDownloader.GetName() + " DONE.");
                }
                catch(Exception ex)
                {
                    Console.WriteLine(ex.InnerException);
                }
            }
        //    Console.Read();
        }
    }
}
