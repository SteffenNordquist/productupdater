﻿using DN_Classes.AppStatus;
using DN_Classes.DBCollection.FalseProducts;
using DN_Classes.Entities.ProductTerminator;
using DN_Classes.Supplier;
using MongoDB.Driver.Builders;
using ProcuctDB;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;

namespace ProductTerminator
{
    class Program
    {
        static void Main(string[] args)
        {
            using (ApplicationStatus appStatus = new ApplicationStatus("ProductTerminator"))
            {
                try
                {
                    GetProductsToTerminate();

                        if (productsToTerminateList.Count() == 0)
                        {
                            Console.WriteLine("No New Item(s) to be Terminated.");
                        }
                        else
                        {
                            Console.WriteLine("Items to be Terminated: \n");
                            productsToTerminateList.ToList().ForEach(x => Console.WriteLine(x.eanSupplier));

                            Console.WriteLine("\nUpdating quantity to 0 ... \n");
                            FindAndUpdateProductToTerminateinDB();

                            Console.WriteLine("\nRemoving item(s) from transferfile ... \n");
                            RemoveIteminTransferFile();
                           
                            ConfirmStatusTerminated();
                        }

                    appStatus.Successful();

                }
                catch (Exception ex)
                {
                    appStatus.AddMessagLine(ex.Message + "\n" + ex.StackTrace);
                }
            }
        }

        
        public static List<ProductToTerminateEntity> productsToTerminateList = new List<ProductToTerminateEntity>();

        
        public static void GetProductsToTerminate()
        {
            FalseProductsDBCollection fp = new FalseProductsDBCollection();
            
            var products = fp.FalseProductsCollection.FindAll().Where(x => x.isTerminated == true).Where(z => z.plus["status"].AsString == "");

            foreach (var product in products)
            {
                productsToTerminateList.Add(new ProductToTerminateEntity
                    {
                        ean = product.ean,
                        eanSupplier = product.eanSupplier,
                        supplier = product.supplier,
                        isTerminated = product.isTerminated
                    });
            }

            
        }
        

        public static void FindAndUpdateProductToTerminateinDB()
        {
            IDatabase db = null;          
            int quantity = 0;

            foreach (var productsToTerminate in productsToTerminateList)
            {
                db = IdentifyDB(productsToTerminate.supplier);
                db.SetItemStock(productsToTerminate.ean, quantity);                
            }
        }

        public static void RemoveIteminTransferFile()
        {
            SupplierDB supplierDB = new SupplierDB();

            foreach (var productToRemove in productsToTerminateList)
            {
                var supplier = supplierDB.mongoCollection.FindOne(Query.EQ("_id", productToRemove.supplier.ToUpper()));
                string transferfile = supplier.GetTransferFile().Split('/').Last();
                string filePath = @"C:\inetpub\wwwroot\transfer\" + transferfile;

                var lines = File.ReadAllLines(filePath);
                var newLines = lines.Where(line => !line.Contains(productToRemove.ean));
                File.WriteAllLines(filePath, newLines);

            }
        }

        public static void ConfirmStatusTerminated()
        {
            if (productsToTerminateList.Count() != 0)
            {
                FalseProductsDBCollection fp = new FalseProductsDBCollection();

                foreach (var products in productsToTerminateList)
                {
                    fp.FalseProductsCollection.Update(Query.EQ("eanSupplier", products.eanSupplier), Update.Set("plus.status", "terminated"));
                    Console.WriteLine("EanSupplier: " + products.eanSupplier + "\t...\tSTATUS: " + "terminated");
                }

            }
        }


        private static IDatabase IdentifyDB(string supplier)
        {
            ProductFinder finder = new ProductFinder();

            foreach (var supplierdb in finder.getAllDatabases())
            {
                if (supplierdb.GetType().Name.ToString().ToLower().Contains(supplier.ToLower()))
                {
                    return supplierdb;

                }
            }
            return null;
        }


    }
}
